/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.platform.node.processor.Family;

namespace br.ufc.mdcc.hpcshelf.platform.node.processor.family.intel.Xeon_E5
{
	public interface BaseIXeonE5 : BaseIProcessorFamily<br.ufc.mdcc.hpcshelf.platform.node.processor.manufacturer.Intel.IIntel>, IQualifierKind 
	{
	}
}