using br.ufc.mdcc.hpcshelf.platform.locale.asiapacific.SouthKorea;

namespace br.ufc.mdcc.hpcshelf.platform.locale.asiapacific.Seoul
{
	public interface ISeoul : BaseISeoul, ISouthKorea
	{
	}
}