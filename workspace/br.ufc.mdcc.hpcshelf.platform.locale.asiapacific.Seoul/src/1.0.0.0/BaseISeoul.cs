/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.platform.locale.asiapacific.SouthKorea;

namespace br.ufc.mdcc.hpcshelf.platform.locale.asiapacific.Seoul
{
	public interface BaseISeoul : BaseISouthKorea, IQualifierKind 
	{
	}
}