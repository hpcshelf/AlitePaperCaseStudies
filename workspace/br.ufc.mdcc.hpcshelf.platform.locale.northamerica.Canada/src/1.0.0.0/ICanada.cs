using br.ufc.mdcc.hpcshelf.platform.locale.NorthAmerica;

namespace br.ufc.mdcc.hpcshelf.platform.locale.northamerica.Canada
{
	public interface ICanada : BaseICanada, INorthAmerica
	{
	}
}