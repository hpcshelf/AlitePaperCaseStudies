using System;
using br.ufc.pargo.hpe.backend.DGAC;
using br.ufc.pargo.hpe.basic;
using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.quantifier.IntUp;
using br.ufc.mdcc.backend.ec2.platform.general.EC2_T3a_Medium;

namespace br.ufc.mdcc.backend.ec2.impl.platform.general.EC2_T3a_Medium_impl
{
	public class IEC2_T3a_Medium_impl<N> : BaseIEC2_T3a_Medium_impl<N>, IEC2_T3a_Medium<N>
		where N:IntUp
	{
	}
}
