using br.ufc.mdcc.hpcshelf.platform.locale.AsiaPacific;

namespace br.ufc.mdcc.hpcshelf.platform.locale.asiapacific.Singapore
{
	public interface ISingapore : BaseISingapore, IAsiaPacific
	{
	}
}