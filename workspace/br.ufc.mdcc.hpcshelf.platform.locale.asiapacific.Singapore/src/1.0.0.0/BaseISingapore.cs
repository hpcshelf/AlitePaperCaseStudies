/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.platform.locale.AsiaPacific;

namespace br.ufc.mdcc.hpcshelf.platform.locale.asiapacific.Singapore
{
	public interface BaseISingapore : BaseIAsiaPacific, IQualifierKind 
	{
	}
}