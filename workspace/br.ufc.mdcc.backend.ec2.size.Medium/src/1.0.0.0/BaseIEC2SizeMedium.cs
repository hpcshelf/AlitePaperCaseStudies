/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.backend.ec2.size.Size;

namespace br.ufc.mdcc.backend.ec2.size.Medium
{
	public interface BaseIEC2SizeMedium : BaseIEC2Size, IQualifierKind 
	{
	}
}