using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.backend.ec2.size.Size;

namespace br.ufc.mdcc.backend.ec2.size.Medium
{
	public interface IEC2SizeMedium : BaseIEC2SizeMedium, IEC2Size
	{
	}
}