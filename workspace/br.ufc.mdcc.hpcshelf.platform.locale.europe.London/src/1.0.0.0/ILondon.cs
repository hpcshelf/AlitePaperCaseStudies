using br.ufc.mdcc.hpcshelf.platform.locale.europe.UnitedKingdon;

namespace br.ufc.mdcc.hpcshelf.platform.locale.europe.London
{
	public interface ILondon : BaseILondon, IUnitedKingdon
	{
	}
}