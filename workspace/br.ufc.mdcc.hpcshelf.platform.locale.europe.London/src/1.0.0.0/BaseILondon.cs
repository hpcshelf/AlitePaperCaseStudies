/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.platform.locale.europe.UnitedKingdon;

namespace br.ufc.mdcc.hpcshelf.platform.locale.europe.London
{
	public interface BaseILondon : BaseIUnitedKingdon, IQualifierKind 
	{
	}
}