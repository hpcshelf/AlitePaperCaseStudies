/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.backend.ec2.size.Size;

namespace br.ufc.mdcc.backend.ec2.size.Micro
{
	public interface BaseIEC2SizeMicro : BaseIEC2Size, IQualifierKind 
	{
	}
}