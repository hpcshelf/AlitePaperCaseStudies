/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.platform.locale.northamerica.US_West;

namespace br.ufc.mdcc.hpcshelf.platform.locale.northamerica.California
{
	public interface BaseICalifornia : BaseIUS_West, IQualifierKind 
	{
	}
}