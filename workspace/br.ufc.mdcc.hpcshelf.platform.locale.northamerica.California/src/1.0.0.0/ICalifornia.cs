using br.ufc.mdcc.hpcshelf.platform.locale.northamerica.US_West;

namespace br.ufc.mdcc.hpcshelf.platform.locale.northamerica.California
{
	public interface ICalifornia : BaseICalifornia, IUS_West
	{
	}
}