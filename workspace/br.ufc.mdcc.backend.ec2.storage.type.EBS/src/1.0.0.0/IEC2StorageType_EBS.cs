using br.ufc.mdcc.backend.ec2.storage.Type;

namespace br.ufc.mdcc.backend.ec2.storage.type.EBS
{
	public interface IEC2StorageType_EBS : BaseIEC2StorageType_EBS, IEC2StorageType
	{
	}
}