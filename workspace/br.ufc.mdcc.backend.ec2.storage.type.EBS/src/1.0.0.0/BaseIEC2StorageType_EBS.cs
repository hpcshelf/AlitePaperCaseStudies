/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.backend.ec2.storage.Type;

namespace br.ufc.mdcc.backend.ec2.storage.type.EBS
{
	public interface BaseIEC2StorageType_EBS : BaseIEC2StorageType, IQualifierKind 
	{
	}
}