using br.ufc.mdcc.hpcshelf.platform.node.processor.Manufacturer;
using br.ufc.mdcc.hpcshelf.platform.node.processor.Family;
using br.ufc.mdcc.hpcshelf.platform.node.processor.Series;
using br.ufc.mdcc.hpcshelf.platform.node.processor.Microarchitecture;

namespace br.ufc.mdcc.hpcshelf.platform.node.processor.Model
{
	public interface IProcessorModel<MAN, FAM, SER, MIC> : BaseIProcessorModel<MAN, FAM, SER, MIC>
		where MAN:IProcessorManufacturer
		where FAM:IProcessorFamily<MAN>
		where SER:IProcessorSeries<MAN, FAM>
		where MIC:IProcessorMicroarchitecture<MAN>
	{
	}
}