/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.blas.MatrixType;

namespace br.ufc.mdcc.hpcshelf.blas.matrixtype.General
{
	public interface BaseIGeneralMatrix : BaseIMatrixType, IQualifierKind 
	{
	}
}