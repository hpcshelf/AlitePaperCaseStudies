using br.ufc.mdcc.hpcshelf.blas.MatrixType;

namespace br.ufc.mdcc.hpcshelf.blas.matrixtype.General
{
	public interface IGeneralMatrix : BaseIGeneralMatrix, IMatrixType
	{
	}
}