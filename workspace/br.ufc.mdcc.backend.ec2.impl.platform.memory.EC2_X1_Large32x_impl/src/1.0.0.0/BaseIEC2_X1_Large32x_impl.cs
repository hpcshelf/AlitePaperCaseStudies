/* Automatically Generated Code */

using br.ufc.mdcc.backend.ec2.platform.memory.EC2_X1_Large32x;
using br.ufc.mdcc.backend.ec2.family.Memory;
using br.ufc.mdcc.backend.ec2.type.memory.X1;
using br.ufc.mdcc.backend.ec2.size.Large32x;
using br.ufc.mdcc.hpcshelf.platform.maintainer.EC2;
using br.ufc.mdcc.hpcshelf.quantifier.IntUp;

namespace br.ufc.mdcc.backend.ec2.impl.platform.memory.EC2_X1_Large32x_impl 
{
	public abstract class BaseIEC2_X1_Large32x_impl<N>: br.ufc.pargo.hpe.kinds.Environment, BaseIEC2_X1_Large32x<N>
		where N:IntUp
	{
		private IEC2TypeX1 type = null;

		protected IEC2TypeX1 Type
		{
			get
			{
				if (this.type == null)
					this.type = (IEC2TypeX1) Services.getPort("type");
				return this.type;
			}
		}
		private IEC2SizeLarge32x size = null;

		protected IEC2SizeLarge32x Size
		{
			get
			{
				if (this.size == null)
					this.size = (IEC2SizeLarge32x) Services.getPort("size");
				return this.size;
			}
		}
		private IEC2FamilyMemory family = null;

		protected IEC2FamilyMemory Family
		{
			get
			{
				if (this.family == null)
					this.family = (IEC2FamilyMemory) Services.getPort("family");
				return this.family;
			}
		}
		private IEC2 maintainer = null;

		protected IEC2 Maintainer
		{
			get
			{
				if (this.maintainer == null)
					this.maintainer = (IEC2) Services.getPort("maintainer");
				return this.maintainer;
			}
		}
	}
}