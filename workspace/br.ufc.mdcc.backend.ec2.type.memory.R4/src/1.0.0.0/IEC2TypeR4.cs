using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.backend.ec2.type.Type;

namespace br.ufc.mdcc.backend.ec2.type.memory.R4
{
	public interface IEC2TypeR4 : BaseIEC2TypeR4, IEC2Type<family.Memory.IEC2FamilyMemory>
	{
	}
}