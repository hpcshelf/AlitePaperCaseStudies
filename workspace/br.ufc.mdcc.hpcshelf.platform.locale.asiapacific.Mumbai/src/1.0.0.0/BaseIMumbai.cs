/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.platform.locale.asiapacific.India;

namespace br.ufc.mdcc.hpcshelf.platform.locale.asiapacific.Mumbai
{
	public interface BaseIMumbai : BaseIIndia, IQualifierKind 
	{
	}
}