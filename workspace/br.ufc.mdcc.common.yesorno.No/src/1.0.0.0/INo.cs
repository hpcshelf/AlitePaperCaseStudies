using br.ufc.mdcc.common.YesOrNo;

namespace br.ufc.mdcc.common.yesorno.No
{
	public interface INo : BaseINo, IYesOrNo
	{
	}
}