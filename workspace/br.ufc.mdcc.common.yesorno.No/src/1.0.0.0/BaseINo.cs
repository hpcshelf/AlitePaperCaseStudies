/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.common.YesOrNo;

namespace br.ufc.mdcc.common.yesorno.No
{
	public interface BaseINo : BaseIYesOrNo, IQualifierKind 
	{
	}
}