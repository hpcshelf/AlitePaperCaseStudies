#!/bin/sh

./generate_line_ec2_instance.sh GeneralPurpose general A1 Medium
./generate_line_ec2_instance.sh GeneralPurpose general A1 Large
./generate_line_ec2_instance.sh GeneralPurpose general A1 Large1x
./generate_line_ec2_instance.sh GeneralPurpose general A1 Large2x
./generate_line_ec2_instance.sh GeneralPurpose general A1 Large4x
./generate_line_ec2_instance.sh GeneralPurpose general A1 Metal

./generate_line_ec2_instance.sh GeneralPurpose general T3 Nano
./generate_line_ec2_instance.sh GeneralPurpose general T3 Micro
./generate_line_ec2_instance.sh GeneralPurpose general T3 Small
./generate_line_ec2_instance.sh GeneralPurpose general T3 Medium
./generate_line_ec2_instance.sh GeneralPurpose general T3 Large
./generate_line_ec2_instance.sh GeneralPurpose general T3 Large1x
./generate_line_ec2_instance.sh GeneralPurpose general T3 Large2x

./generate_line_ec2_instance.sh GeneralPurpose general T3a Nano
./generate_line_ec2_instance.sh GeneralPurpose general T3a Micro
./generate_line_ec2_instance.sh GeneralPurpose general T3a Small
./generate_line_ec2_instance.sh GeneralPurpose general T3a Medium
./generate_line_ec2_instance.sh GeneralPurpose general T3a Large
./generate_line_ec2_instance.sh GeneralPurpose general T3a Large1x
./generate_line_ec2_instance.sh GeneralPurpose general T3a Large2x

./generate_line_ec2_instance.sh GeneralPurpose general T2 Nano
./generate_line_ec2_instance.sh GeneralPurpose general T2 Micro
./generate_line_ec2_instance.sh GeneralPurpose general T2 Small
./generate_line_ec2_instance.sh GeneralPurpose general T2 Medium
./generate_line_ec2_instance.sh GeneralPurpose general T2 Large
./generate_line_ec2_instance.sh GeneralPurpose general T2 Large1x
./generate_line_ec2_instance.sh GeneralPurpose general T2 Large2x

./generate_line_ec2_instance.sh GeneralPurpose general M5 Large
./generate_line_ec2_instance.sh GeneralPurpose general M5 Large1x
./generate_line_ec2_instance.sh GeneralPurpose general M5 Large2x
./generate_line_ec2_instance.sh GeneralPurpose general M5 Large4x
./generate_line_ec2_instance.sh GeneralPurpose general M5 Large8x
./generate_line_ec2_instance.sh GeneralPurpose general M5 Large12x
./generate_line_ec2_instance.sh GeneralPurpose general M5 Large16x
./generate_line_ec2_instance.sh GeneralPurpose general M5 Large24x
./generate_line_ec2_instance.sh GeneralPurpose general M5 Metal

./generate_line_ec2_instance.sh GeneralPurpose general M5d Large
./generate_line_ec2_instance.sh GeneralPurpose general M5d Large1x
./generate_line_ec2_instance.sh GeneralPurpose general M5d Large2x
./generate_line_ec2_instance.sh GeneralPurpose general M5d Large4x
./generate_line_ec2_instance.sh GeneralPurpose general M5d Large8x
./generate_line_ec2_instance.sh GeneralPurpose general M5d Large12x
./generate_line_ec2_instance.sh GeneralPurpose general M5d Large16x
./generate_line_ec2_instance.sh GeneralPurpose general M5d Large24x
./generate_line_ec2_instance.sh GeneralPurpose general M5d Metal

./generate_line_ec2_instance.sh GeneralPurpose general M5a Large
./generate_line_ec2_instance.sh GeneralPurpose general M5a Large1x
./generate_line_ec2_instance.sh GeneralPurpose general M5a Large2x
./generate_line_ec2_instance.sh GeneralPurpose general M5a Large4x
./generate_line_ec2_instance.sh GeneralPurpose general M5a Large8x
./generate_line_ec2_instance.sh GeneralPurpose general M5a Large12x
./generate_line_ec2_instance.sh GeneralPurpose general M5a Large16x
./generate_line_ec2_instance.sh GeneralPurpose general M5a Large24x

./generate_line_ec2_instance.sh GeneralPurpose general M5ad Large
./generate_line_ec2_instance.sh GeneralPurpose general M5ad Large1x
./generate_line_ec2_instance.sh GeneralPurpose general M5ad Large2x
./generate_line_ec2_instance.sh GeneralPurpose general M5ad Large4x
./generate_line_ec2_instance.sh GeneralPurpose general M5ad Large8x
./generate_line_ec2_instance.sh GeneralPurpose general M5ad Large12x
./generate_line_ec2_instance.sh GeneralPurpose general M5ad Large16x
./generate_line_ec2_instance.sh GeneralPurpose general M5ad Large24x

./generate_line_ec2_instance.sh GeneralPurpose general M5n Large
./generate_line_ec2_instance.sh GeneralPurpose general M5n Large1x
./generate_line_ec2_instance.sh GeneralPurpose general M5n Large2x
./generate_line_ec2_instance.sh GeneralPurpose general M5n Large4x
./generate_line_ec2_instance.sh GeneralPurpose general M5n Large8x
./generate_line_ec2_instance.sh GeneralPurpose general M5n Large12x
./generate_line_ec2_instance.sh GeneralPurpose general M5n Large16x
./generate_line_ec2_instance.sh GeneralPurpose general M5n Large24x

./generate_line_ec2_instance.sh GeneralPurpose general M5dn Large
./generate_line_ec2_instance.sh GeneralPurpose general M5dn Large1x
./generate_line_ec2_instance.sh GeneralPurpose general M5dn Large2x
./generate_line_ec2_instance.sh GeneralPurpose general M5dn Large4x
./generate_line_ec2_instance.sh GeneralPurpose general M5dn Large8x
./generate_line_ec2_instance.sh GeneralPurpose general M5dn Large12x
./generate_line_ec2_instance.sh GeneralPurpose general M5dn Large16x
./generate_line_ec2_instance.sh GeneralPurpose general M5dn Large24x

./generate_line_ec2_instance.sh GeneralPurpose general M4 Large
./generate_line_ec2_instance.sh GeneralPurpose general M4 Large1x
./generate_line_ec2_instance.sh GeneralPurpose general M4 Large2x
./generate_line_ec2_instance.sh GeneralPurpose general M4 Large4x
./generate_line_ec2_instance.sh GeneralPurpose general M4 Large10x
./generate_line_ec2_instance.sh GeneralPurpose general M4 Large16x

./generate_line_ec2_instance.sh Computation compute C5 Large
./generate_line_ec2_instance.sh Computation compute C5 Large1x
./generate_line_ec2_instance.sh Computation compute C5 Large2x
./generate_line_ec2_instance.sh Computation compute C5 Large4x
./generate_line_ec2_instance.sh Computation compute C5 Large9x
./generate_line_ec2_instance.sh Computation compute C5 Large12x
./generate_line_ec2_instance.sh Computation compute C5 Large18x
./generate_line_ec2_instance.sh Computation compute C5 Large24x
./generate_line_ec2_instance.sh Computation compute C5 Metal

./generate_line_ec2_instance.sh Computation compute C5d Large
./generate_line_ec2_instance.sh Computation compute C5d Large1x
./generate_line_ec2_instance.sh Computation compute C5d Large2x
./generate_line_ec2_instance.sh Computation compute C5d Large4x
./generate_line_ec2_instance.sh Computation compute C5d Large9x
./generate_line_ec2_instance.sh Computation compute C5d Large12x
./generate_line_ec2_instance.sh Computation compute C5d Large18x
./generate_line_ec2_instance.sh Computation compute C5d Large24x
./generate_line_ec2_instance.sh Computation compute C5d Metal

./generate_line_ec2_instance.sh Computation compute C5n Large
./generate_line_ec2_instance.sh Computation compute C5n Large1x
./generate_line_ec2_instance.sh Computation compute C5n Large2x
./generate_line_ec2_instance.sh Computation compute C5n Large4x
./generate_line_ec2_instance.sh Computation compute C5n Large9x
./generate_line_ec2_instance.sh Computation compute C5n Large18x
./generate_line_ec2_instance.sh Computation compute C5n Metal

./generate_line_ec2_instance.sh Computation compute C4 Large
./generate_line_ec2_instance.sh Computation compute C4 Large1x
./generate_line_ec2_instance.sh Computation compute C4 Large2x
./generate_line_ec2_instance.sh Computation compute C4 Large4x
./generate_line_ec2_instance.sh Computation compute C4 Large8x

./generate_line_ec2_instance.sh Memory memory R5 Large
./generate_line_ec2_instance.sh Memory memory R5 Large1x
./generate_line_ec2_instance.sh Memory memory R5 Large2x
./generate_line_ec2_instance.sh Memory memory R5 Large4x
./generate_line_ec2_instance.sh Memory memory R5 Large8x
./generate_line_ec2_instance.sh Memory memory R5 Large12x
./generate_line_ec2_instance.sh Memory memory R5 Large16x
./generate_line_ec2_instance.sh Memory memory R5 Large24x
./generate_line_ec2_instance.sh Memory memory R5 Metal

./generate_line_ec2_instance.sh Memory memory R5d Large
./generate_line_ec2_instance.sh Memory memory R5d Large1x
./generate_line_ec2_instance.sh Memory memory R5d Large2x
./generate_line_ec2_instance.sh Memory memory R5d Large4x
./generate_line_ec2_instance.sh Memory memory R5d Large8x
./generate_line_ec2_instance.sh Memory memory R5d Large12x
./generate_line_ec2_instance.sh Memory memory R5d Large16x
./generate_line_ec2_instance.sh Memory memory R5d Large24x
./generate_line_ec2_instance.sh Memory memory R5d Metal

./generate_line_ec2_instance.sh Memory memory R5a Large
./generate_line_ec2_instance.sh Memory memory R5a Large1x
./generate_line_ec2_instance.sh Memory memory R5a Large2x
./generate_line_ec2_instance.sh Memory memory R5a Large4x
./generate_line_ec2_instance.sh Memory memory R5a Large8x
./generate_line_ec2_instance.sh Memory memory R5a Large12x
./generate_line_ec2_instance.sh Memory memory R5a Large16x
./generate_line_ec2_instance.sh Memory memory R5a Large24x

./generate_line_ec2_instance.sh Memory memory R5ad Large
./generate_line_ec2_instance.sh Memory memory R5ad Large1x
./generate_line_ec2_instance.sh Memory memory R5ad Large2x
./generate_line_ec2_instance.sh Memory memory R5ad Large4x
./generate_line_ec2_instance.sh Memory memory R5ad Large12x
./generate_line_ec2_instance.sh Memory memory R5ad Large24x

./generate_line_ec2_instance.sh Memory memory R5n Large
./generate_line_ec2_instance.sh Memory memory R5n Large1x
./generate_line_ec2_instance.sh Memory memory R5n Large2x
./generate_line_ec2_instance.sh Memory memory R5n Large4x
./generate_line_ec2_instance.sh Memory memory R5n Large8x
./generate_line_ec2_instance.sh Memory memory R5n Large12x
./generate_line_ec2_instance.sh Memory memory R5n Large16x
./generate_line_ec2_instance.sh Memory memory R5n Large24x

./generate_line_ec2_instance.sh Memory memory R5dn Large
./generate_line_ec2_instance.sh Memory memory R5dn Large1x
./generate_line_ec2_instance.sh Memory memory R5dn Large2x
./generate_line_ec2_instance.sh Memory memory R5dn Large4x
./generate_line_ec2_instance.sh Memory memory R5dn Large8x
./generate_line_ec2_instance.sh Memory memory R5dn Large12x
./generate_line_ec2_instance.sh Memory memory R5dn Large16x
./generate_line_ec2_instance.sh Memory memory R5dn Large24x

./generate_line_ec2_instance.sh Memory memory R4 Large
./generate_line_ec2_instance.sh Memory memory R4 Large1x
./generate_line_ec2_instance.sh Memory memory R4 Large2x
./generate_line_ec2_instance.sh Memory memory R4 Large4x
./generate_line_ec2_instance.sh Memory memory R4 Large8x
./generate_line_ec2_instance.sh Memory memory R4 Large16x

./generate_line_ec2_instance.sh Memory memory X1e Large1x
./generate_line_ec2_instance.sh Memory memory X1e Large2x
./generate_line_ec2_instance.sh Memory memory X1e Large4x
./generate_line_ec2_instance.sh Memory memory X1e Large8x
./generate_line_ec2_instance.sh Memory memory X1e Large16x
./generate_line_ec2_instance.sh Memory memory X1e Large32x

./generate_line_ec2_instance.sh Memory memory X1 Large16x
./generate_line_ec2_instance.sh Memory memory X1 Large32x

./generate_line_ec2_instance.sh Memory memory HighMemory Metal_u_6tb1
./generate_line_ec2_instance.sh Memory memory HighMemory Metal_u_9tb1
./generate_line_ec2_instance.sh Memory memory HighMemory Metal_u_12tb1
./generate_line_ec2_instance.sh Memory memory HighMemory Metal_u_18tb1
./generate_line_ec2_instance.sh Memory memory HighMemory Metal_u_24tb1

./generate_line_ec2_instance.sh Memory memory Z1d Large
./generate_line_ec2_instance.sh Memory memory Z1d Large1x
./generate_line_ec2_instance.sh Memory memory Z1d Large2x
./generate_line_ec2_instance.sh Memory memory Z1d Large3x
./generate_line_ec2_instance.sh Memory memory Z1d Large6x
./generate_line_ec2_instance.sh Memory memory Z1d Large12x
./generate_line_ec2_instance.sh Memory memory Z1d Metal

./generate_line_ec2_instance.sh Acceleration accelerated P3 Large2x
./generate_line_ec2_instance.sh Acceleration accelerated P3 Large8x
./generate_line_ec2_instance.sh Acceleration accelerated P3 Large16x
./generate_line_ec2_instance.sh Acceleration accelerated P3 Large24x

./generate_line_ec2_instance.sh Acceleration accelerated P2 Large1x
./generate_line_ec2_instance.sh Acceleration accelerated P2 Large8x
./generate_line_ec2_instance.sh Acceleration accelerated P2 Large16x

./generate_line_ec2_instance.sh Acceleration accelerated G4dn Large1x
./generate_line_ec2_instance.sh Acceleration accelerated G4dn Large2x
./generate_line_ec2_instance.sh Acceleration accelerated G4dn Large4x
./generate_line_ec2_instance.sh Acceleration accelerated G4dn Large8x
./generate_line_ec2_instance.sh Acceleration accelerated G4dn Large16x
./generate_line_ec2_instance.sh Acceleration accelerated G4dn Large12x
./generate_line_ec2_instance.sh Acceleration accelerated G4dn Metal

./generate_line_ec2_instance.sh Acceleration accelerated G3 Large1x
./generate_line_ec2_instance.sh Acceleration accelerated G3 Large4x
./generate_line_ec2_instance.sh Acceleration accelerated G3 Large8x
./generate_line_ec2_instance.sh Acceleration accelerated G3 Large16x

./generate_line_ec2_instance.sh Acceleration accelerated F1 Large2x
./generate_line_ec2_instance.sh Acceleration accelerated F1 Large4x
./generate_line_ec2_instance.sh Acceleration accelerated F1 Large16x

./generate_line_ec2_instance.sh Storage storage I3 Large
./generate_line_ec2_instance.sh Storage storage I3 Large1x
./generate_line_ec2_instance.sh Storage storage I3 Large2x
./generate_line_ec2_instance.sh Storage storage I3 Large4x
./generate_line_ec2_instance.sh Storage storage I3 Large8x
./generate_line_ec2_instance.sh Storage storage I3 Large16x
./generate_line_ec2_instance.sh Storage storage I3 Metal

./generate_line_ec2_instance.sh Storage storage I3en Large
./generate_line_ec2_instance.sh Storage storage I3en Large1x
./generate_line_ec2_instance.sh Storage storage I3en Large2x
./generate_line_ec2_instance.sh Storage storage I3en Large3x
./generate_line_ec2_instance.sh Storage storage I3en Large6x
./generate_line_ec2_instance.sh Storage storage I3en Large12x
./generate_line_ec2_instance.sh Storage storage I3en Large24x
./generate_line_ec2_instance.sh Storage storage I3en Metal

./generate_line_ec2_instance.sh Storage storage D2 Large1x
./generate_line_ec2_instance.sh Storage storage D2 Large2x
./generate_line_ec2_instance.sh Storage storage D2 Large4x
./generate_line_ec2_instance.sh Storage storage D2 Large8x

./generate_line_ec2_instance.sh Storage storage H1 Large2x
./generate_line_ec2_instance.sh Storage storage H1 Large4x
./generate_line_ec2_instance.sh Storage storage H1 Large8x
./generate_line_ec2_instance.sh Storage storage H1 Large16x


