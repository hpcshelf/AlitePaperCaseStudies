/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.common.BrowserPortType;
using br.ufc.mdcc.hpc.storm.binding.environment.EnvironmentBindingBaseDirect;

namespace br.ufc.mdcc.hpcshelf.common.BrowserBinding
{
	public interface BaseIBrowserBinding<S> : BaseIBindingDirect<br.ufc.mdcc.hpcshelf.common.BrowserPortType.IBrowserPortType, S>, ISynchronizerKind 
		where S:IBrowserPortType
	{
	}
}