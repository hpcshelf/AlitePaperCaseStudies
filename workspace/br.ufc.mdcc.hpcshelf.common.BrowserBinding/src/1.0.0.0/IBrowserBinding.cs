using br.ufc.mdcc.hpcshelf.common.BrowserPortType;
using br.ufc.mdcc.hpc.storm.binding.environment.EnvironmentBindingBaseDirect;

namespace br.ufc.mdcc.hpcshelf.common.BrowserBinding
{
	public interface IBrowserBinding<S> : BaseIBrowserBinding<S>, IBindingDirect<br.ufc.mdcc.hpcshelf.common.BrowserPortType.IBrowserPortType, S>
		where S:IBrowserPortType
	{
	}
}