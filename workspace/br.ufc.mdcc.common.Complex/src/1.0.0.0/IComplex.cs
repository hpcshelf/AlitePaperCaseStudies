using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.common.Data;
using System;

namespace br.ufc.mdcc.common.Complex { 

	public interface IComplex : BaseIComplex, IData
	{
		IComplexInstance newInstance(System.Numerics.Complex i);
	} // end main interface 

	public interface IComplexInstance : IDataInstance, ICloneable
	{
		System.Numerics.Complex Value { set; get; }
	}

} // end namespace 
