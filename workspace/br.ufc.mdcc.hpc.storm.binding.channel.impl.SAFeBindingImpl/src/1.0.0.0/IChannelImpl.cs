
using System;
using br.ufc.pargo.hpe.backend.DGAC;
using br.ufc.pargo.hpe.basic;
using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpc.storm.binding.channel.Binding;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Diagnostics;

using EnvelopType = System.Tuple<int, int, int, int, int, int>;
using System.Collections.Generic;
using System.Threading;
using System.Collections.Concurrent;
using System.Net.Sockets;
using System.Net;
using System.Collections;

namespace br.ufc.mdcc.hpc.storm.binding.channel.impl.SAFeBindingImpl
{
    public class IChannelImpl : BaseIChannelImpl, IChannel
    {
        public TypeOfChannelSend HPCShelfSend { get { return null; } }
        public TypeOfChannelRecv HPCShelfRecv { get { return null; } }

        public override void release1()
        {
            base.release1();
        }

        public override void release2()
        {
            base.release2();
            Console.WriteLine("CHANNEL SAFeBINDING - RELEASE2 1");
            foreach (Thread t in thread_receive_requests.Values)
                t.Abort();
            Console.WriteLine("CHANNEL SAFeBINDING - RELEASE2 2");
            foreach (Socket s in server_socket_facet.Values)
                s.Close();
            Console.WriteLine("CHANNEL SAFeBINDING - RELEASE2 3");
            foreach (Socket s in client_socket_facet.Values)
            {
                s.Disconnect(false);
                s.Close();
            }
            Console.WriteLine("CHANNEL SAFeBINDING - RELEASE2 4");
        }

        private IDictionary<int, Thread> thread_receive_requests = null;

        public int ChannelKey { get { return -1; } }

        private static byte[] ObjectToByteArray(Object obj)
        {
            if (obj == null)
                return null;
            BinaryFormatter bf = new BinaryFormatter();
            MemoryStream ms = new MemoryStream();
            bf.Serialize(ms, obj);
            return ms.ToArray();
        }

        private static Object ByteArrayToObject(byte[] arrBytes)
        {
            MemoryStream memStream = new MemoryStream();
            BinaryFormatter binForm = new BinaryFormatter();
            memStream.Write(arrBytes, 0, arrBytes.Length);
            memStream.Seek(0, SeekOrigin.Begin);
            Object obj = (Object)binForm.Deserialize(memStream);
            return obj;
        }

        #region implemented abstract members of BindingRoot
        public override void server()
        {
            this.TraceFlag = true;

            Console.WriteLine(": BEFORE CREATE SOCKETS !!! " + this.ThisFacet + " / " + this.ThisFacetInstance + " : " + this.CID.getInstanceName());

            createSockets();

            Console.WriteLine(": AFTER CREATED SOCKETS !!! " + this.ThisFacet + " / " + this.ThisFacetInstance + " : " + this.CID.getInstanceName());

            Synchronizer_monitor = new SynchronizerMonitor(this, client_socket_facet, server_socket_facet_ack, this.ThisFacetInstance, this.CID.getInstanceName());

            sockets_initialized_flag.Set();

            // Create the threads that will listen the sockets for each other facet.
            thread_receive_requests = new Dictionary<int, Thread>();

            foreach (int facet in this.Facet.Keys)
            {
                // * if (facet != this.ThisFacetInstance)
                {
                    Console.WriteLine("loop create thread_receive_requests: " + facet + " / " + this.ThisFacetInstance);
                    Socket server_socket = server_socket_facet[facet];
                    Socket client_socket_ack = client_socket_facet_ack[facet];
                    thread_receive_requests[facet] = new Thread(new ThreadStart(() => Synchronizer_monitor.serverReceiveRequests(facet, client_socket_ack, server_socket)));
                    thread_receive_requests[facet].Start();
                }
            }

            while (true)
            {
                Thread.Sleep(100);
                Synchronizer_monitor.serverReadRequest();
            }

        }

        private AutoResetEvent sockets_initialized_flag = new AutoResetEvent(false);

        public override void client()
        {
            this.TraceFlag = true;

            //while (!sockets_initialized_flag) Thread.Sleep (100);
            sockets_initialized_flag.WaitOne();

        }
        #endregion

        private SynchronizerMonitor synchronizer_monitor;

        private IDictionary<int, Socket> client_socket_facet = new Dictionary<int, Socket>();
        private IDictionary<int, Socket> server_socket_facet = new Dictionary<int, Socket>();

        private IDictionary<int, Socket> client_socket_facet_ack = new Dictionary<int, Socket>();
        private IDictionary<int, Socket> server_socket_facet_ack = new Dictionary<int, Socket>();

        private void connectSockets()
        {
            foreach (KeyValuePair<int, FacetAccess> facet_access in this.Facet)
            {
                int facet = facet_access.Key;

                Socket socket = client_socket_facet[facet];
                IPEndPoint endPoint = end_point_client[facet];

                Socket socket_ack = client_socket_facet_ack[facet];
                IPEndPoint endPoint_ack = end_point_client_ack[facet];

                bool isConnected = false;
                while (!isConnected)
                {
                    try
                    {
                        Console.WriteLine("CONNECTING " + "facet=" + facet + " / " + endPoint + " / " + this.CID.getInstanceName());
                        socket.Connect(endPoint);
                        Console.WriteLine("CONNECTED " + "facet=" + facet + " / " + endPoint + " / " + this.CID.getInstanceName());

                        isConnected = true;
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("CONNECTION FAILED N --- *** " + e.Message + " --- " + endPoint);
                        Thread.Sleep(1000);
                    }

                }

                if (!isConnected)
                {
                    Console.WriteLine("createSockets --- It was not possible to talk to the server");
                    throw new Exception("createSockets --- It was not possible to talk to the server");
                }

                bool isConnected_ack = false;
                while (!isConnected_ack)
                {
                    try
                    {
                        Console.WriteLine("CONNECTING ACK " + "facet=" + facet + " / " + endPoint_ack + " / " + this.CID.getInstanceName());
                        socket_ack.Connect(endPoint_ack);
                        Console.WriteLine("CONNECTED ACK " + "facet=" + facet + " / " + endPoint_ack + " / " + this.CID.getInstanceName());

                        isConnected_ack = true;
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("CONNECTION ACK FAILED N --- *** " + e.Message + " --- " + endPoint_ack);
                        Thread.Sleep(1000);
                    }
                }
                if (!isConnected_ack)
                {
                    Console.WriteLine("createSockets ACK --- It was not possible to talk to the server");
                    throw new Exception("createSockets ACK --- It was not possible to talk to the server");
                }

            }
        }



        private void acceptSockets()
        {
            foreach (KeyValuePair<int, FacetAccess> facet_access in this.Facet)
            {
                int facet = facet_access.Key;
                // * if (facet != this.ThisFacetInstance)
                {
                    Socket socket = server_socket_facet[facet];
                    IPEndPoint endPoint = end_point_server[facet];

                    Socket socket_ack = server_socket_facet_ack[facet];
                    IPEndPoint endPoint_ack = end_point_server_ack[facet];

                    Console.WriteLine("BINDING " + endPoint + " -- " + facet + " / " + this.CID.getInstanceName());

                    socket.Bind(endPoint);
                    socket.Listen(10);
                    server_socket_facet[facet] = socket.Accept();

                    Console.WriteLine("BINDED " + endPoint + " -- " + facet + " / " + this.CID.getInstanceName());

                    Console.WriteLine("BINDING ACK " + endPoint_ack + " -- " + facet + " / " + this.CID.getInstanceName());

                    socket_ack.Bind(endPoint_ack);
                    socket_ack.Listen(10);
                    server_socket_facet_ack[facet] = socket_ack.Accept();

                    Console.WriteLine("BINDED ACK " + endPoint_ack + " -- " + facet + " / " + this.CID.getInstanceName());
                }
            }
        }

        private IDictionary<int, IPEndPoint> end_point_client = new Dictionary<int, IPEndPoint>();
        private IDictionary<int, IPEndPoint> end_point_server = new Dictionary<int, IPEndPoint>();

        private IDictionary<int, IPEndPoint> end_point_client_ack = new Dictionary<int, IPEndPoint>();
        private IDictionary<int, IPEndPoint> end_point_server_ack = new Dictionary<int, IPEndPoint>();

        private void createSockets()
        {
            SortedList l = new SortedList();
            foreach (int facet_instance in this.Facet.Keys)
                l.Add(facet_instance, facet_instance);

            int[] u = new int[l.Count];
            l.GetKeyList().CopyTo(u, 0);

            IDictionary<int, int> u_inv = new Dictionary<int, int>();
            for (int i = 0; i < u.Length; i++)
                u_inv[u[i]] = i * 2;

            FacetAccess facet_access_server = this.Facet[ThisFacetInstance];
            string ip_address_server = facet_access_server.ip_address;

            foreach (KeyValuePair<int, FacetAccess> facet_access_client in this.Facet)
            {
                int facet_instance = facet_access_client.Key;
                // * if (facet_instance != this.ThisFacetInstance)
                {
                    string ip_address_client = facet_access_client.Value.ip_address;

                    Console.WriteLine("ip_address_client={0} ---- ip_address_server={1} ", ip_address_client, ip_address_server);
                    if (ip_address_client.Equals(ip_address_server))
                        ip_address_client = "127.0.0.1";

                    int port_client = facet_access_client.Value.port + u_inv[this.ThisFacetInstance];
                    IPAddress ipAddress_client = Dns.GetHostAddresses(ip_address_client)[0]; // ipHostInfo_client.AddressList[0];
                    IPEndPoint endPoint_client = new IPEndPoint(ipAddress_client, port_client);
                    end_point_client[facet_instance] = endPoint_client;

                    int port_client_ack = facet_access_client.Value.port + u_inv[this.ThisFacetInstance] + 1;
                    IPAddress ipAddress_client_ack = Dns.GetHostAddresses(ip_address_client)[0]; // ipHostInfo_client.AddressList[0];
                    IPEndPoint endPoint_client_ack = new IPEndPoint(ipAddress_client_ack, port_client_ack);
                    end_point_client_ack[facet_instance] = endPoint_client_ack;

                    Console.WriteLine("CREATE SOCKETS - end_point_client[" + facet_instance + "]=" + endPoint_client);
                    Console.WriteLine("CREATE SOCKETS - end_point_client_ack[" + facet_instance + "]=" + endPoint_client_ack);

                    int port_server = facet_access_server.port + u_inv[facet_instance];
                    IPEndPoint endPoint_server = new IPEndPoint(IPAddress.Any, port_server);
                    end_point_server[facet_instance] = endPoint_server;

                    int port_server_ack = facet_access_server.port + u_inv[facet_instance] + 1;
                    IPEndPoint endPoint_server_ack = new IPEndPoint(IPAddress.Any, port_server_ack);
                    end_point_server_ack[facet_instance] = endPoint_server_ack;

                    Console.WriteLine("CREATE SOCKETS - end_point_server[" + facet_instance + "]=" + endPoint_server);
                    Console.WriteLine("CREATE SOCKETS - end_point_server_ack[" + facet_instance + "]=" + endPoint_server_ack);

                    // Create a TCP/IP client socket.
                    Socket client_socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, System.Net.Sockets.ProtocolType.Tcp);
                    Socket client_socket_ack = new Socket(AddressFamily.InterNetwork, SocketType.Stream, System.Net.Sockets.ProtocolType.Tcp);

                    // Create a TCP/IP server socket.
                    Socket server_socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, System.Net.Sockets.ProtocolType.Tcp);
                    Socket server_socket_ack = new Socket(AddressFamily.InterNetwork, SocketType.Stream, System.Net.Sockets.ProtocolType.Tcp);

                    Console.WriteLine("SOCKETS CREATED - facet_instance=" + facet_instance);

                    server_socket.SendTimeout = server_socket.ReceiveTimeout = client_socket.SendTimeout = client_socket.ReceiveTimeout = -1;
                    client_socket_facet[facet_instance] = client_socket;
                    server_socket_facet[facet_instance] = server_socket;

                    server_socket_ack.SendTimeout = server_socket_ack.ReceiveTimeout = client_socket_ack.SendTimeout = client_socket_ack.ReceiveTimeout = -1;
                    client_socket_facet_ack[facet_instance] = client_socket_ack;
                    server_socket_facet_ack[facet_instance] = server_socket_ack;
                }
            }

            Thread thread_connect_sockets = new Thread(new ThreadStart(connectSockets));
            Thread thread_accept_sockets = new Thread(new ThreadStart(acceptSockets));

            thread_connect_sockets.Start();
            thread_accept_sockets.Start();

            Console.WriteLine("CREATE SOCKETS - connectSockets and acceptSockets launched !!!");

            thread_connect_sockets.Join();
            thread_accept_sockets.Join();

            Console.WriteLine("CREATE SOCKETS - connectSockets and acceptSockets finished !!!");
        }


        public void Send<T>(T value, Tuple<int, int> dest, int tag)
        {
            byte[] value_packet = ObjectToByteArray(value);
            handle_SEND(AliencommunicatorOperation.SEND, new Tuple<int, int, int, byte[]>(dest.Item1, dest.Item2, tag, value_packet));
        }

        public IRequest ImmediateSend<T>(T value, Tuple<int, int> dest, int tag)
        {
            ManualResetEvent waiting_request = new ManualResetEvent(false);
            IRequest request = Request.createRequest(waiting_request, dest, tag);
            byte[] value_packet = ObjectToByteArray(value);

            Thread t = new Thread(new ThreadStart(delegate ()
            {
                handle_SEND(AliencommunicatorOperation.SEND, new Tuple<int, int, int, byte[]>(dest.Item1, dest.Item2, tag, value_packet));
                waiting_request.Set();
            }));
            t.Start();

            return request;
        }

        public void Send<T>(T[] values, Tuple<int, int> dest, int tag)
        {
            byte[] value_packet = ObjectToByteArray(values);
            handle_SEND(AliencommunicatorOperation.SEND_ARRAY, new Tuple<int, int, int, byte[]>(dest.Item1, dest.Item2, tag, value_packet));
        }

        public IRequest ImmediateSend<T>(T[] values, Tuple<int, int> dest, int tag)
        {
            ManualResetEvent waiting_request = new ManualResetEvent(false);
            IRequest request = Request.createRequest(waiting_request, dest, tag);
            byte[] value_packet = ObjectToByteArray(values);

            Thread t = new Thread(new ThreadStart(delegate ()
            {
                handle_SEND(AliencommunicatorOperation.SEND_ARRAY, new Tuple<int, int, int, byte[]>(dest.Item1, dest.Item2, tag, value_packet));
                waiting_request.Set();
            }));
            t.Start();

            return request;
        }

        public IRequest ImmediateSyncSend<T>(T value, Tuple<int, int> dest, int tag)
        {
            // In SAFe, it has the same semantics of ImmediateSend, since the call returns after handle_SEND has been completed.
            return ImmediateSend<T>(value, dest, tag);
        }

        void handle_SEND(int operation_type, Tuple<int, int, int, byte[]> operation_info)
        {
            int facet_src = this.ThisFacetInstance;
            int facet_dst = operation_info.Item1;
            int src = 0;
            int dst = operation_info.Item2;
            int tag = operation_info.Item3;

            EnvelopType envelop = new EnvelopType(operation_type, facet_src, facet_dst, src, dst, tag);
            byte[] message1 = operation_info.Item4;

            if (tag >= 0)
                Synchronizer_monitor.clientSendRequest(envelop, message1);
            else
                Synchronizer_monitor.clientSendRequestAnyTag(envelop, message1, ref tag);

        }

        //private object lock_recv = new object();

        public T Receive<T>(Tuple<int, int> source, int tag)
        {
            T result;
            Receive<T>(source, tag, out result);
            return result;
        }

        public void Receive<T>(Tuple<int, int> source, int tag, out T value)
        {
            br.ufc.mdcc.hpc.storm.binding.channel.Binding.CompletedStatus status;
            Receive<T>(source, tag, out value, out status);
        }

        public void Receive<T>(Tuple<int, int> source, int tag, out T value, out br.ufc.mdcc.hpc.storm.binding.channel.Binding.CompletedStatus status)
        {
            byte[] v;
            ReceiveNative(source, tag, out v, out status);
            value = (T)ByteArrayToObject(v);
        }


        public byte[] ReceiveNative(Tuple<int, int> source, int tag)
        {
            byte[] result;
            ReceiveNative(source, tag, out result);
            return result;
        }

        public void ReceiveNative(Tuple<int, int> source, int tag, out byte[] value)
        {
            br.ufc.mdcc.hpc.storm.binding.channel.Binding.CompletedStatus status;
            ReceiveNative(source, tag, out value, out status);
        }

        public void ReceiveNative(Tuple<int, int> source, int tag, out byte[] value, out br.ufc.mdcc.hpc.storm.binding.channel.Binding.CompletedStatus status)
        {
            Console.WriteLine("RECEIVE NATIVE 1 facet={0} rank={1} tag={2}", source.Item1, source.Item2, tag);
            value = handle_RECEIVE(AliencommunicatorOperation.RECEIVE, new Tuple<int, int, int>(source.Item1, source.Item2, tag));
            Console.WriteLine("RECEIVE NATIVE 2 facet={0} rank={1} tag={2}", source.Item1, source.Item2, tag);
            status = SAFeCompletedStatus.createStatus(source, tag);
            Console.WriteLine("RECEIVE NATIVE 3 facet={0} rank={1} tag={2}", source.Item1, source.Item2, tag);
        }


        public br.ufc.mdcc.hpc.storm.binding.channel.Binding.IReceiveRequest ImmediateReceive<T>(Tuple<int, int> source, int tag)
        {
            ManualResetEvent waiting_request = new ManualResetEvent(false);
            IReceiveRequest request = ValueReceiveRequest<T>.createRequest(waiting_request, source, tag);

            Thread t = new Thread(new ThreadStart(delegate ()
            {
                byte[] v = handle_RECEIVE(AliencommunicatorOperation.RECEIVE, new Tuple<int, int, int>(source.Item1, source.Item2, tag));
                ((ValueReceiveRequest<T>)request).SetValue(v);
                waiting_request.Set();
            }));
            t.Start();

            return request;
        }

        public void Receive<T>(Tuple<int, int> source, int tag, ref T[] values)
        {
            br.ufc.mdcc.hpc.storm.binding.channel.Binding.CompletedStatus status;
            Receive<T>(source, tag, ref values, out status);
        }

        public void Receive<T>(Tuple<int, int> source, int tag, ref T[] values, out br.ufc.mdcc.hpc.storm.binding.channel.Binding.CompletedStatus status)
        {
            byte[] v = handle_RECEIVE(AliencommunicatorOperation.RECEIVE_ARRAY, new Tuple<int, int, int>(source.Item1, source.Item2, tag));
            values = (T[])ByteArrayToObject(v);
            status = SAFeCompletedStatus.createStatus(source, tag);
        }

        public br.ufc.mdcc.hpc.storm.binding.channel.Binding.IReceiveRequest ImmediateReceive<T>(Tuple<int, int> source, int tag, T[] values)
        {
            ManualResetEvent waiting_request = new ManualResetEvent(false);
            IReceiveRequest request = ValueReceiveRequest<T>.createRequest(waiting_request, source, tag);

            Thread t = new Thread(new ThreadStart(delegate ()
            {
                byte[] v = handle_RECEIVE(AliencommunicatorOperation.RECEIVE_ARRAY, new Tuple<int, int, int>(source.Item1, source.Item2, tag));
                ((ValueReceiveRequest<T>)request).SetValue(v);
                waiting_request.Set();
            }));
            t.Start();

            return request;
        }

        byte[] handle_RECEIVE(int operation_type, Tuple<int, int, int> operation_info)
        {
            int facet_src = this.ThisFacetInstance;
            int facet_dst = operation_info.Item1;
            int src = 0;
            int dst = operation_info.Item2;
            int tag = operation_info.Item3;

            EnvelopType envelop = new EnvelopType(operation_type, facet_src, facet_dst, src, dst, tag);
            byte[] message2 = tag < 0 ? Synchronizer_monitor.clientSendRequestAnyTag(envelop, new byte[0], ref tag)
                                      : Synchronizer_monitor.clientSendRequest(envelop, new byte[0]);

            return message2;
        }


        #region IDisposable implementation
        private bool disposed = false;

        internal SynchronizerMonitor Synchronizer_monitor
        {
            get
            {
                if (synchronizer_monitor == null)
                {
                    Console.WriteLine("SYNCHRONIZER MONITOR --- WAIT BEFORE --- {0}", this.CID);
                    synchronizer_monitor_initialized.WaitOne();
                    Console.WriteLine("SYNCHRONIZER MONITOR --- WAIT AFTER  --- {0}", this.CID);
                }
                return synchronizer_monitor;
            }
            set
            {
                synchronizer_monitor = value;
                synchronizer_monitor_initialized.Set();
                Console.WriteLine("SYNCHRONIZER MONITOR --- SET --- {0}", this.CID);
            }
        }

        private ManualResetEvent synchronizer_monitor_initialized = new ManualResetEvent(false);

        protected override void Dispose(bool disposing)
        {

            if (!disposed)
            {
                if (disposing)
                {
                    Console.WriteLine("DISPOSING BINDING ROOT ...");
                    foreach (int i in thread_receive_requests.Keys)
                        //for (int i=0; i<thread_receive_requests.Count; i++)
                        // * if (i != this.ThisFacetInstance)
                        thread_receive_requests[i].Abort();
                }
                base.Dispose(disposing);
            }
            //dispose unmanaged resources
            disposed = true;
        }

        #endregion

    }

    class SynchronizerMonitor
    {
        private int server_facet = default(int);

        private string instance_name = null;
        private IDictionary<int, Socket> client_socket_facet = new Dictionary<int, Socket>();
        private IDictionary<int, Socket> server_socket_facet_ack = new Dictionary<int, Socket>();
        private IDictionary<EnvelopKey, IDictionary<int, Queue<byte[]>>> reply_pending_list = new Dictionary<EnvelopKey, IDictionary<int, Queue<byte[]>>>();
        private IDictionary<EnvelopKey, IDictionary<int, Queue<AutoResetEvent>>> request_pending_list = new Dictionary<EnvelopKey, IDictionary<int, Queue<AutoResetEvent>>>();
        private IChannelImpl unit;

        private byte[] buffer_ack = new byte[1];

        private object sync = new object();

        public SynchronizerMonitor(IChannelImpl unit, IDictionary<int, Socket> client_socket_facet, IDictionary<int, Socket> server_socket_facet_ack, int server_facet, string instance_name)
        {
            this.unit = unit;
            this.client_socket_facet = client_socket_facet;
            this.server_socket_facet_ack = server_socket_facet_ack;
            this.server_facet = server_facet;

            this.instance_name = instance_name;
        }

        public byte[] clientSendRequest(EnvelopType envelop, byte[] messageSide1)
        {
            EnvelopKey envelop_key = new EnvelopKey(envelop);
            int envelop_tag = envelop.Item6;
            Console.WriteLine(server_facet + "/" + ": clientSendRequest 1" + " / " + envelop_key + " -- " + instance_name);

            byte[] messageSide2 = null;
            Monitor.Enter(sync);
            try
            {
                // envia a requisição para o root parceiro
                int facet = envelop.Item3;
                Console.WriteLine(server_facet + "/" + ": clientSendRequest send to facet " + facet + " - nofsockets=" + client_socket_facet.Count + " / " + envelop_key + " -- " + instance_name);
                foreach (int f in client_socket_facet.Keys)
                    Console.WriteLine(server_facet + "/" + ": clientSendRequest --- FACET KEY=" + f);
                Socket client_socket = client_socket_facet[facet];
                Socket server_socket_ack = server_socket_facet_ack[facet];
                byte[] messageSide1_enveloped_raw = ObjectToByteArray(new Tuple<EnvelopType, byte[]>(envelop, messageSide1));
                Int32 length = messageSide1_enveloped_raw.Length;
                byte[] messageSide1_enveloped_raw_ = new byte[4 + length];
                BitConverter.GetBytes(length).CopyTo(messageSide1_enveloped_raw_, 0);
                Array.Copy(messageSide1_enveloped_raw, 0, messageSide1_enveloped_raw_, 4, length);

                client_socket.Send(messageSide1_enveloped_raw_);

                Console.WriteLine(server_facet + "/" + ": clientSendRequest 2 nbytes=" + messageSide1_enveloped_raw.Length + " / " + envelop_key);

                // ACK
                // server_socket_ack.Receive(buffer_ack);

                Console.WriteLine(server_facet + "/" + ": clientSendRequest 2 ACK nbytes=" + messageSide1_enveloped_raw.Length + " / " + envelop_key);

                // Verifica se j� h� resposta para a requisi��o no "conjunto de respostas pendentes de requisi��o"
                if (!(reply_pending_list.ContainsKey(envelop_key) && reply_pending_list[envelop_key].ContainsKey(envelop_tag)) ||
                    (reply_pending_list.ContainsKey(envelop_key) && reply_pending_list[envelop_key].ContainsKey(envelop_tag) && reply_pending_list[envelop_key][envelop_tag].Count == 0))
                {
                    Console.WriteLine(server_facet + "/" + ": clientSendRequest 3 - BEFORE WAIT envelop_key={0} envelop_tag={1}", envelop_key, envelop_tag);
                    // Se n�o houver, coloca um item no "conjunto de requisi��es pendentes de resposta" e espera.

                    if (!request_pending_list.ContainsKey(envelop_key))
                        request_pending_list[envelop_key] = new Dictionary<int, Queue<AutoResetEvent>>();

                    if (!request_pending_list[envelop_key].ContainsKey(envelop_tag))
                    {
                        request_pending_list[envelop_key][envelop_tag] = new Queue<AutoResetEvent>();
                        request_pending_list[envelop_key][envelop_tag].Enqueue(new AutoResetEvent(false));
                    }

                    AutoResetEvent sync_send = request_pending_list[envelop_key][envelop_tag].Peek();

                    //request_pending_list [envelop_key][envelop_tag] = sync_send;
                    Monitor.Exit(sync);
                    Console.WriteLine("clientSendRequest - WAIT / " + unit.CID.getInstanceName() + "/" + sync_send.GetHashCode() + " BEFORE !!! ");
                    sync_send.WaitOne();
                    Console.WriteLine("clientSendRequest - WAIT / " + unit.CID.getInstanceName() + "/" + sync_send.GetHashCode() + " AFTER !!! ");
                    Monitor.Enter(sync);
                    Console.WriteLine(server_facet + "/" + ": clientSendRequest 3 - AFTER WAIT " + envelop_key);
                }
                Console.WriteLine(server_facet + "/" + ": clientSendRequest 4" + " / " + envelop_key);

                Queue<byte[]> pending_replies = reply_pending_list[envelop_key][envelop_tag];
                Console.WriteLine(server_facet + "/" + ": clientSendRequest 5 -- pending_replies.Count = " + pending_replies.Count);
                if (pending_replies.Count > 0)
                    messageSide2 = reply_pending_list[envelop_key][envelop_tag].Dequeue();

                if (pending_replies.Count == 0)
                    reply_pending_list[envelop_key].Remove(envelop_tag);

                if (reply_pending_list[envelop_key].Count == 0)
                    reply_pending_list.Remove(envelop_key);
            }
            catch (Exception e)
            {
                Console.WriteLine("{0}: EXCEPTION (clientSendRequest)", server_facet);
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                if (e.InnerException != null)
                {
                    Console.WriteLine(e.InnerException.Message);
                    Console.WriteLine(e.InnerException.StackTrace);
                }
            }
            finally
            {
                Monitor.Exit(sync);
            }

            Console.WriteLine(server_facet + "/" + ": clientSendRequest 5");
            // retorna a menagem ...
            return messageSide2;
        }

        public byte[] clientSendRequestAnyTag(EnvelopType envelop, byte[] messageSide1, ref int envelop_tag)
        {
            EnvelopKey envelop_key = new EnvelopKey(envelop);
            Console.WriteLine(server_facet + "/" + ": clientSendRequestAnyTag 1" + " / " + envelop_key + " -- " + instance_name);

            byte[] messageSide2 = null;
            Monitor.Enter(sync);
            try
            {
                // envia a requisi��o para o root parceiro
                int facet = envelop.Item3;
                Console.WriteLine(server_facet + "/" + ": clientSendRequestAnyTag send to facet " + facet + " - nofsockets=" + client_socket_facet.Count + " / " + envelop_key + " -- " + instance_name);
                foreach (int f in client_socket_facet.Keys)
                    Console.WriteLine(server_facet + "/" + ": clientSendRequestAnyTag --- FACET KEY=" + f);
                Socket client_socket = client_socket_facet[facet];
                Socket server_socket_ack = server_socket_facet_ack[facet];
                byte[] messageSide1_enveloped_raw = ObjectToByteArray(new Tuple<EnvelopType, byte[]>(envelop, messageSide1));
                Int32 length = messageSide1_enveloped_raw.Length;
                byte[] messageSide1_enveloped_raw_ = new byte[4 + length];
                BitConverter.GetBytes(length).CopyTo(messageSide1_enveloped_raw_, 0);
                Array.Copy(messageSide1_enveloped_raw, 0, messageSide1_enveloped_raw_, 4, length);

                client_socket.Send(messageSide1_enveloped_raw_);

                // ACK
                //server_socket_ack.Receive(buffer_ack);

                Console.WriteLine(server_facet + "/" + ": clientSendRequestAnyTag 2 nbytes=" + messageSide1_enveloped_raw.Length + " / " + envelop_key);

                // Verifica se j� h� resposta para a requisi��o no "conjunto de respostas pendentes de requisi��o"
                if (!reply_pending_list.ContainsKey(envelop_key))
                {
                    Console.WriteLine(server_facet + "/" + ": clientSendRequestAnyTag 3 - BEFORE WAIT " + envelop_key);
                    // Se n�o houver, coloca um item no "conjunto de requisi��es pendentes de resposta" e espera.

                    if (!request_pending_list.ContainsKey(envelop_key))
                        request_pending_list[envelop_key] = new Dictionary<int, Queue<AutoResetEvent>>();

                    if (!request_pending_list[envelop_key].ContainsKey(-1))
                    {
                        request_pending_list[envelop_key][-1] = new Queue<AutoResetEvent>();
                        request_pending_list[envelop_key][-1].Enqueue(new AutoResetEvent(false));
                    }

                    AutoResetEvent sync_send = request_pending_list[envelop_key][-1].Peek();

                    //request_pending_list [envelop_key][envelop_tag] = sync_send;
                    Monitor.Exit(sync);
                    Console.WriteLine("clientSendRequestAny - WAIT / " + unit.CID.getInstanceName() + "/" + sync_send.GetHashCode() + " BEFORE !!! ");
                    sync_send.WaitOne();
                    Console.WriteLine("clientSendRequestAny - WAIT / " + unit.CID.getInstanceName() + "/" + sync_send.GetHashCode() + " AFTER !!! ");
                    Monitor.Enter(sync);
                    Console.WriteLine(server_facet + "/" + ": clientSendRequestAnyTag 3 - AFTER WAIT " + envelop_key);
                }
                Console.WriteLine(server_facet + "/" + ": clientSendRequestAnyTag 4" + " / " + envelop_key);

                int[] keys_vector = new int[reply_pending_list[envelop_key].Keys.Count];
                reply_pending_list[envelop_key].Keys.CopyTo(keys_vector, 0);

                envelop_tag = keys_vector[0];
                Queue<byte[]> pending_replies = reply_pending_list[envelop_key][envelop_tag];
                Console.WriteLine(server_facet + "/" + ": clientSendRequestAnyTag 5 -- pending_replies.Count = " + pending_replies.Count);
                if (pending_replies.Count > 0)
                    messageSide2 = reply_pending_list[envelop_key][envelop_tag].Dequeue();

                if (pending_replies.Count == 0)
                    reply_pending_list[envelop_key].Remove(envelop_tag);

                if (reply_pending_list[envelop_key].Count == 0)
                    reply_pending_list.Remove(envelop_key);

                //reply_pending_list.Remove(envelop_key);
            }
            catch (Exception e)
            {
                Console.WriteLine("{0}: EXCEPTION (clientSendRequestAnyTag)", server_facet);
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                if (e.InnerException != null)
                {
                    Console.WriteLine(e.InnerException.Message);
                    Console.WriteLine(e.InnerException.StackTrace);
                }
            }
            finally
            {
                Monitor.Exit(sync);
            }

            Console.WriteLine(server_facet + "/" + ": clientSendRequest 5");
            // retorna a menagem ...
            return messageSide2;
        }

        private static int BUFFER_SIZE = 8192 * 8192;

        public void serverReceiveRequests(int facet, Socket client_socket_ack, Socket server_socket)
        {
            try
            {
                byte[] buffer = new byte[BUFFER_SIZE];
                byte[] buffer2 = new byte[BUFFER_SIZE];

                int nbytes = default(int);

                Console.WriteLine("serverReceiveRequest RECEIVE " + unit.CID.getInstanceName() + " / facet=" + facet + " BEFORE 1");
                nbytes = server_socket.Receive(buffer);
                Console.WriteLine("serverReceiveRequest RECEIVE " + unit.CID.getInstanceName() + " / facet=" + facet + " AFTER 1");

                Console.WriteLine(server_facet + "/" + ": serverReceiveRequests 1 - RECEIVED " + nbytes + " bytes -- " + instance_name);

                if (nbytes == 0)
                {
                    string error_message = server_facet + "/" + ": serverReceiveRequests  -- the partner " + this.server_facet + " is died " + instance_name;
                    Console.WriteLine(error_message);
                    throw new Exception(error_message);
                }

                while (true)
                {
                    while (nbytes < 4)
                    {
                        int nbytes2 = server_socket.Receive(buffer2);
                        Array.Copy(buffer2, 0, buffer, nbytes, nbytes2);
                        nbytes += nbytes2;
                    }

                    int length = BitConverter.ToInt32(buffer, 0);
                    nbytes = nbytes - 4;
                    byte[] message = new byte[length];

                    if (length > buffer.Length)
                    {
                        Console.WriteLine(server_facet + "/: serverReceiveRequests ADJUST buffer - from " + buffer.Length + " to " + (length * 2));

                        // adjust buffer size.
                        byte[] buffer_copy = buffer;
                        buffer = new byte[length * 2];
                        Array.Copy(buffer_copy, buffer, buffer_copy.Length);
                    }

                    Console.WriteLine(server_facet + "/: serverReceiveRequests 2 - length is " + length + " bytes" + " / nbytes = " + nbytes);

                    while (nbytes < length)
                    {
                        Console.WriteLine(server_facet + ": server_socket.Receive -- facet = " + facet);
                        int nbytes2 = server_socket.Receive(buffer2, length - nbytes, SocketFlags.None);
                        Console.WriteLine(server_facet + ": serverReceiveRequests 2 - LOOP - length is " + length + " bytes" + " / nbytes2 = " + nbytes2 + " / nbytes = " + nbytes + " facet=" + facet + ", " + server_socket.LocalEndPoint);
                        Array.Copy(buffer2, 0, buffer, nbytes + 4, nbytes2);
                        nbytes += nbytes2;
                    }

                    Console.WriteLine(server_facet + "/: serverReceiveRequests 2 - AFTER LOOP - BEGIN SEND ACK - length is " + length + " bytes" + " / nbytes = " + nbytes + " facet=" + facet + ", " + server_socket.LocalEndPoint);

                    // ACK
                    //client_socket_ack.Send(new byte[1]);

                    Console.WriteLine(server_facet + "/: serverReceiveRequests 2 - AFTER LOOP - END SEND ACK");

                    Array.Copy(buffer, 4, message, 0, length);
                    requestQueue.Add(message);

                    if (nbytes == length)
                    {
                        Console.WriteLine("serverReceiveRequest RECEIVE " + unit.CID.getInstanceName() + " / facet=" + facet + " BEFORE 2");
                        nbytes = server_socket.Receive(buffer);
                        Console.WriteLine("serverReceiveRequest RECEIVE " + unit.CID.getInstanceName() + " / facet=" + facet + " AFTER 2");
                        Console.WriteLine(server_facet + "/" + ": serverReceiveRequests 3 - RECEIVED " + nbytes + " bytes --- facet=" + facet + ", " + server_socket.LocalEndPoint + " " + instance_name);

                        if (nbytes == 0)
                        {
                            string error_message = server_facet + "/" + ": serverReceiveRequests  -- the partner " + this.server_facet + " is died " + instance_name;
                            Console.WriteLine(error_message);
                            throw new Exception(error_message);
                        }
                    }
                    else if (nbytes > length)
                    {
                        // assume that nbytes - length > 4
                        byte[] aux = buffer;
                        nbytes = nbytes - length;

                        Array.Copy(buffer, length + 4, buffer2, 0, nbytes);
                        buffer = buffer2;
                        buffer2 = aux;
                        Console.WriteLine(server_facet + "/" + ": serverReceiveRequests 4 - nbytes=" + nbytes);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("{0}: EXCEPTION in serverReceiveRequests - facet={1}", server_facet, facet);
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                if (e.InnerException != null)
                {
                    Console.WriteLine(e.InnerException.Message);
                    Console.WriteLine(e.InnerException.StackTrace);
                }
            }
        }

        private ProducerConsumerQueue<byte[]> requestQueue = new ProducerConsumerQueue<byte[]>();

        public void serverReadRequest()
        {
            Console.WriteLine(server_facet + "/" + ": serverReadRequest 1 ");

            byte[] buffer = requestQueue.Take();
            int nbytes = buffer.Length;

            Console.WriteLine(server_facet + "/" + ": serverReadRequest 2 " + nbytes + " bytes received.");

            Monitor.Enter(sync);
            try
            {
                // Aguarda uma resposta proveniente do outro root parceiro.
                byte[] messageSide1_enveloped_raw = new byte[nbytes];
                Console.WriteLine(server_facet + "/" + ": serverReadRequest 2-1 nbytes=" + nbytes);

                // TODO: otimizar isso ...
                //for (int i=0; i<nbytes; i++)
                //  messageSide1_enveloped_raw[i] = buffer[i];

                Array.Copy(buffer, 0, messageSide1_enveloped_raw, 0, nbytes);

                Tuple<EnvelopType, byte[]> messageSide1_enveloped = (Tuple<EnvelopType, byte[]>)ByteArrayToObject(messageSide1_enveloped_raw);

                EnvelopType envelop = messageSide1_enveloped.Item1;
                EnvelopKey envelop_key = new EnvelopKey(envelop);
                int envelop_tag = envelop.Item6;

                // Coloca a resposta no "conjunto de respostas pendentes de requisi��o"
                if (!reply_pending_list.ContainsKey(envelop_key))
                    reply_pending_list[envelop_key] = new Dictionary<int, Queue<byte[]>>();

                if (!reply_pending_list[envelop_key].ContainsKey(envelop_tag))
                    reply_pending_list[envelop_key][envelop_tag] = new Queue<byte[]>();

                reply_pending_list[envelop_key][envelop_tag].Enqueue(messageSide1_enveloped.Item2);

                Console.WriteLine(server_facet + "/" + ": serverReadRequest 3 " + envelop.Item1 + "," + envelop_key);
                foreach (EnvelopKey ek in request_pending_list.Keys)
                    Console.WriteLine(server_facet + ": key: " + ek);

                // Busca, no "conjunto de requisi��es pendentes de resposta", a requisi��o correspondente a resposta.
                if (request_pending_list.ContainsKey(envelop_key) && request_pending_list[envelop_key].ContainsKey(envelop_tag))
                {
                    Console.WriteLine(server_facet + "/" + ": serverReadRequest 3-1" + " / " + envelop_key);
                    AutoResetEvent sync_send = request_pending_list[envelop_key][envelop_tag].Dequeue();

                    sync_send.Set();

                    if (request_pending_list[envelop_key][envelop_tag].Count == 0)
                        request_pending_list[envelop_key].Remove(envelop_tag);

                    if (request_pending_list[envelop_key].Count == 0)
                        request_pending_list.Remove(envelop_key);

                    Console.WriteLine(server_facet + "/" + ": serverReadRequest 3-2" + " / " + envelop_key);
                }
                else if (request_pending_list.ContainsKey(envelop_key) && request_pending_list[envelop_key].ContainsKey(-1))
                {
                    Console.WriteLine(server_facet + "/" + ": serverReadRequest 3-11" + " / " + envelop_key);
                    AutoResetEvent sync_send = request_pending_list[envelop_key][-1].Dequeue();
                    //Monitor.Pulse (sync_send);
                    sync_send.Set();

                    if (request_pending_list[envelop_key][-1].Count == 0)
                        request_pending_list[envelop_key].Remove(-1);

                    if (request_pending_list[envelop_key].Count == 0)
                        request_pending_list.Remove(envelop_key);

                    Console.WriteLine(server_facet + "/" + ": serverReadRequest 3-21" + " / " + envelop_key);
                }
            }
            finally
            {
                Monitor.Exit(sync);
            }
            Console.WriteLine(server_facet + "/" + ": serverReadRequest 4");
        }

        // Convert an object to a byte array
        private static byte[] ObjectToByteArray(Object obj)
        {
            if (obj == null)
                return null;
            BinaryFormatter bf = new BinaryFormatter();
            MemoryStream ms = new MemoryStream();
            bf.Serialize(ms, obj);
            return ms.ToArray();
        }

        // Convert a byte array to an Object
        private static Object ByteArrayToObject(byte[] arrBytes)
        {
            MemoryStream memStream = new MemoryStream();
            BinaryFormatter binForm = new BinaryFormatter();
            memStream.Write(arrBytes, 0, arrBytes.Length);
            memStream.Seek(0, SeekOrigin.Begin);
            Object obj = (Object)binForm.Deserialize(memStream);
            return obj;
        }
    }

    class EnvelopKey
    {
        private EnvelopType envelop = null;
        public EnvelopKey(EnvelopType envelop)
        {
            this.envelop = envelop;
        }

        public override string ToString()
        {
            string key = base.ToString();
            switch (envelop.Item1)
            {
                case AliencommunicatorOperation.SEND:
                case AliencommunicatorOperation.SYNC_SEND:
                case AliencommunicatorOperation.SEND_ARRAY:
                    //              key = string.Format ("SR-{0}-{1}-{2}-{3}-{4}",envelop.Item2, envelop.Item3, envelop.Item4, envelop.Item5, envelop.Item6);
                    key = string.Format("SR-{0}-{1}-{2}-{3}", envelop.Item2, envelop.Item3, envelop.Item4, envelop.Item5);
                    break;
                case AliencommunicatorOperation.RECEIVE:
                case AliencommunicatorOperation.RECEIVE_ARRAY:
                    //              key = string.Format ("SR-{1}-{0}-{3}-{2}-{4}",envelop.Item2, envelop.Item3, envelop.Item4, envelop.Item5, envelop.Item6);
                    key = string.Format("SR-{1}-{0}-{3}-{2}", envelop.Item2, envelop.Item3, envelop.Item4, envelop.Item5);
                    break;
                case AliencommunicatorOperation.PROBE:
                    break;
                case AliencommunicatorOperation.ALL_GATHER:
                    break;
                case AliencommunicatorOperation.ALL_GATHER_FLATTENED:
                    break;
                case AliencommunicatorOperation.ALL_REDUCE:
                    break;
                case AliencommunicatorOperation.ALL_REDUCE_ARRAY:
                    break;
                case AliencommunicatorOperation.ALL_TO_ALL:
                    break;
                case AliencommunicatorOperation.ALL_TO_ALL_FLATTENED:
                    break;
                case AliencommunicatorOperation.REDUCE_SCATTER:
                    break;
                case AliencommunicatorOperation.BROADCAST:
                    break;
                case AliencommunicatorOperation.BROADCAST_ARRAY:
                    break;
                case AliencommunicatorOperation.SCATTER:
                    break;
                case AliencommunicatorOperation.SCATTER_FROM_FLATTENED:
                    break;
                case AliencommunicatorOperation.GATHER:
                    break;
                case AliencommunicatorOperation.GATHER_FLATTENED:
                    break;
                case AliencommunicatorOperation.REDUCE:
                    break;
                case AliencommunicatorOperation.REDUCE_ARRAY:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            return key;
        }

        public override bool Equals(object obj)
        {
            EnvelopKey fooItem = obj as EnvelopKey;

            return fooItem.ToString().Equals(this.ToString());
        }

        public override int GetHashCode()
        {
            return this.ToString().GetHashCode();
        }

    }

    public class ProducerConsumerQueue<T> : BlockingCollection<T>
    {
        /// <summary>
        /// Initializes a new instance of the ProducerConsumerQueue, Use Add and TryAdd for Enqueue and TryEnqueue and Take and TryTake for Dequeue and TryDequeue functionality
        /// </summary>
        public ProducerConsumerQueue()
            : base(new ConcurrentQueue<T>())
        {
        }

        /// <summary>
        /// Initializes a new instance of the ProducerConsumerQueue, Use Add and TryAdd for Enqueue and TryEnqueue and Take and TryTake for Dequeue and TryDequeue functionality
        /// </summary>
        /// <param name="maxSize"></param>
        public ProducerConsumerQueue(int maxSize)
            : base(new ConcurrentQueue<T>(), maxSize)
        {
        }
    }

    public class SAFeStatus : Status
    {

        /// <summary>
        ///   Constructs a <code>Status</code> object from a low-level <see cref="Unsafe.MPI_Status"/> structure.
        /// </summary>
        internal SAFeStatus(Tuple<int, int> source, int tag)
        {
            this.source = source;
            this.tag = tag;
        }

        Tuple<int, int> source;

        /// <summary>
        /// The rank of the process that sent the message.
        /// </summary>
        public Tuple<int, int> Source
        {
            get
            {
                return source;
            }
        }

        int tag;

        /// <summary>
        /// The tag used to send the message.
        /// </summary>
        public int Tag
        {
            get
            {
                return tag;
            }
        }

        /// <summary>
        /// Determine the number of elements transmitted by the communication
        /// operation associated with this object.
        /// </summary>
        /// <param name="type">
        ///   The type of data that will be stored in the message.
        /// </param>
        /// <returns>
        ///   If the type of the data is a value type, returns the number
        ///   of elements in the message. Otherwise, returns <c>null</c>,
        ///   because the number of elements stored in the message won't
        ///   be known until the message is received.
        /// </returns>
        public int? Count(Type type)
        {
            // TODO:
            return null;
        }

        /// <summary>
        /// Whether the communication was cancelled before it completed.
        /// </summary>
        public bool Cancelled
        {
            get
            {
                // TODO: cancel functionality not implemented ...
                return false;
            }
        }
    }

    /// <summary>
    /// Information about a specific message that has already been
    /// transferred via MPI.
    /// </summary>
    public class SAFeCompletedStatus : SAFeStatus, CompletedStatus
    {
        /// <summary>
        ///   Constructs a <code>Status</code> object from a low-level <see cref="Unsafe.MPI_Status"/> structure
        ///   and a count of the number of elements received.
        /// </summary>
        internal SAFeCompletedStatus(Tuple<int, int> source, int tag, int count) : base(source, tag)
        {
            this.count = count;
        }

        internal SAFeCompletedStatus(Tuple<int, int> source, int tag) : base(source, tag)
        {
        }

        public static CompletedStatus createStatus(Tuple<int, int> source, int tag, int count)
        {
            return new SAFeCompletedStatus(source, tag, count);
        }

        public static CompletedStatus createStatus(Tuple<int, int> source, int tag)
        {
            return new SAFeCompletedStatus(source, tag);
        }

        private int? count = null;

        /// <summary>
        /// Determines the number of elements in the transmitted message.
        /// </summary>
        public int? Count
        {
            get { return count; }
            set { count = value; }
        }

    }

    public class Request : IRequest // Without MPI
    {
        private Tuple<int, int> source;
        int tag;

        private ManualResetEvent e;
        private Thread waiting_request = null;
        protected ManualResetEvent initial_signal = new ManualResetEvent(false);
        private bool completed = false;

        bool completed_request = false;


        internal Request(ManualResetEvent e, Tuple<int, int> source, int tag)
        {
            this.e = e;
            this.source = source;
            this.tag = tag;
            new Thread(new ThreadStart(delegate ()
            {
                e.WaitOne();
                completed_request = true;
            })).Start();

        }


        private object wait_lock = new object();

        private IList<AutoResetEvent> waiting_sets = new List<AutoResetEvent>();

        public void registerWaitingSet(AutoResetEvent waiting_set)
        {
            waiting_request = new Thread(new ThreadStart(delegate
            {
                while (true)
                {
                    initial_signal.WaitOne();
                    e.WaitOne();
                    lock (wait_lock)
                    {
                        completed = true;
                        foreach (AutoResetEvent ws in waiting_sets)
                            ws.Set();
                    }
                }
            }));

            waiting_request.Start();

            lock (wait_lock)
            {
                if (completed)
                    waiting_set.Set();
                else
                {
                    waiting_sets.Add(waiting_set);
                    initial_signal.Set();
                }
            }
        }

        public void unregisterWaitingSet(AutoResetEvent waiting_set)
        {
            waiting_sets.Remove(waiting_set);
            initial_signal.Reset();
            waiting_request.Abort();
            waiting_request = null;
        }

        /// <summary>
        /// Wait until this non-blocking operation has completed.
        /// </summary>
        /// <returns>
        ///   Information about the completed communication operation.
        /// </returns>
        public CompletedStatus Wait()
        {
            e.WaitOne();
            return SAFeCompletedStatus.createStatus(source, tag);
        }

        /// <summary>
        /// Determine whether this non-blocking operation has completed.
        /// </summary>
        /// <returns>
        /// If the non-blocking operation has completed, returns information
        /// about the completed communication operation. Otherwise, returns
        /// <c>null</c> to indicate that the operation has not completed.
        /// </returns>
        public CompletedStatus Test()
        {
            return completed_request ? SAFeCompletedStatus.createStatus(source, tag) : null;
        }

        /// <summary>
        /// Cancel this communication request.
        /// </summary>
        public void Cancel()
        {
            // TODO: cancel functionality not implemented ...
            //internal_request.Cancel ();
        }

        public static IRequest createRequest(ManualResetEvent e, Tuple<int, int> source, int tag)
        {
            return new Request(e, source, tag);
        }

        // Convert an object to a byte array
        protected static byte[] ObjectToByteArray(Object obj)
        {
            if (obj == null)
                return null;
            BinaryFormatter bf = new BinaryFormatter();
            MemoryStream ms = new MemoryStream();
            bf.Serialize(ms, obj);
            return ms.ToArray();
        }

        protected static Object ByteArrayToObject(byte[] arrBytes)
        {
            MemoryStream memStream = new MemoryStream();
            BinaryFormatter binForm = new BinaryFormatter();
            memStream.Write(arrBytes, 0, arrBytes.Length);
            memStream.Seek(0, SeekOrigin.Begin);
            Object obj = (Object)binForm.Deserialize(memStream);
            return obj;
        }

    }


    /// <summary>
    /// A non-blocking receive request. 
    /// </summary>
    /// <remarks>
    /// This class allows one to test a receive
    /// request for completion, wait for completion of a request, cancel a request,
    /// or extract the value received by this communication request.
    /// </remarks>
    public class ValueReceiveRequest<T> : ReceiveRequest
    {
        Tuple<int, int> source = null;
        int? tag = null;

        internal ValueReceiveRequest(ManualResetEvent e, Tuple<int, int> source, int tag) : base(e, source, tag)
        {
            this.source = source;
            this.tag = tag;
        }

        private object value = null;

        /// <summary>
        /// Retrieve the value received via this communication. The value
        /// will only be available when the communication has completed.
        /// </summary>
        /// <returns>The value received by this communication.</returns>
        public override object GetValue()
        {
            return value;
        }

        public void SetValue(byte[] v)
        {
            value = (T)ByteArrayToObject(v);
        }

        public static new ValueReceiveRequest<T> createRequest(ManualResetEvent e, Tuple<int, int> source, int tag)
        {
            return new ValueReceiveRequest<T>(e, source, tag);
        }

        public new Binding.CompletedStatus Wait()
        {
            SAFeCompletedStatus status = (SAFeCompletedStatus)base.Wait();
            status.Count = 1;
            return status;
        }

        public new Binding.CompletedStatus Test()
        {
            SAFeCompletedStatus status = (SAFeCompletedStatus)base.Test();

            if (status != null)
                status.Count = 1;

            return status;
        }
    }

    public abstract class ReceiveRequest : Request, IReceiveRequest
    {
        public ReceiveRequest(ManualResetEvent e, Tuple<int, int> source, int tag) : base(e, source, tag)
        {
        }
        /// <summary>
        /// Retrieve the value received via this communication. The value
        /// will only be available when the communication has completed.
        /// </summary>
        /// <returns>The value received by this communication.</returns>
        public abstract object GetValue();
    }




}
