/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.platform.locale.northamerica.US_East;

namespace br.ufc.mdcc.hpcshelf.platform.locale.northamerica.Virginia
{
	public interface BaseIVirginia : BaseIUS_East, IQualifierKind 
	{
	}
}