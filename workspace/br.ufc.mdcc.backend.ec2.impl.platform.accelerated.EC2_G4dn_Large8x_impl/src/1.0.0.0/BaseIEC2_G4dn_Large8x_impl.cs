/* Automatically Generated Code */

using br.ufc.mdcc.backend.ec2.platform.accelerated.EC2_G4dn_Large8x;
using br.ufc.mdcc.backend.ec2.family.Acceleration;
using br.ufc.mdcc.backend.ec2.type.accelerated.G4dn;
using br.ufc.mdcc.backend.ec2.size.Large8x;
using br.ufc.mdcc.hpcshelf.platform.maintainer.EC2;
using br.ufc.mdcc.hpcshelf.quantifier.IntUp;

namespace br.ufc.mdcc.backend.ec2.impl.platform.accelerated.EC2_G4dn_Large8x_impl 
{
	public abstract class BaseIEC2_G4dn_Large8x_impl<N>: br.ufc.pargo.hpe.kinds.Environment, BaseIEC2_G4dn_Large8x<N>
		where N:IntUp
	{
		private IEC2TypeG4dn type = null;

		protected IEC2TypeG4dn Type
		{
			get
			{
				if (this.type == null)
					this.type = (IEC2TypeG4dn) Services.getPort("type");
				return this.type;
			}
		}
		private IEC2SizeLarge8x size = null;

		protected IEC2SizeLarge8x Size
		{
			get
			{
				if (this.size == null)
					this.size = (IEC2SizeLarge8x) Services.getPort("size");
				return this.size;
			}
		}
		private IEC2FamilyAcceleration family = null;

		protected IEC2FamilyAcceleration Family
		{
			get
			{
				if (this.family == null)
					this.family = (IEC2FamilyAcceleration) Services.getPort("family");
				return this.family;
			}
		}
		private IEC2 maintainer = null;

		protected IEC2 Maintainer
		{
			get
			{
				if (this.maintainer == null)
					this.maintainer = (IEC2) Services.getPort("maintainer");
				return this.maintainer;
			}
		}
	}
}