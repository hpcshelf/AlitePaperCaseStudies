/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.platform.locale.oceania.Australia;

namespace br.ufc.mdcc.hpcshelf.platform.locale.oceania.Sydney
{
	public interface BaseISydney : BaseIAustralia, IQualifierKind 
	{
	}
}