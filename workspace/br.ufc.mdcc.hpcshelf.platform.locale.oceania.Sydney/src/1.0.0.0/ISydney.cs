using br.ufc.mdcc.hpcshelf.platform.locale.oceania.Australia;

namespace br.ufc.mdcc.hpcshelf.platform.locale.oceania.Sydney
{
	public interface ISydney : BaseISydney, IAustralia
	{
	}
}