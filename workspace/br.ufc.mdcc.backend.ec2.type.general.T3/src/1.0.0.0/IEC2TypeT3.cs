using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.backend.ec2.type.Type;

namespace br.ufc.mdcc.backend.ec2.type.general.T3
{
	public interface IEC2TypeT3 : BaseIEC2TypeT3, IEC2Type<family.GeneralPurpose.IEC2FamilyGeneralPurpose>
	{
	}
}