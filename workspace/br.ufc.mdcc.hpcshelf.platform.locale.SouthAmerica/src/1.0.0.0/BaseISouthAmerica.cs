/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.platform.locale.AnyWhere;

namespace br.ufc.mdcc.hpcshelf.platform.locale.SouthAmerica
{
	public interface BaseISouthAmerica : BaseIAnyWhere, IQualifierKind 
	{
	}
}