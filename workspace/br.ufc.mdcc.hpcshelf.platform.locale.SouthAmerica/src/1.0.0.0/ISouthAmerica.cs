using br.ufc.mdcc.hpcshelf.platform.locale.AnyWhere;

namespace br.ufc.mdcc.hpcshelf.platform.locale.SouthAmerica
{
	public interface ISouthAmerica : BaseISouthAmerica, IAnyWhere
	{
	}
}