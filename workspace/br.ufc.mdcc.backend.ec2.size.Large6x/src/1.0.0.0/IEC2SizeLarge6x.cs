using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.backend.ec2.size.Size;

namespace br.ufc.mdcc.backend.ec2.size.Large6x
{
	public interface IEC2SizeLarge6x : BaseIEC2SizeLarge6x, IEC2Size
	{
	}
}