/* Automatically Generated Code */

using br.ufc.mdcc.backend.ec2.platform.compute.EC2_C5n_Large1x;
using br.ufc.mdcc.backend.ec2.family.Computation;
using br.ufc.mdcc.backend.ec2.type.compute.C5n;
using br.ufc.mdcc.backend.ec2.size.Large1x;
using br.ufc.mdcc.hpcshelf.platform.maintainer.EC2;
using br.ufc.mdcc.hpcshelf.quantifier.IntUp;

namespace br.ufc.mdcc.backend.ec2.impl.platform.compute.EC2_C5n_Large1x_impl 
{
	public abstract class BaseIEC2_C5n_Large1x_impl<N>: br.ufc.pargo.hpe.kinds.Environment, BaseIEC2_C5n_Large1x<N>
		where N:IntUp
	{
		private IEC2TypeC5n type = null;

		protected IEC2TypeC5n Type
		{
			get
			{
				if (this.type == null)
					this.type = (IEC2TypeC5n) Services.getPort("type");
				return this.type;
			}
		}
		private IEC2SizeLarge1x size = null;

		protected IEC2SizeLarge1x Size
		{
			get
			{
				if (this.size == null)
					this.size = (IEC2SizeLarge1x) Services.getPort("size");
				return this.size;
			}
		}
		private IEC2FamilyComputation family = null;

		protected IEC2FamilyComputation Family
		{
			get
			{
				if (this.family == null)
					this.family = (IEC2FamilyComputation) Services.getPort("family");
				return this.family;
			}
		}
		private IEC2 maintainer = null;

		protected IEC2 Maintainer
		{
			get
			{
				if (this.maintainer == null)
					this.maintainer = (IEC2) Services.getPort("maintainer");
				return this.maintainer;
			}
		}
	}
}