#!/bin/sh

./erase_projectGuid.sh GeneralPurpose general A1 Medium
./erase_projectGuid.sh GeneralPurpose general A1 Large
./erase_projectGuid.sh GeneralPurpose general A1 Large1x
./erase_projectGuid.sh GeneralPurpose general A1 Large2x
./erase_projectGuid.sh GeneralPurpose general A1 Large4x
./erase_projectGuid.sh GeneralPurpose general A1 Metal

./erase_projectGuid.sh GeneralPurpose general T3 Nano
./erase_projectGuid.sh GeneralPurpose general T3 Micro
./erase_projectGuid.sh GeneralPurpose general T3 Small
./erase_projectGuid.sh GeneralPurpose general T3 Medium
./erase_projectGuid.sh GeneralPurpose general T3 Large
./erase_projectGuid.sh GeneralPurpose general T3 Large1x
./erase_projectGuid.sh GeneralPurpose general T3 Large2x

./erase_projectGuid.sh GeneralPurpose general T3a Nano
./erase_projectGuid.sh GeneralPurpose general T3a Micro
./erase_projectGuid.sh GeneralPurpose general T3a Small
./erase_projectGuid.sh GeneralPurpose general T3a Medium
./erase_projectGuid.sh GeneralPurpose general T3a Large
./erase_projectGuid.sh GeneralPurpose general T3a Large1x
./erase_projectGuid.sh GeneralPurpose general T3a Large2x

./erase_projectGuid.sh GeneralPurpose general T2 Nano
./erase_projectGuid.sh GeneralPurpose general T2 Micro
./erase_projectGuid.sh GeneralPurpose general T2 Small
./erase_projectGuid.sh GeneralPurpose general T2 Medium
./erase_projectGuid.sh GeneralPurpose general T2 Large
./erase_projectGuid.sh GeneralPurpose general T2 Large1x
./erase_projectGuid.sh GeneralPurpose general T2 Large2x

./erase_projectGuid.sh GeneralPurpose general M5 Large
./erase_projectGuid.sh GeneralPurpose general M5 Large1x
./erase_projectGuid.sh GeneralPurpose general M5 Large2x
./erase_projectGuid.sh GeneralPurpose general M5 Large4x
./erase_projectGuid.sh GeneralPurpose general M5 Large8x
./erase_projectGuid.sh GeneralPurpose general M5 Large12x
./erase_projectGuid.sh GeneralPurpose general M5 Large16x
./erase_projectGuid.sh GeneralPurpose general M5 Large24x
./erase_projectGuid.sh GeneralPurpose general M5 Metal

./erase_projectGuid.sh GeneralPurpose general M5d Large
./erase_projectGuid.sh GeneralPurpose general M5d Large1x
./erase_projectGuid.sh GeneralPurpose general M5d Large2x
./erase_projectGuid.sh GeneralPurpose general M5d Large4x
./erase_projectGuid.sh GeneralPurpose general M5d Large8x
./erase_projectGuid.sh GeneralPurpose general M5d Large12x
./erase_projectGuid.sh GeneralPurpose general M5d Large16x
./erase_projectGuid.sh GeneralPurpose general M5d Large24x
./erase_projectGuid.sh GeneralPurpose general M5d Metal

./erase_projectGuid.sh GeneralPurpose general M5a Large
./erase_projectGuid.sh GeneralPurpose general M5a Large1x
./erase_projectGuid.sh GeneralPurpose general M5a Large2x
./erase_projectGuid.sh GeneralPurpose general M5a Large4x
./erase_projectGuid.sh GeneralPurpose general M5a Large8x
./erase_projectGuid.sh GeneralPurpose general M5a Large12x
./erase_projectGuid.sh GeneralPurpose general M5a Large16x
./erase_projectGuid.sh GeneralPurpose general M5a Large24x

./erase_projectGuid.sh GeneralPurpose general M5ad Large
./erase_projectGuid.sh GeneralPurpose general M5ad Large1x
./erase_projectGuid.sh GeneralPurpose general M5ad Large2x
./erase_projectGuid.sh GeneralPurpose general M5ad Large4x
./erase_projectGuid.sh GeneralPurpose general M5ad Large8x
./erase_projectGuid.sh GeneralPurpose general M5ad Large12x
./erase_projectGuid.sh GeneralPurpose general M5ad Large16x
./erase_projectGuid.sh GeneralPurpose general M5ad Large24x

./erase_projectGuid.sh GeneralPurpose general M5n Large
./erase_projectGuid.sh GeneralPurpose general M5n Large1x
./erase_projectGuid.sh GeneralPurpose general M5n Large2x
./erase_projectGuid.sh GeneralPurpose general M5n Large4x
./erase_projectGuid.sh GeneralPurpose general M5n Large8x
./erase_projectGuid.sh GeneralPurpose general M5n Large12x
./erase_projectGuid.sh GeneralPurpose general M5n Large16x
./erase_projectGuid.sh GeneralPurpose general M5n Large24x

./erase_projectGuid.sh GeneralPurpose general M5dn Large
./erase_projectGuid.sh GeneralPurpose general M5dn Large1x
./erase_projectGuid.sh GeneralPurpose general M5dn Large2x
./erase_projectGuid.sh GeneralPurpose general M5dn Large4x
./erase_projectGuid.sh GeneralPurpose general M5dn Large8x
./erase_projectGuid.sh GeneralPurpose general M5dn Large12x
./erase_projectGuid.sh GeneralPurpose general M5dn Large16x
./erase_projectGuid.sh GeneralPurpose general M5dn Large24x

./erase_projectGuid.sh GeneralPurpose general M4 Large
./erase_projectGuid.sh GeneralPurpose general M4 Large1x
./erase_projectGuid.sh GeneralPurpose general M4 Large2x
./erase_projectGuid.sh GeneralPurpose general M4 Large4x
./erase_projectGuid.sh GeneralPurpose general M4 Large10x
./erase_projectGuid.sh GeneralPurpose general M4 Large16x

./erase_projectGuid.sh Computation compute C5 Large
./erase_projectGuid.sh Computation compute C5 Large1x
./erase_projectGuid.sh Computation compute C5 Large2x
./erase_projectGuid.sh Computation compute C5 Large4x
./erase_projectGuid.sh Computation compute C5 Large9x
./erase_projectGuid.sh Computation compute C5 Large12x
./erase_projectGuid.sh Computation compute C5 Large18x
./erase_projectGuid.sh Computation compute C5 Large24x
./erase_projectGuid.sh Computation compute C5 Metal

./erase_projectGuid.sh Computation compute C5d Large
./erase_projectGuid.sh Computation compute C5d Large1x
./erase_projectGuid.sh Computation compute C5d Large2x
./erase_projectGuid.sh Computation compute C5d Large4x
./erase_projectGuid.sh Computation compute C5d Large9x
./erase_projectGuid.sh Computation compute C5d Large12x
./erase_projectGuid.sh Computation compute C5d Large18x
./erase_projectGuid.sh Computation compute C5d Large24x
./erase_projectGuid.sh Computation compute C5d Metal

./erase_projectGuid.sh Computation compute C5n Large
./erase_projectGuid.sh Computation compute C5n Large1x
./erase_projectGuid.sh Computation compute C5n Large2x
./erase_projectGuid.sh Computation compute C5n Large4x
./erase_projectGuid.sh Computation compute C5n Large9x
./erase_projectGuid.sh Computation compute C5n Large18x
./erase_projectGuid.sh Computation compute C5n Metal

./erase_projectGuid.sh Computation compute C4 Large
./erase_projectGuid.sh Computation compute C4 Large1x
./erase_projectGuid.sh Computation compute C4 Large2x
./erase_projectGuid.sh Computation compute C4 Large4x
./erase_projectGuid.sh Computation compute C4 Large8x

./erase_projectGuid.sh Memory memory R5 Large
./erase_projectGuid.sh Memory memory R5 Large1x
./erase_projectGuid.sh Memory memory R5 Large2x
./erase_projectGuid.sh Memory memory R5 Large4x
./erase_projectGuid.sh Memory memory R5 Large8x
./erase_projectGuid.sh Memory memory R5 Large12x
./erase_projectGuid.sh Memory memory R5 Large16x
./erase_projectGuid.sh Memory memory R5 Large24x
./erase_projectGuid.sh Memory memory R5 Metal

./erase_projectGuid.sh Memory memory R5d Large
./erase_projectGuid.sh Memory memory R5d Large1x
./erase_projectGuid.sh Memory memory R5d Large2x
./erase_projectGuid.sh Memory memory R5d Large4x
./erase_projectGuid.sh Memory memory R5d Large8x
./erase_projectGuid.sh Memory memory R5d Large12x
./erase_projectGuid.sh Memory memory R5d Large16x
./erase_projectGuid.sh Memory memory R5d Large24x
./erase_projectGuid.sh Memory memory R5d Metal

./erase_projectGuid.sh Memory memory R5a Large
./erase_projectGuid.sh Memory memory R5a Large1x
./erase_projectGuid.sh Memory memory R5a Large2x
./erase_projectGuid.sh Memory memory R5a Large4x
./erase_projectGuid.sh Memory memory R5a Large8x
./erase_projectGuid.sh Memory memory R5a Large12x
./erase_projectGuid.sh Memory memory R5a Large16x
./erase_projectGuid.sh Memory memory R5a Large24x

./erase_projectGuid.sh Memory memory R5ad Large
./erase_projectGuid.sh Memory memory R5ad Large1x
./erase_projectGuid.sh Memory memory R5ad Large2x
./erase_projectGuid.sh Memory memory R5ad Large4x
./erase_projectGuid.sh Memory memory R5ad Large12x
./erase_projectGuid.sh Memory memory R5ad Large24x

./erase_projectGuid.sh Memory memory R5n Large
./erase_projectGuid.sh Memory memory R5n Large1x
./erase_projectGuid.sh Memory memory R5n Large2x
./erase_projectGuid.sh Memory memory R5n Large4x
./erase_projectGuid.sh Memory memory R5n Large8x
./erase_projectGuid.sh Memory memory R5n Large12x
./erase_projectGuid.sh Memory memory R5n Large16x
./erase_projectGuid.sh Memory memory R5n Large24x

./erase_projectGuid.sh Memory memory R5dn Large
./erase_projectGuid.sh Memory memory R5dn Large1x
./erase_projectGuid.sh Memory memory R5dn Large2x
./erase_projectGuid.sh Memory memory R5dn Large4x
./erase_projectGuid.sh Memory memory R5dn Large8x
./erase_projectGuid.sh Memory memory R5dn Large12x
./erase_projectGuid.sh Memory memory R5dn Large16x
./erase_projectGuid.sh Memory memory R5dn Large24x

./erase_projectGuid.sh Memory memory R4 Large
./erase_projectGuid.sh Memory memory R4 Large1x
./erase_projectGuid.sh Memory memory R4 Large2x
./erase_projectGuid.sh Memory memory R4 Large4x
./erase_projectGuid.sh Memory memory R4 Large8x
./erase_projectGuid.sh Memory memory R4 Large16x

./erase_projectGuid.sh Memory memory X1e Large1x
./erase_projectGuid.sh Memory memory X1e Large2x
./erase_projectGuid.sh Memory memory X1e Large4x
./erase_projectGuid.sh Memory memory X1e Large8x
./erase_projectGuid.sh Memory memory X1e Large16x
./erase_projectGuid.sh Memory memory X1e Large32x

./erase_projectGuid.sh Memory memory X1 Large16x
./erase_projectGuid.sh Memory memory X1 Large32x

./erase_projectGuid.sh Memory memory HighMemory Metal_u_6tb1
./erase_projectGuid.sh Memory memory HighMemory Metal_u_9tb1
./erase_projectGuid.sh Memory memory HighMemory Metal_u_12tb1
./erase_projectGuid.sh Memory memory HighMemory Metal_u_18tb1
./erase_projectGuid.sh Memory memory HighMemory Metal_u_24tb1

./erase_projectGuid.sh Memory memory Z1d Large
./erase_projectGuid.sh Memory memory Z1d Large1x
./erase_projectGuid.sh Memory memory Z1d Large2x
./erase_projectGuid.sh Memory memory Z1d Large3x
./erase_projectGuid.sh Memory memory Z1d Large6x
./erase_projectGuid.sh Memory memory Z1d Large12x
./erase_projectGuid.sh Memory memory Z1d Metal

./erase_projectGuid.sh Acceleration accelerated P3 Large2x
./erase_projectGuid.sh Acceleration accelerated P3 Large8x
./erase_projectGuid.sh Acceleration accelerated P3 Large16x
./erase_projectGuid.sh Acceleration accelerated P3 Large24x

./erase_projectGuid.sh Acceleration accelerated P2 Large1x
./erase_projectGuid.sh Acceleration accelerated P2 Large8x
./erase_projectGuid.sh Acceleration accelerated P2 Large16x

./erase_projectGuid.sh Acceleration accelerated G4dn Large1x
./erase_projectGuid.sh Acceleration accelerated G4dn Large2x
./erase_projectGuid.sh Acceleration accelerated G4dn Large4x
./erase_projectGuid.sh Acceleration accelerated G4dn Large8x
./erase_projectGuid.sh Acceleration accelerated G4dn Large16x
./erase_projectGuid.sh Acceleration accelerated G4dn Large12x
./erase_projectGuid.sh Acceleration accelerated G4dn Metal

./erase_projectGuid.sh Acceleration accelerated G3 Large1x
./erase_projectGuid.sh Acceleration accelerated G3 Large4x
./erase_projectGuid.sh Acceleration accelerated G3 Large8x
./erase_projectGuid.sh Acceleration accelerated G3 Large16x

./erase_projectGuid.sh Acceleration accelerated F1 Large2x
./erase_projectGuid.sh Acceleration accelerated F1 Large4x
./erase_projectGuid.sh Acceleration accelerated F1 Large16x

./erase_projectGuid.sh Storage storage I3 Large
./erase_projectGuid.sh Storage storage I3 Large1x
./erase_projectGuid.sh Storage storage I3 Large2x
./erase_projectGuid.sh Storage storage I3 Large4x
./erase_projectGuid.sh Storage storage I3 Large8x
./erase_projectGuid.sh Storage storage I3 Large16x
./erase_projectGuid.sh Storage storage I3 Metal

./erase_projectGuid.sh Storage storage I3en Large
./erase_projectGuid.sh Storage storage I3en Large1x
./erase_projectGuid.sh Storage storage I3en Large2x
./erase_projectGuid.sh Storage storage I3en Large3x
./erase_projectGuid.sh Storage storage I3en Large6x
./erase_projectGuid.sh Storage storage I3en Large12x
./erase_projectGuid.sh Storage storage I3en Large24x
./erase_projectGuid.sh Storage storage I3en Metal

./erase_projectGuid.sh Storage storage D2 Large1x
./erase_projectGuid.sh Storage storage D2 Large2x
./erase_projectGuid.sh Storage storage D2 Large4x
./erase_projectGuid.sh Storage storage D2 Large8x

./erase_projectGuid.sh Storage storage H1 Large2x
./erase_projectGuid.sh Storage storage H1 Large4x
./erase_projectGuid.sh Storage storage H1 Large8x
./erase_projectGuid.sh Storage storage H1 Large16x


