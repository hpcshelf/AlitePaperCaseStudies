#!/bin/sh

export Fu=Acceleration
export Fl=accelerated
export TYPE=F1
export SIZE=Large2x


if [ ! -d "EC2_Platforms/br.ufc.mdcc.backend.ec2.size.$4" ] ; then

  echo "create EC2_Platforms/br.ufc.mdcc.backend.ec2.size_$4"

  cp -r "EC2_Platforms/br.ufc.mdcc.backend.ec2.size.${SIZE}" "EC2_Platforms/br.ufc.mdcc.backend.ec2.size.$4"

  mv EC2_Platforms/br.ufc.mdcc.backend.ec2.size.$4/br.ufc.mdcc.backend.ec2.size.${SIZE}.csproj EC2_Platforms/br.ufc.mdcc.backend.ec2.size.$4/br.ufc.mdcc.backend.ec2.size.$4.csproj
  mv EC2_Platforms/br.ufc.mdcc.backend.ec2.size.$4/br.ufc.mdcc.backend.ec2.size.${SIZE}.csproj.user EC2_Platforms/br.ufc.mdcc.backend.ec2.size.$4/br.ufc.mdcc.backend.ec2.size.$4.csproj.user

  sed -i "s/$SIZE/$4/g" EC2_Platforms/br.ufc.mdcc.backend.ec2.size.$4/br.ufc.mdcc.backend.ec2.size.$4.csproj
  sed -i "s/$SIZE/$4/g" EC2_Platforms/br.ufc.mdcc.backend.ec2.size.$4/br.ufc.mdcc.backend.ec2.size.$4.csproj.user 
  sed -i "s/$SIZE/$4/g" EC2_Platforms/br.ufc.mdcc.backend.ec2.size.$4/Properties/AssemblyInfo.cs

  sed -i 's/<Project>.*<\/Project>//g;s/<ProjectGuid>.*<\/ProjectGuid>//g' EC2_Platforms/br.ufc.mdcc.backend.ec2.size.$4/br.ufc.mdcc.backend.ec2.size.$4.csproj

fi

if [ ! -d "EC2_Platforms/br.ufc.mdcc.backend.ec2.type.$2.$3" ] ; then

  echo "create EC2_Platforms/br.ufc.mdcc.backend.ec2.type.$2.$3"

  cp -r "EC2_Platforms/br.ufc.mdcc.backend.ec2.type.$Fl.${TYPE}" "EC2_Platforms/br.ufc.mdcc.backend.ec2.type.$2.$3"

  mv EC2_Platforms/br.ufc.mdcc.backend.ec2.type.$2.$3/br.ufc.mdcc.backend.ec2.type.$Fl.${TYPE}.csproj EC2_Platforms/br.ufc.mdcc.backend.ec2.type.$2.$3/br.ufc.mdcc.backend.ec2.type.$2.$3.csproj
  mv EC2_Platforms/br.ufc.mdcc.backend.ec2.type.$2.$3/br.ufc.mdcc.backend.ec2.type.$Fl.${TYPE}.csproj.user EC2_Platforms/br.ufc.mdcc.backend.ec2.type.$2.$3/br.ufc.mdcc.backend.ec2.type.$2.$3.csproj.user

  sed -i "s/$Fl/$2/g;s/$Fu/$1/g;s/$TYPE/$3/g" EC2_Platforms/br.ufc.mdcc.backend.ec2.type.$2.$3/br.ufc.mdcc.backend.ec2.type.$2.$3.csproj
  sed -i "s/$Fl/$2/g;s/$Fu/$1/g;s/$TYPE/$3/g" EC2_Platforms/br.ufc.mdcc.backend.ec2.type.$2.$3/br.ufc.mdcc.backend.ec2.type.$2.$3.csproj.user 
  sed -i "s/$Fl/$2/g;s/$Fu/$1/g;s/$TYPE/$3/g" EC2_Platforms/br.ufc.mdcc.backend.ec2.type.$2.$3/Properties/AssemblyInfo.cs

  sed -i "s/<F>/<family.$1.IEC2Family$1>/g;s/<IEC2Family>/<family.$1.IEC2Family$1>/g" br.ufc.mdcc.backend.ec2.type.$2.$3/src/1.0.0.0/IEC2Type$3.cs
  sed -i "s/<F>/<family.$1.IEC2Family$1>/g;s/<IEC2Family>/<family.$1.IEC2Family$1>/g" br.ufc.mdcc.backend.ec2.type.$2.$3/src/1.0.0.0/BaseIEC2Type$3.cs

  sed -i 's/<Project>.*<\/Project>//g;s/<ProjectGuid>.*<\/ProjectGuid>//g' EC2_Platforms/br.ufc.mdcc.backend.ec2.type.$2.$3/br.ufc.mdcc.backend.ec2.type.$2.$3.csproj

fi

if [ ! -d "EC2_Platforms/br.ufc.mdcc.backend.ec2.family.$1" ] ; then

  echo "create EC2_Platforms/br.ufc.mdcc.backend.ec2.family.$1"

  cp -r "EC2_Platforms/br.ufc.mdcc.backend.ec2.family.$Fu" "EC2_Platforms/br.ufc.mdcc.backend.ec2.family.$1"

  mv EC2_Platforms/br.ufc.mdcc.backend.ec2.family.$1/br.ufc.mdcc.backend.ec2.family.$Fu.csproj EC2_Platforms/br.ufc.mdcc.backend.ec2.family.$1/br.ufc.mdcc.backend.ec2.family.$1.csproj
  mv EC2_Platforms/br.ufc.mdcc.backend.ec2.family.$1/br.ufc.mdcc.backend.ec2.family.$Fu.csproj.user EC2_Platforms/br.ufc.mdcc.backend.ec2.family.$1/br.ufc.mdcc.backend.ec2.family.$1.csproj.user

  sed -i "s/$Fl/$2/g;s/$Fu/$1/g" EC2_Platforms/br.ufc.mdcc.backend.ec2.family.$1/br.ufc.mdcc.backend.ec2.family.$1.csproj
  sed -i "s/$Fl/$2/g;s/$Fu/$1/g" EC2_Platforms/br.ufc.mdcc.backend.ec2.family.$1/br.ufc.mdcc.backend.ec2.family.$1.csproj.user
  sed -i "s/$Fl/$2/g;s/$Fu/$1/g" EC2_Platforms/br.ufc.mdcc.backend.ec2.family.$1/Properties/AssemblyInfo.cs

  sed -i 's/<Project>.*<\/Project>//g;s/<ProjectGuid>.*<\/ProjectGuid>//g' EC2_Platforms/br.ufc.mdcc.backend.ec2.family.$1/br.ufc.mdcc.backend.ec2.family.$1.csproj

fi

if [ ! -d "EC2_Platforms/br.ufc.mdcc.backend.ec2.platform.$2.EC2_$3_$4" ] ; then

  echo "create EC2_Platforms/br.ufc.mdcc.backend.ec2.platform.$2.EC2_$3_$4"

  cp -r "EC2_Platforms/br.ufc.mdcc.backend.ec2.platform.$Fl.EC2_${TYPE}_${SIZE}" "EC2_Platforms/br.ufc.mdcc.backend.ec2.platform.$2.EC2_$3_$4"

  mv EC2_Platforms/br.ufc.mdcc.backend.ec2.platform.$2.EC2_$3_$4/br.ufc.mdcc.backend.ec2.platform.$Fl.EC2_${TYPE}_${SIZE}.csproj EC2_Platforms/br.ufc.mdcc.backend.ec2.platform.$2.EC2_$3_$4/br.ufc.mdcc.backend.ec2.platform.$2.EC2_$3_$4.csproj
  mv EC2_Platforms/br.ufc.mdcc.backend.ec2.platform.$2.EC2_$3_$4/br.ufc.mdcc.backend.ec2.platform.$Fl.EC2_${TYPE}_${SIZE}.csproj.user EC2_Platforms/br.ufc.mdcc.backend.ec2.platform.$2.EC2_$3_$4/br.ufc.mdcc.backend.ec2.platform.$2.EC2_$3_$4.csproj.user

  sed -i "s/$Fl/$2/g;s/$Fu/$1/g;s/$TYPE/$3/g;s/$SIZE/$4/g" EC2_Platforms/br.ufc.mdcc.backend.ec2.platform.$2.EC2_$3_$4/br.ufc.mdcc.backend.ec2.platform.$2.EC2_$3_$4.csproj
  sed -i "s/$Fl/$2/g;s/$Fu/$1/g;s/$TYPE/$3/g;s/$SIZE/$4/g" EC2_Platforms/br.ufc.mdcc.backend.ec2.platform.$2.EC2_$3_$4/br.ufc.mdcc.backend.ec2.platform.$2.EC2_$3_$4.csproj.user 
  sed -i "s/$Fl/$2/g;s/$Fu/$1/g;s/$TYPE/$3/g;s/$SIZE/$4/g" EC2_Platforms/br.ufc.mdcc.backend.ec2.platform.$2.EC2_$3_$4/Properties/AssemblyInfo.cs

  cp br.ufc.mdcc.backend.ec2.platform.$Fl.EC2_${TYPE}_${SIZE}/src/1.0.0.0/IEC2_${TYPE}_${SIZE}.cs br.ufc.mdcc.backend.ec2.platform.$2.EC2_$3_$4/src/1.0.0.0/IEC2_$3_$4.cs
  cp br.ufc.mdcc.backend.ec2.platform.$Fl.EC2_${TYPE}_${SIZE}/src/1.0.0.0/BaseIEC2_${TYPE}_${SIZE}.cs br.ufc.mdcc.backend.ec2.platform.$2.EC2_$3_$4/src/1.0.0.0/BaseIEC2_$3_$4.cs

  sed -i "s/$Fl/$2/g;s/$Fu/$1/g;s/$TYPE/$3/g;s/$SIZE/$4/g" br.ufc.mdcc.backend.ec2.platform.$2.EC2_$3_$4/src/1.0.0.0/IEC2_$3_$4.cs
  sed -i "s/$Fl/$2/g;s/$Fu/$1/g;s/$TYPE/$3/g;s/$SIZE/$4/g" br.ufc.mdcc.backend.ec2.platform.$2.EC2_$3_$4/src/1.0.0.0/BaseIEC2_$3_$4.cs

  sed -i 's/<Project>.*<\/Project>//g;s/<ProjectGuid>.*<\/ProjectGuid>//g' EC2_Platforms/br.ufc.mdcc.backend.ec2.platform.$2.EC2_$3_$4/br.ufc.mdcc.backend.ec2.platform.$2.EC2_$3_$4.csproj

fi

if [ ! -d "EC2_Platforms/br.ufc.mdcc.backend.ec2.impl.platform.$2.EC2_$3_$4_impl" ] ; then

  echo "create EC2_Platforms/br.ufc.mdcc.backend.ec2.impl.platform.$2.EC2_$3_$4_impl"

  cp -r "EC2_Platforms/br.ufc.mdcc.backend.ec2.impl.platform.$Fl.EC2_${TYPE}_${SIZE}_impl" "EC2_Platforms/br.ufc.mdcc.backend.ec2.impl.platform.$2.EC2_$3_$4_impl"

  mv EC2_Platforms/br.ufc.mdcc.backend.ec2.impl.platform.$2.EC2_$3_$4_impl/br.ufc.mdcc.backend.ec2.impl.platform.$Fl.EC2_${TYPE}_${SIZE}_impl.csproj EC2_Platforms/br.ufc.mdcc.backend.ec2.impl.platform.$2.EC2_$3_$4_impl/br.ufc.mdcc.backend.ec2.impl.platform.$2.EC2_$3_$4_impl.csproj
  mv EC2_Platforms/br.ufc.mdcc.backend.ec2.impl.platform.$2.EC2_$3_$4_impl/br.ufc.mdcc.backend.ec2.impl.platform.$Fl.EC2_${TYPE}_${SIZE}_impl.csproj.user EC2_Platforms/br.ufc.mdcc.backend.ec2.impl.platform.$2.EC2_$3_$4_impl/br.ufc.mdcc.backend.ec2.impl.platform.$2.EC2_$3_$4_impl.csproj.user

  sed -i "s/$Fl/$2/g;s/$Fu/$1/g;s/$TYPE/$3/g;s/$SIZE/$4/g" EC2_Platforms/br.ufc.mdcc.backend.ec2.impl.platform.$2.EC2_$3_$4_impl/br.ufc.mdcc.backend.ec2.impl.platform.$2.EC2_$3_$4_impl.csproj
  sed -i "s/$Fl/$2/g;s/$Fu/$1/g;s/$TYPE/$3/g;s/$SIZE/$4/g" EC2_Platforms/br.ufc.mdcc.backend.ec2.impl.platform.$2.EC2_$3_$4_impl/br.ufc.mdcc.backend.ec2.impl.platform.$2.EC2_$3_$4_impl.csproj.user 
  sed -i "s/$Fl/$2/g;s/$Fu/$1/g;s/$TYPE/$3/g;s/$SIZE/$4/g" EC2_Platforms/br.ufc.mdcc.backend.ec2.impl.platform.$2.EC2_$3_$4_impl/Properties/AssemblyInfo.cs

  cp br.ufc.mdcc.backend.ec2.impl.platform.$Fl.EC2_${TYPE}_${SIZE}_impl/src/1.0.0.0/IEC2_${TYPE}_${SIZE}_impl.cs br.ufc.mdcc.backend.ec2.impl.platform.$2.EC2_$3_$4_impl/src/1.0.0.0/IEC2_$3_$4_impl.cs
  cp br.ufc.mdcc.backend.ec2.impl.platform.$Fl.EC2_${TYPE}_${SIZE}_impl/src/1.0.0.0/BaseIEC2_${TYPE}_${SIZE}_impl.cs br.ufc.mdcc.backend.ec2.impl.platform.$2.EC2_$3_$4_impl/src/1.0.0.0/BaseIEC2_$3_$4_impl.cs

  sed -i "s/$Fl/$2/g;s/$Fu/$1/g;s/$TYPE/$3/g;s/$SIZE/$4/g" br.ufc.mdcc.backend.ec2.impl.platform.$2.EC2_$3_$4_impl/src/1.0.0.0/IEC2_$3_$4_impl.cs
  sed -i "s/$Fl/$2/g;s/$Fu/$1/g;s/$TYPE/$3/g;s/$SIZE/$4/g" br.ufc.mdcc.backend.ec2.impl.platform.$2.EC2_$3_$4_impl/src/1.0.0.0/BaseIEC2_$3_$4_impl.cs

  sed -i 's/<Project>.*<\/Project>//g;s/<ProjectGuid>.*<\/ProjectGuid>//g' EC2_Platforms/br.ufc.mdcc.backend.ec2.impl.platform.$2.EC2_$3_$4_impl/br.ufc.mdcc.backend.ec2.impl.platform.$2.EC2_$3_$4_impl.csproj

fi


