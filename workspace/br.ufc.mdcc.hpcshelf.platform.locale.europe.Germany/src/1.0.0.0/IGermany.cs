using br.ufc.mdcc.hpcshelf.platform.locale.europe.CentralEurope;

namespace br.ufc.mdcc.hpcshelf.platform.locale.europe.Germany
{
	public interface IGermany : BaseIGermany, ICentralEurope
	{
	}
}