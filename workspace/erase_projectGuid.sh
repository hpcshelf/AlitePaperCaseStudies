#!/bin/sh
sed -i 's/<ProjectGuid>.*<\/ProjectGuid>//g' EC2_Platforms/br.ufc.mdcc.backend.ec2.platform.$2.EC2_$3_$4/br.ufc.mdcc.backend.ec2.platform.$2.EC2_$3_$4.csproj
sed -i 's/<ProjectGuid>.*<\/ProjectGuid>//g' EC2_Platforms/br.ufc.mdcc.backend.ec2.impl.platform.$2.EC2_$3_$4_impl/br.ufc.mdcc.backend.ec2.impl.platform.$2.EC2_$3_$4_impl.csproj
sed -i 's/<ProjectGuid>.*<\/ProjectGuid>//g' EC2_Platforms/br.ufc.mdcc.backend.ec2.size.$4/br.ufc.mdcc.backend.ec2.size.$4.csproj
sed -i 's/<ProjectGuid>.*<\/ProjectGuid>//g' EC2_Platforms/br.ufc.mdcc.backend.ec2.family.$1/br.ufc.mdcc.backend.ec2.family.$1.csproj
sed -i 's/<ProjectGuid>.*<\/ProjectGuid>//g' EC2_Platforms/br.ufc.mdcc.backend.ec2.type.$2.$3/br.ufc.mdcc.backend.ec2.type.$2.$3.csproj
