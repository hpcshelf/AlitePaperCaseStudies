using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.backend.ec2.type.Type;

namespace br.ufc.mdcc.backend.ec2.type.memory.HighMemory
{
	public interface IEC2TypeHighMemory : BaseIEC2TypeHighMemory, IEC2Type<family.Memory.IEC2FamilyMemory>
	{
	}
}