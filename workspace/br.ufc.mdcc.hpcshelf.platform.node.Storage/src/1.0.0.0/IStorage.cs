using br.ufc.mdcc.hpcshelf.quantifier.IntUp;
using br.ufc.mdcc.hpcshelf.quantifier.IntDown;

namespace br.ufc.mdcc.hpcshelf.platform.node.Storage
{
	public interface IStorage<SIZ, LAT, BAND, NETBAND> : BaseIStorage<SIZ, LAT, BAND, NETBAND>
		where SIZ:IntUp
		where LAT:IntDown
		where BAND:IntUp
		where NETBAND:IntUp
	{
	}
}