/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.quantifier.IntUp;
using br.ufc.mdcc.hpcshelf.quantifier.IntDown;

namespace br.ufc.mdcc.hpcshelf.platform.node.Storage
{
	public interface BaseIStorage<SIZ, LAT, BAND, NETBAND> : IQualifierKind 
		where SIZ:IntUp
		where LAT:IntDown
		where BAND:IntUp
		where NETBAND:IntUp
	{
	}
}