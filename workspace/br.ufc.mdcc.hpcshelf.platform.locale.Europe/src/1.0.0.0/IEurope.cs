using br.ufc.mdcc.hpcshelf.platform.locale.AnyWhere;

namespace br.ufc.mdcc.hpcshelf.platform.locale.Europe
{
	public interface IEurope : BaseIEurope, IAnyWhere
	{
	}
}