/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.quantifier.IntUp;
using br.ufc.mdcc.hpcshelf.platform.node.accelerator.Manufacturer;
using br.ufc.mdcc.hpcshelf.platform.node.accelerator.Type;
using br.ufc.mdcc.hpcshelf.platform.node.accelerator.Architecture;
using br.ufc.mdcc.hpcshelf.platform.node.accelerator.Model;

namespace br.ufc.mdcc.hpcshelf.platform.node.Accelerator
{
	public interface BaseIAccelerator<ACT, MAN, TYP, ARC, MOD, MEM> : IQualifierKind 
		where ACT:IntUp
		where MAN:IAcceleratorManufacturer
		where TYP:IAcceleratorType<MAN>
		where ARC:IAcceleratorArchitecture
		where MOD:IAcceleratorModel<MAN, TYP, ARC>
		where MEM:IntUp
	{
	}
}