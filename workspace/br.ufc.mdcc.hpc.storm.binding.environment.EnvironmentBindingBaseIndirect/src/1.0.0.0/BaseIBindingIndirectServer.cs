/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpc.storm.binding.environment.EnvironmentPortType;
using br.ufc.mdcc.hpc.storm.binding.environment.EnvironmentBindingBase;

namespace br.ufc.mdcc.hpc.storm.binding.environment.EnvironmentBindingBaseIndirect
{
	public interface BaseIBindingIndirectServer<S> : BaseIServerBase<S>, ISynchronizerKind 
		where S:IEnvironmentPortType
	{
	}
}