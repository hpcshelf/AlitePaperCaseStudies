using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpc.storm.binding.environment.EnvironmentPortType;
using br.ufc.mdcc.hpc.storm.binding.environment.EnvironmentBindingBase;

namespace br.ufc.mdcc.hpc.storm.binding.environment.EnvironmentBindingBaseIndirect
{
	public interface IBindingIndirectServer<S> : BaseIBindingIndirectServer<S>, IServerBase<S>
		where S:IEnvironmentPortType
	{
	}
}