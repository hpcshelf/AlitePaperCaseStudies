using br.ufc.mdcc.common.Data;
using br.ufc.mdcc.hpcshelf.blas.data.DataDistribution;

namespace br.ufc.mdcc.hpcshelf.blas.MatrixConnector
{
	public interface IOutput<T_GET, DIST_GET> : BaseIOutput<T_GET, DIST_GET>
		where T_GET:IData
		where DIST_GET:IDataDistribution
	{
	}
}