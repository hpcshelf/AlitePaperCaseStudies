/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpc.storm.binding.environment.EnvironmentBindingBaseDirect;
using br.ufc.mdcc.hpc.storm.binding.environment.EnvironmentPortTypeSinglePartner;
using br.ufc.mdcc.hpcshelf.blas.MatrixPortType;
using br.ufc.mdcc.common.Data;
using br.ufc.mdcc.hpcshelf.blas.data.DataDistribution;

namespace br.ufc.mdcc.hpcshelf.blas.MatrixConnector
{
	public interface BaseIOutput<T_GET, DIST_GET> : ISynchronizerKind 
		where T_GET:IData
		where DIST_GET:IDataDistribution
	{
		IBindingDirect<IEnvironmentPortTypeSinglePartner, IMatrixPortType<T_GET, DIST_GET>> Get {get;}
	}
}