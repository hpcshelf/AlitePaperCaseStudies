/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.platform.node.os.linux.ubuntu.CodeName;

namespace br.ufc.mdcc.hpcshelf.platform.node.os.linux.ubuntu.codename.XenialXerus
{
	public interface BaseIXenialXerius : BaseICodeName, IQualifierKind 
	{
	}
}