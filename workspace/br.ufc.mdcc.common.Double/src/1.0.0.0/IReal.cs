using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.common.Data;
using System;

namespace br.ufc.mdcc.common.Real { 

	public interface IReal : BaseIReal, IData
	{
		IRealInstance newInstance(double d);
	} // end main interface 

	public interface IRealInstance : IDataInstance, ICloneable
	{
		double Value { set; get; }
	}

} // end namespace 
