using br.ufc.mdcc.hpc.storm.binding.environment.EnvironmentPortTypeMultiplePartner;

namespace br.ufc.mdcc.hpcshelf.mpi.ChannelPortType
{
	   	   
	public interface IChannelPortType : BaseIChannelPortType, IEnvironmentPortTypeMultiplePartner
	{
	}
}