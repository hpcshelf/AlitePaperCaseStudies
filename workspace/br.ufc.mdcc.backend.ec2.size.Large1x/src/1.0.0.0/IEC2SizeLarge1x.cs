using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.backend.ec2.size.Size;

namespace br.ufc.mdcc.backend.ec2.size.Large1x
{
	public interface IEC2SizeLarge1x : BaseIEC2SizeLarge1x, IEC2Size
	{
	}
}