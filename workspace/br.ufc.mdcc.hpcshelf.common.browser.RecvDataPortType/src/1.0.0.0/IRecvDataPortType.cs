using System;
using System.Collections.Generic;
using br.ufc.mdcc.hpcshelf.common.BrowserPortType;

namespace br.ufc.mdcc.hpcshelf.common.browser.RecvDataPortType
{
	public interface IRecvDataPortType : BaseIRecvDataPortType, IBrowserPortType
	{
	   IList<string> receive_output_message();
	}
}