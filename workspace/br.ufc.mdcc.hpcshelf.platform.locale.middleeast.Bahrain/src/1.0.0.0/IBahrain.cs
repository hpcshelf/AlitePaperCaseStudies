using br.ufc.mdcc.hpcshelf.platform.locale.MiddleEast;

namespace br.ufc.mdcc.hpcshelf.platform.locale.middleeast.Bahrain
{
	public interface IBahrain : BaseIBahrain, IMiddleEast
	{
	}
}