/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.platform.locale.MiddleEast;

namespace br.ufc.mdcc.hpcshelf.platform.locale.middleeast.Bahrain
{
	public interface BaseIBahrain : BaseIMiddleEast, IQualifierKind 
	{
	}
}