/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.backend.ec2.type.Type;

namespace br.ufc.mdcc.backend.ec2.type.general.M5n
{
	public interface BaseIEC2TypeM5n : BaseIEC2Type<family.GeneralPurpose.IEC2FamilyGeneralPurpose>, IQualifierKind 
	{
	}
}