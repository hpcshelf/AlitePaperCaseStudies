using br.ufc.mdcc.hpcshelf.platform.node.processor.Model;

namespace br.ufc.mdcc.hpcshelf.platform.node.processor.model.intel.Xeon_E7_8880v3
{
	public interface IXeon_E7_8880v3 : BaseIXeon_E7_8880v3, IProcessorModel<br.ufc.mdcc.hpcshelf.platform.node.processor.manufacturer.Intel.IIntel, br.ufc.mdcc.hpcshelf.platform.node.processor.family.intel.Xeon_E7.IXeonE7, br.ufc.mdcc.hpcshelf.platform.node.processor.series.intel.Xeon_E7_8000.IXeonE78000, br.ufc.mdcc.hpcshelf.platform.node.processor.microarchitecture.intel.Haswell.IHaswell>
	{
	}
}