/* Automatically Generated Code */

using br.ufc.mdcc.backend.ec2.platform.storage.EC2_I3_Metal;
using br.ufc.mdcc.backend.ec2.family.Storage;
using br.ufc.mdcc.backend.ec2.type.storage.I3;
using br.ufc.mdcc.backend.ec2.size.Metal;
using br.ufc.mdcc.hpcshelf.platform.maintainer.EC2;
using br.ufc.mdcc.hpcshelf.quantifier.IntUp;

namespace br.ufc.mdcc.backend.ec2.impl.platform.storage.EC2_I3_Metal_impl 
{
	public abstract class BaseIEC2_I3_Metal_impl<N>: br.ufc.pargo.hpe.kinds.Environment, BaseIEC2_I3_Metal<N>
		where N:IntUp
	{
		private IEC2TypeI3 type = null;

		protected IEC2TypeI3 Type
		{
			get
			{
				if (this.type == null)
					this.type = (IEC2TypeI3) Services.getPort("type");
				return this.type;
			}
		}
		private IEC2SizeMetal size = null;

		protected IEC2SizeMetal Size
		{
			get
			{
				if (this.size == null)
					this.size = (IEC2SizeMetal) Services.getPort("size");
				return this.size;
			}
		}
		private IEC2FamilyStorage family = null;

		protected IEC2FamilyStorage Family
		{
			get
			{
				if (this.family == null)
					this.family = (IEC2FamilyStorage) Services.getPort("family");
				return this.family;
			}
		}
		private IEC2 maintainer = null;

		protected IEC2 Maintainer
		{
			get
			{
				if (this.maintainer == null)
					this.maintainer = (IEC2) Services.getPort("maintainer");
				return this.maintainer;
			}
		}
	}
}