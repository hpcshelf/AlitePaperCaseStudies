using br.ufc.mdcc.hpcshelf.mpi.Language;
using br.ufc.mdcc.hpcshelf.common.BrowserPortType;

namespace br.ufc.mdcc.hpcshelf.common.BrowserConnector
{
	public interface IBrowserPeer<L, S1> : BaseIBrowserPeer<L, S1>
		where L:ILanguage
		where S1:IBrowserPortType
	{
	   int ConnectorKey { get; }
	}
}