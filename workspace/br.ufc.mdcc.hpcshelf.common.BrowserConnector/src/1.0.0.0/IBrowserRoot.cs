using br.ufc.mdcc.hpcshelf.common.BrowserPortType;

namespace br.ufc.mdcc.hpcshelf.common.BrowserConnector
{
	public interface IBrowserRoot<S0> : BaseIBrowserRoot<S0>
		where S0:IBrowserPortType
	{
	}
}