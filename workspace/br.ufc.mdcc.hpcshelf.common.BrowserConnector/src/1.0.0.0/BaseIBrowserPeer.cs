/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.common.BrowserBinding;
using br.ufc.mdcc.hpcshelf.common.BrowserPortType;
using br.ufc.mdcc.hpcshelf.mpi.Language;

namespace br.ufc.mdcc.hpcshelf.common.BrowserConnector
{
	public interface BaseIBrowserPeer<L, S1> : ISynchronizerKind 
		where L:ILanguage
		where S1:IBrowserPortType
	{
		IBrowserBinding<S1> Send_data_port {get;}
	}
}