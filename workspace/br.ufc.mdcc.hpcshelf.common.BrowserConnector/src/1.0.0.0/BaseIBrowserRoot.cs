/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.common.BrowserBinding;
using br.ufc.mdcc.hpcshelf.common.BrowserPortType;

namespace br.ufc.mdcc.hpcshelf.common.BrowserConnector
{
	public interface BaseIBrowserRoot<S0> : ISynchronizerKind 
		where S0:IBrowserPortType
	{
		IBrowserBinding<S0> Browse_port {get;}
	}
}