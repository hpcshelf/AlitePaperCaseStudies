using br.ufc.mdcc.hpcshelf.platform.node.processor.microarchitecture.intel.Skylake;

namespace br.ufc.mdcc.hpcshelf.platform.node.processor.microarchitecture.intel.Skylake_X
{
	public interface ISkylake_X : BaseISkylake_X, ISkylake
	{
	}
}