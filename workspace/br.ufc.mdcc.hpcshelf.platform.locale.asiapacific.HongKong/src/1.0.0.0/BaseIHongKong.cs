/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.platform.locale.AsiaPacific;

namespace br.ufc.mdcc.hpcshelf.platform.locale.asiapacific.HongKong
{
	public interface BaseIHongKong : BaseIAsiaPacific, IQualifierKind 
	{
	}
}