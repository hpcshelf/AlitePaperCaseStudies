using br.ufc.mdcc.hpcshelf.platform.locale.AsiaPacific;

namespace br.ufc.mdcc.hpcshelf.platform.locale.asiapacific.HongKong
{
	public interface IHongKong : BaseIHongKong, IAsiaPacific
	{
	}
}