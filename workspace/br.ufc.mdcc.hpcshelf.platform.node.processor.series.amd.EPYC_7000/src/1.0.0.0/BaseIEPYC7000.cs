/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.platform.node.processor.Series;

namespace br.ufc.mdcc.hpcshelf.platform.node.processor.series.amd.EPYC_7000
{
	public interface BaseIEPYC7000 : BaseIProcessorSeries<br.ufc.mdcc.hpcshelf.platform.node.processor.manufacturer.AMD.IAMD, br.ufc.mdcc.hpcshelf.platform.node.processor.family.amd.EPYC.IEPYC>, IQualifierKind 
	{
	}
}