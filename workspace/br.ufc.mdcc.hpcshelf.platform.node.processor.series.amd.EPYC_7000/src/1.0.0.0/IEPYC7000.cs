using br.ufc.mdcc.hpcshelf.platform.node.processor.Series;

namespace br.ufc.mdcc.hpcshelf.platform.node.processor.series.amd.EPYC_7000
{
	public interface IEPYC7000 : BaseIEPYC7000, IProcessorSeries<br.ufc.mdcc.hpcshelf.platform.node.processor.manufacturer.AMD.IAMD, br.ufc.mdcc.hpcshelf.platform.node.processor.family.amd.EPYC.IEPYC>
	{
	}
}