/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.platform.node.processor.Model;

namespace br.ufc.mdcc.hpcshelf.platform.node.processor.model.intel.Xeon_Platinum_8275L
{
	public interface BaseIXeon_Platinum_8275L : BaseIProcessorModel<br.ufc.mdcc.hpcshelf.platform.node.processor.manufacturer.Intel.IIntel, br.ufc.mdcc.hpcshelf.platform.node.processor.family.intel.Xeon_Platinum.IXeonPlatinum, br.ufc.mdcc.hpcshelf.platform.node.processor.series.intel.Xeon_Platinum_8000.IXeonPlatinum8000, br.ufc.mdcc.hpcshelf.platform.node.processor.microarchitecture.intel.CascadeLake.ICascadeLake>, IQualifierKind 
	{
	}
}