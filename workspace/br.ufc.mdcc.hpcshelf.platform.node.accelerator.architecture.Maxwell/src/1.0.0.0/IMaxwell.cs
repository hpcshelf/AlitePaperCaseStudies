using br.ufc.mdcc.hpcshelf.platform.node.accelerator.Architecture;

namespace br.ufc.mdcc.hpcshelf.platform.node.accelerator.architecture.Maxwell
{
	public interface IMaxwell : BaseIMaxwell, IAcceleratorArchitecture
	{
	}
}