/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.platform.node.accelerator.Architecture;

namespace br.ufc.mdcc.hpcshelf.platform.node.accelerator.architecture.Maxwell
{
	public interface BaseIMaxwell : BaseIAcceleratorArchitecture, IQualifierKind 
	{
	}
}