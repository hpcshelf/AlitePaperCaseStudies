using br.ufc.mdcc.hpcshelf.platform.node.accelerator.Architecture;

namespace br.ufc.mdcc.hpcshelf.platform.node.accelerator.architecture.Volta
{
	public interface IVolta : BaseIVolta, IAcceleratorArchitecture
	{
	}
}