#!/bin/sh

if [ ! -f "$1.ok" ] ; then
	echo "Project(\"{FAE04EC0-301F-11D3-BF4B-00C04F79EFBC}\") = \"br.ufc.mdcc.backend.ec2.family.$1\", \"br.ufc.mdcc.backend.ec2.family.$1/br.ufc.mdcc.backend.ec2.family.$1.csproj\", \"{$7}\"\nEndProject" >>ttt.txt
	echo "{$7}.Debug|Any CPU.ActiveCfg = Debug|Any CPU" >>rrr.txt
	echo "{$7}.Debug|Any CPU.Build.0 = Debug|Any CPU" >>rrr.txt
	echo "{$7}.Release|Any CPU.ActiveCfg = Release|Any CPU" >>rrr.txt                                  
	echo "{$7}.Release|Any CPU.Build.0 = Release|Any CPU" >>rrr.txt
	echo $1 >$1.ok
fi

if [ ! -f "$4.ok" ] ; then
	echo "Project(\"{FAE04EC0-301F-11D3-BF4B-00C04F79EFBC}\") = \"br.ufc.mdcc.backend.ec2.size.$4\", \"br.ufc.mdcc.backend.ec2.size.$4/br.ufc.mdcc.backend.ec2.size.$4.csproj\", \"{$8}\"\nEndProject" >>ttt.txt
        echo "{$8}.Debug|Any CPU.ActiveCfg = Debug|Any CPU" >>rrr.txt
        echo "{$8}.Debug|Any CPU.Build.0 = Debug|Any CPU" >>rrr.txt
        echo "{$8}.Release|Any CPU.ActiveCfg = Release|Any CPU" >>rrr.txt
        echo "{$8}.Release|Any CPU.Build.0 = Release|Any CPU" >>rrr.txt	
	echo $4 >$4.ok
fi

if [ ! -f "$3.ok" ] ; then
	echo "Project(\"{FAE04EC0-301F-11D3-BF4B-00C04F79EFBC}\") = \"br.ufc.mdcc.backend.ec2.type.$2.$3\", \"br.ufc.mdcc.backend.ec2.type.$2.$3/br.ufc.mdcc.backend.ec2.type.$2.$3.csproj\", \"{$9}\"\nEndProject" >>ttt.txt
        echo "{$9}.Debug|Any CPU.ActiveCfg = Debug|Any CPU" >>rrr.txt
        echo "{$9}.Debug|Any CPU.Build.0 = Debug|Any CPU" >>rrr.txt
        echo "{$9}.Release|Any CPU.ActiveCfg = Release|Any CPU" >>rrr.txt
        echo "{$9}.Release|Any CPU.Build.0 = Release|Any CPU" >>rrr.txt
	echo $3 >$3.ok
fi

echo "Project(\"{FAE04EC0-301F-11D3-BF4B-00C04F79EFBC}\") = \"br.ufc.mdcc.backend.ec2.platform.$2.EC2_$3_$4\", \"br.ufc.mdcc.backend.ec2.platform.$2.EC2_$3_$4/br.ufc.mdcc.backend.ec2.platform.$2.EC2_$3_$4.csproj\", \"{$5}\"\nEndProject" >>ttt.txt

echo "{$5}.Debug|Any CPU.ActiveCfg = Debug|Any CPU" >>rrr.txt
echo "{$5}.Debug|Any CPU.Build.0 = Debug|Any CPU" >>rrr.txt
echo "{$5}.Release|Any CPU.ActiveCfg = Release|Any CPU" >>rrr.txt 
echo "{$5}.Release|Any CPU.Build.0 = Release|Any CPU" >>rrr.txt

echo "Project(\"{FAE04EC0-301F-11D3-BF4B-00C04F79EFBC}\") = \"br.ufc.mdcc.backend.ec2.impl.platform.$2.EC2_$3_$4_impl\", \"br.ufc.mdcc.backend.ec2.impl.platform.$2.EC2_$3_$4_impl/br.ufc.mdcc.backend.ec2.impl.platform.$2.EC2_$3_$4_impl.csproj\", \"{$6}\"\nEndProject" >>ttt.txt

echo "{$6}.Debug|Any CPU.ActiveCfg = Debug|Any CPU" >>rrr.txt
echo "{$6}.Debug|Any CPU.Build.0 = Debug|Any CPU" >>rrr.txt
echo "{$6}.Release|Any CPU.ActiveCfg = Release|Any CPU" >>rrr.txt
echo "{$6}.Release|Any CPU.Build.0 = Release|Any CPU" >>rrr.txt

