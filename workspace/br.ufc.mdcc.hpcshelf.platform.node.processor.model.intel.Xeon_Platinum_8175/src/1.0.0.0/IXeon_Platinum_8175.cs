using br.ufc.mdcc.hpcshelf.platform.node.processor.Model;

namespace br.ufc.mdcc.hpcshelf.platform.node.processor.model.intel.Xeon_Platinum_8175
{
	public interface IXeon_Platinum_8175 : BaseIXeon_Platinum_8175, IProcessorModel<br.ufc.mdcc.hpcshelf.platform.node.processor.manufacturer.Intel.IIntel, br.ufc.mdcc.hpcshelf.platform.node.processor.family.intel.Xeon_Platinum.IXeonPlatinum, br.ufc.mdcc.hpcshelf.platform.node.processor.series.intel.Xeon_Platinum_8000.IXeonPlatinum8000, br.ufc.mdcc.hpcshelf.platform.node.processor.microarchitecture.intel.Skylake_SP.ISkylake_SP>
	{
	}
}