using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.backend.ec2.family.Family;

namespace br.ufc.mdcc.backend.ec2.family.Acceleration
{
	public interface IEC2FamilyAcceleration : BaseIEC2FamilyAcceleration, IEC2Family
	{
	}
}