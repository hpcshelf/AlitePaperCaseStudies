/* Automatically Generated Code */

using br.ufc.mdcc.backend.ec2.platform.memory.EC2_R4_Large;
using br.ufc.mdcc.backend.ec2.family.Memory;
using br.ufc.mdcc.backend.ec2.type.memory.R4;
using br.ufc.mdcc.backend.ec2.size.Large;
using br.ufc.mdcc.hpcshelf.platform.maintainer.EC2;
using br.ufc.mdcc.hpcshelf.quantifier.IntUp;

namespace br.ufc.mdcc.backend.ec2.impl.platform.memory.EC2_R4_Large_impl 
{
	public abstract class BaseIEC2_R4_Large_impl<N>: br.ufc.pargo.hpe.kinds.Environment, BaseIEC2_R4_Large<N>
		where N:IntUp
	{
		private IEC2TypeR4 type = null;

		protected IEC2TypeR4 Type
		{
			get
			{
				if (this.type == null)
					this.type = (IEC2TypeR4) Services.getPort("type");
				return this.type;
			}
		}
		private IEC2SizeLarge size = null;

		protected IEC2SizeLarge Size
		{
			get
			{
				if (this.size == null)
					this.size = (IEC2SizeLarge) Services.getPort("size");
				return this.size;
			}
		}
		private IEC2FamilyMemory family = null;

		protected IEC2FamilyMemory Family
		{
			get
			{
				if (this.family == null)
					this.family = (IEC2FamilyMemory) Services.getPort("family");
				return this.family;
			}
		}
		private IEC2 maintainer = null;

		protected IEC2 Maintainer
		{
			get
			{
				if (this.maintainer == null)
					this.maintainer = (IEC2) Services.getPort("maintainer");
				return this.maintainer;
			}
		}
	}
}