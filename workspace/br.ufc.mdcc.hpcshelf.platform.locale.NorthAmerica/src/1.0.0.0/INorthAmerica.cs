using br.ufc.mdcc.hpcshelf.platform.locale.AnyWhere;

namespace br.ufc.mdcc.hpcshelf.platform.locale.NorthAmerica
{
	public interface INorthAmerica : BaseINorthAmerica, IAnyWhere
	{
	}
}