using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.backend.ec2.type.Type;

namespace br.ufc.mdcc.backend.ec2.type.general.M5dn
{
	public interface IEC2TypeM5dn : BaseIEC2TypeM5dn, IEC2Type<family.GeneralPurpose.IEC2FamilyGeneralPurpose>
	{
	}
}