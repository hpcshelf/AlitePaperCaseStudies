using br.ufc.mdcc.hpcshelf.platform.locale.europe.France;

namespace br.ufc.mdcc.hpcshelf.platform.locale.europe.Paris
{
	public interface IParis : BaseIParis, IFrance
	{
	}
}