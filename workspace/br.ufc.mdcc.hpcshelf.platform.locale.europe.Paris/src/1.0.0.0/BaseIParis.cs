/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.platform.locale.europe.France;

namespace br.ufc.mdcc.hpcshelf.platform.locale.europe.Paris
{
	public interface BaseIParis : BaseIFrance, IQualifierKind 
	{
	}
}