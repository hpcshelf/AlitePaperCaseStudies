using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.backend.ec2.type.Type;

namespace br.ufc.mdcc.backend.ec2.type.memory.R5a
{
	public interface IEC2TypeR5a : BaseIEC2TypeR5a, IEC2Type<family.Memory.IEC2FamilyMemory>
	{
	}
}