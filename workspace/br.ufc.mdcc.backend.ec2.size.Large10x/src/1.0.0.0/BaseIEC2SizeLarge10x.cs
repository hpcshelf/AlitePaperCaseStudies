/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.backend.ec2.size.Size;

namespace br.ufc.mdcc.backend.ec2.size.Large10x
{
	public interface BaseIEC2SizeLarge10x : BaseIEC2Size, IQualifierKind 
	{
	}
}