/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.platform.node.accelerator.Type;

namespace br.ufc.mdcc.hpcshelf.platform.node.accelerator.type.Tesla
{
	public interface BaseITesla : BaseIAcceleratorType<br.ufc.mdcc.hpcshelf.platform.node.accelerator.manufacturer.NVIDIA.INVIDIA>, IQualifierKind 
	{
	}
}