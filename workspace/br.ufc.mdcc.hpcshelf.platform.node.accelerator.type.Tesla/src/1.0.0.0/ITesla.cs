using br.ufc.mdcc.hpcshelf.platform.node.accelerator.Type;

namespace br.ufc.mdcc.hpcshelf.platform.node.accelerator.type.Tesla
{
	public interface ITesla : BaseITesla, IAcceleratorType<br.ufc.mdcc.hpcshelf.platform.node.accelerator.manufacturer.NVIDIA.INVIDIA>
	{
	}
}