using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.backend.ec2.type.Type;

namespace br.ufc.mdcc.backend.ec2.type.memory.R5dn
{
	public interface IEC2TypeR5dn : BaseIEC2TypeR5dn, IEC2Type<family.Memory.IEC2FamilyMemory>
	{
	}
}