/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.backend.ec2.type.Type;

namespace br.ufc.mdcc.backend.ec2.type.memory.R5dn
{
	public interface BaseIEC2TypeR5dn : BaseIEC2Type<family.Memory.IEC2FamilyMemory>, IQualifierKind 
	{
	}
}