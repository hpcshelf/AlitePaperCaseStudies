using br.ufc.mdcc.hpcshelf.common.BrowserPortType;

namespace br.ufc.mdcc.hpcshelf.common.browser.SendDataPortType
{
    public delegate void TypeOfBrowserOutputMessage (int key, string data);
   
	public interface ISendDataPortType : BaseISendDataPortType, IBrowserPortType
	{
	   TypeOfBrowserOutputMessage OutputMessage { get; }
       int ConnectorKey { get; }
    }
}