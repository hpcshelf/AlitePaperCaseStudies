/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.common.BrowserPortType;

namespace br.ufc.mdcc.hpcshelf.common.browser.SendDataPortType
{
	public interface BaseISendDataPortType : BaseIBrowserPortType, IQualifierKind 
	{
	}
}