/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.platform.locale.northamerica.UnitedStates;

namespace br.ufc.mdcc.hpcshelf.platform.locale.northamerica.US_East
{
	public interface BaseIUS_East : BaseIUnitedStates, IQualifierKind 
	{
	}
}