using br.ufc.mdcc.hpcshelf.platform.locale.northamerica.UnitedStates;

namespace br.ufc.mdcc.hpcshelf.platform.locale.northamerica.US_East
{
	public interface IUS_East : BaseIUS_East, IUnitedStates
	{
	}
}