/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.backend.ec2.type.Type;

namespace br.ufc.mdcc.backend.ec2.type.storage.D2
{
	public interface BaseIEC2TypeD2 : BaseIEC2Type<family.Storage.IEC2FamilyStorage>, IQualifierKind 
	{
	}
}