using br.ufc.mdcc.hpcshelf.platform.node.processor.cache.mapping.SetAssociative;

namespace br.ufc.mdcc.hpcshelf.platform.node.processor.cache.mapping.SetAssociative_8Way
{
	public interface ISetAssociative_8Way : BaseISetAssociative_8Way, ICacheMappingSetAssociative<br.ufc.mdcc.hpcshelf.quantifier.IntUp.IntUp>
	{
	}
}