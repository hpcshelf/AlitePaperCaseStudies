/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.platform.maintainer.EC2;
using br.ufc.mdcc.backend.ec2.family.Family;
using br.ufc.mdcc.backend.ec2.size.Size;
using br.ufc.mdcc.backend.ec2.type.Type;
using br.ufc.mdcc.common.YesOrNo;
using br.ufc.mdcc.hpcshelf.quantifier.IntUp;
using br.ufc.mdcc.hpcshelf.platform.locale.AnyWhere;
using br.ufc.mdcc.hpcshelf.quantifier.DecimalDown;
using br.ufc.mdcc.hpcshelf.platform.node.processor.Manufacturer;
using br.ufc.mdcc.hpcshelf.platform.node.processor.Family;
using br.ufc.mdcc.hpcshelf.platform.node.processor.Series;
using br.ufc.mdcc.hpcshelf.platform.node.processor.Microarchitecture;
using br.ufc.mdcc.hpcshelf.platform.node.processor.Model;
using br.ufc.mdcc.hpcshelf.platform.node.processor.cache.Mapping;
using br.ufc.mdcc.hpcshelf.quantifier.IntDown;
using br.ufc.mdcc.hpcshelf.platform.node.processor.Cache;
using br.ufc.mdcc.hpcshelf.platform.node.processor.Core;
using br.ufc.mdcc.hpcshelf.platform.node.Processor;
using br.ufc.mdcc.hpcshelf.platform.node.accelerator.Manufacturer;
using br.ufc.mdcc.hpcshelf.platform.node.accelerator.Type;
using br.ufc.mdcc.hpcshelf.platform.node.accelerator.Architecture;
using br.ufc.mdcc.hpcshelf.platform.node.accelerator.Model;
using br.ufc.mdcc.hpcshelf.platform.node.Accelerator;
using br.ufc.mdcc.hpcshelf.platform.node.Memory;
using br.ufc.mdcc.backend.ec2.storage.Type;
using br.ufc.mdcc.backend.ec2.EC2InstanceStorage;
using br.ufc.mdcc.hpcshelf.platform.node.OS;
using br.ufc.mdcc.backend.ec2.EC2Node;
using br.ufc.mdcc.hpcshelf.platform.interconnection.Topology;
using br.ufc.mdcc.backend.ec2.EC2Interconnection;
using br.ufc.mdcc.hpcshelf.platform.node.Storage;
using br.ufc.mdcc.hpcshelf.platform.Performance;
using br.ufc.mdcc.hpcshelf.platform.Power;
using br.ufc.mdcc.hpcshelf.platform.Platform;

namespace br.ufc.mdcc.backend.ec2.EC2Platform
{
	public interface BaseIProcessingNode<M, F, L, T, DED, PLG, NCT, LOC, CPH, POWER, VCT, NTT, NOD_PRO_MAN, NOD_PRO_FAM, NOD_PRO_SER, NOD_PRO_MIC, NOD_PRO_MOD, NOD_PRO_NCT, NOD_PRO_CLK, NOD_PRO_TPC, NOD_PRO_MAP1i, NOD_PRO_SIZ1i, NOD_PRO_LAT1i, NOD_PRO_LINSIZ1i, NOD_PRO_CL1i, NOD_PRO_MAP1d, NOD_PRO_SIZ1d, NOD_PRO_LAT1d, NOD_PRO_LINSIZ1d, NOD_PRO_CL1d, NOD_PRO_MAP2, NOD_PRO_SIZ2, NOD_PRO_LAT2, NOD_PRO_LINSIZ2, NOD_PRO_CL2, NOD_PRO_COR, NOD_PRO_MAP3, NOD_PRO_SIZ3, NOD_PRO_LAT3, NOD_PRO_LINSIZ3, NOD_PRO_CL3, TsFMA, TdFMA, TiADD, TiSUB, TiMUL, TiDIV, TsADD, TsSUB, TsMUL, TsDIV, TdADD, TdSUB, TdMUL, TdDIV, NOD_PRO, NOD_ACC_NCT, NOD_ACC_MAN, NOD_ACC_TYP, NOD_ACC_ARC, NOD_ACC_MOD, NOD_ACC_MEM, NOD_ACC, NOD_MEM_SIZ, NOD_MEM_LAT, NOD_MEM_BAND, NOD_MEM, NOD_STO_SIZ, NOD_STO_LAT, NOD_STO_BAND, NOD_STO_NETBAND, NOD_STO_TYPE, NOD_STO, NOD_OS, NOD_DEFSTO_SIZ, NOD_DEFSTO_BAND, NOD_DEFSTO_LAT, NOD_DEFSTO_NETBAND, NOD_DEFSTO_TYPE, NOD_DEFSTO, NOD, NET_TOP, NET_BAN, NET_NLT, NET_STT, NET, STO_SIZ, STO_LAT, STO_BAND, STO_NETBAND, STO, PER_P1, PER_P2, PER_P0, PER, POW_P0, POW_P1, POW_P2, POW_P3, POW> : BaseIProcessingNode<M, br.ufc.mdcc.common.yesorno.Yes.IYes, DED, NCT, LOC, CPH, POWER, br.ufc.mdcc.hpcshelf.quantifier.IntUp.IntUp, NOD_PRO_MAN, NOD_PRO_FAM, NOD_PRO_SER, NOD_PRO_MIC, NOD_PRO_MOD, NOD_PRO_NCT, NOD_PRO_CLK, NOD_PRO_TPC, NOD_PRO_MAP1i, NOD_PRO_SIZ1i, NOD_PRO_LAT1i, NOD_PRO_LINSIZ1i, NOD_PRO_CL1i, NOD_PRO_MAP1d, NOD_PRO_SIZ1d, NOD_PRO_LAT1d, NOD_PRO_LINSIZ1d, NOD_PRO_CL1d, NOD_PRO_MAP2, NOD_PRO_SIZ2, NOD_PRO_LAT2, NOD_PRO_LINSIZ2, NOD_PRO_CL2, NOD_PRO_COR, NOD_PRO_MAP3, NOD_PRO_SIZ3, NOD_PRO_LAT3, NOD_PRO_LINSIZ3, NOD_PRO_CL3, TsFMA, TdFMA, TiADD, TiSUB, TiMUL, TiDIV, TsADD, TsSUB, TsMUL, TsDIV, TdADD, TdSUB, TdMUL, TdDIV, NOD_PRO, NOD_ACC_NCT, NOD_ACC_MAN, NOD_ACC_TYP, NOD_ACC_ARC, NOD_ACC_MOD, NOD_ACC_MEM, NOD_ACC, NOD_MEM_SIZ, NOD_MEM_LAT, NOD_MEM_BAND, NOD_MEM, NOD_STO_LAT, NOD_STO_SIZ, NOD_STO_BAND, NOD_STO_NETBAND, NOD_STO, NOD_OS, NOD, NET_STT, NET_NLT, NET_BAN, NET_TOP, NET, STO_SIZ, STO_LAT, STO_BAND, STO_NETBAND, STO, PER_P0, PER_P1, PER_P2, PER, POW_P0, POW_P1, POW_P2, POW_P3, POW>, IEnvironmentKind 
		where M:IEC2
		where F:IEC2Family
		where L:IEC2Size
		where T:IEC2Type<F>
		where DED:IYesOrNo
		where PLG:IYesOrNo
		where NCT:IntUp
		where LOC:IAnyWhere
		where CPH:DecimalDown
		where POWER:DecimalDown
		where VCT:IntUp
		where NTT:IntUp
		where NOD_PRO_MAN:IProcessorManufacturer
		where NOD_PRO_FAM:IProcessorFamily<NOD_PRO_MAN>
		where NOD_PRO_SER:IProcessorSeries<NOD_PRO_MAN, NOD_PRO_FAM>
		where NOD_PRO_MIC:IProcessorMicroarchitecture<NOD_PRO_MAN>
		where NOD_PRO_MOD:IProcessorModel<NOD_PRO_MAN, NOD_PRO_FAM, NOD_PRO_SER, NOD_PRO_MIC>
		where NOD_PRO_NCT:IntUp
		where NOD_PRO_CLK:IntUp
		where NOD_PRO_TPC:IntUp
		where NOD_PRO_MAP1i:ICacheMapping
		where NOD_PRO_SIZ1i:IntUp
		where NOD_PRO_LAT1i:IntDown
		where NOD_PRO_LINSIZ1i:IntUp
		where NOD_PRO_CL1i:ICache<NOD_PRO_MAP1i, NOD_PRO_SIZ1i, NOD_PRO_LAT1i, NOD_PRO_LINSIZ1i>
		where NOD_PRO_MAP1d:ICacheMapping
		where NOD_PRO_SIZ1d:IntUp
		where NOD_PRO_LAT1d:IntDown
		where NOD_PRO_LINSIZ1d:IntUp
		where NOD_PRO_CL1d:ICache<NOD_PRO_MAP1d, NOD_PRO_SIZ1d, NOD_PRO_LAT1d, NOD_PRO_LINSIZ1d>
		where NOD_PRO_MAP2:ICacheMapping
		where NOD_PRO_SIZ2:IntUp
		where NOD_PRO_LAT2:IntDown
		where NOD_PRO_LINSIZ2:IntUp
		where NOD_PRO_CL2:ICache<NOD_PRO_MAP2, NOD_PRO_SIZ2, NOD_PRO_LAT2, NOD_PRO_LINSIZ2>
		where NOD_PRO_COR:ICore<NOD_PRO_NCT, NOD_PRO_CLK, NOD_PRO_TPC, NOD_PRO_MAP1i, NOD_PRO_SIZ1i, NOD_PRO_LAT1i, NOD_PRO_LINSIZ1i, NOD_PRO_CL1i, NOD_PRO_MAP1d, NOD_PRO_SIZ1d, NOD_PRO_LAT1d, NOD_PRO_LINSIZ1d, NOD_PRO_CL1d, NOD_PRO_MAP2, NOD_PRO_SIZ2, NOD_PRO_LAT2, NOD_PRO_LINSIZ2, NOD_PRO_CL2>
		where NOD_PRO_MAP3:ICacheMapping
		where NOD_PRO_SIZ3:IntUp
		where NOD_PRO_LAT3:IntDown
		where NOD_PRO_LINSIZ3:IntUp
		where NOD_PRO_CL3:ICache<NOD_PRO_MAP3, NOD_PRO_SIZ3, NOD_PRO_LAT3, NOD_PRO_LINSIZ3>
		where TsFMA:DecimalDown
		where TdFMA:DecimalDown
		where TiADD:DecimalDown
		where TiSUB:DecimalDown
		where TiMUL:DecimalDown
		where TiDIV:DecimalDown
		where TsADD:DecimalDown
		where TsSUB:DecimalDown
		where TsMUL:DecimalDown
		where TsDIV:DecimalDown
		where TdADD:DecimalDown
		where TdSUB:DecimalDown
		where TdMUL:DecimalDown
		where TdDIV:DecimalDown
		where NOD_PRO:IProcessor<IntUp, NOD_PRO_MAN, NOD_PRO_FAM, NOD_PRO_SER, NOD_PRO_MOD, NOD_PRO_MIC, NOD_PRO_NCT, NOD_PRO_CLK, NOD_PRO_TPC, NOD_PRO_MAP1i, NOD_PRO_SIZ1i, NOD_PRO_LAT1i, NOD_PRO_LINSIZ1i, NOD_PRO_CL1i, NOD_PRO_MAP1d, NOD_PRO_SIZ1d, NOD_PRO_LAT1d, NOD_PRO_LINSIZ1d, NOD_PRO_CL1d, NOD_PRO_MAP2, NOD_PRO_SIZ2, NOD_PRO_LAT2, NOD_PRO_LINSIZ2, NOD_PRO_CL2, NOD_PRO_COR, NOD_PRO_MAP3, NOD_PRO_SIZ3, NOD_PRO_LAT3, NOD_PRO_LINSIZ3, NOD_PRO_CL3, TsFMA, TdFMA, TiADD, TiSUB, TiMUL, TiDIV, TsADD, TsSUB, TsMUL, TsDIV, TdADD, TdSUB, TdMUL, TdDIV>
		where NOD_ACC_NCT:IntUp
		where NOD_ACC_MAN:IAcceleratorManufacturer
		where NOD_ACC_TYP:IAcceleratorType<NOD_ACC_MAN>
		where NOD_ACC_ARC:IAcceleratorArchitecture
		where NOD_ACC_MOD:IAcceleratorModel<NOD_ACC_MAN, NOD_ACC_TYP, NOD_ACC_ARC>
		where NOD_ACC_MEM:IntUp
		where NOD_ACC:IAccelerator<NOD_ACC_NCT, NOD_ACC_MAN, NOD_ACC_TYP, NOD_ACC_ARC, NOD_ACC_MOD, NOD_ACC_MEM>
		where NOD_MEM_SIZ:IntUp
		where NOD_MEM_LAT:IntDown
		where NOD_MEM_BAND:IntUp
		where NOD_MEM:IMemory<NOD_MEM_SIZ, NOD_MEM_LAT, NOD_MEM_BAND>
		where NOD_STO_SIZ:IntUp
		where NOD_STO_LAT:IntDown
		where NOD_STO_BAND:IntUp
		where NOD_STO_NETBAND:IntUp
		where NOD_STO_TYPE:IEC2StorageType
		where NOD_STO:IEC2InstanceStorage<NOD_STO_SIZ, NOD_STO_LAT, NOD_STO_BAND, NOD_STO_NETBAND, NOD_STO_TYPE>
		where NOD_OS:IOperatingSystem
		where NOD_DEFSTO_SIZ:IntUp
		where NOD_DEFSTO_BAND:IntUp
		where NOD_DEFSTO_LAT:IntDown
		where NOD_DEFSTO_NETBAND:IntUp
		where NOD_DEFSTO_TYPE:IEC2StorageType
		where NOD_DEFSTO:IEC2InstanceStorage<NOD_DEFSTO_SIZ, NOD_DEFSTO_LAT, NOD_DEFSTO_BAND, NOD_DEFSTO_NETBAND, NOD_DEFSTO_TYPE>
		where NOD:IEC2Node<NCT, LOC, CPH, POWER, VCT, NTT, NOD_PRO_MAN, NOD_PRO_FAM, NOD_PRO_SER, NOD_PRO_MIC, NOD_PRO_MOD, NOD_PRO_NCT, NOD_PRO_CLK, NOD_PRO_TPC, NOD_PRO_MAP1i, NOD_PRO_SIZ1i, NOD_PRO_LAT1i, NOD_PRO_LINSIZ1i, NOD_PRO_LINSIZ2, NOD_PRO_CL1i, NOD_PRO_MAP1d, NOD_PRO_SIZ1d, NOD_PRO_LAT1d, NOD_PRO_LINSIZ1d, NOD_PRO_CL1d, NOD_PRO_MAP2, NOD_PRO_SIZ2, NOD_PRO_LAT2, NOD_PRO_CL2, NOD_PRO_COR, NOD_PRO_MAP3, NOD_PRO_SIZ3, NOD_PRO_LAT3, NOD_PRO_LINSIZ3, NOD_PRO_CL3, TsFMA, TdFMA, TiADD, TiSUB, TiMUL, TiDIV, TsADD, TsSUB, TsMUL, TsDIV, TdADD, TdSUB, TdMUL, TdDIV, NOD_PRO, NOD_ACC_NCT, NOD_ACC_MAN, NOD_ACC_TYP, NOD_ACC_ARC, NOD_ACC_MOD, NOD_ACC_MEM, NOD_ACC, NOD_MEM_SIZ, NOD_MEM_LAT, NOD_MEM_BAND, NOD_MEM, NOD_STO_SIZ, NOD_STO_LAT, NOD_STO_BAND, NOD_STO_NETBAND, NOD_STO_TYPE, NOD_STO, NOD_DEFSTO_SIZ, NOD_DEFSTO_LAT, NOD_DEFSTO_BAND, NOD_DEFSTO_NETBAND, NOD_DEFSTO_TYPE, NOD_DEFSTO, NOD_OS>
		where NET_TOP:ITopology
		where NET_BAN:IntUp
		where NET_NLT:IntDown
		where NET_STT:IntDown
		where NET:IEC2Interconnection<NET_STT, NET_BAN, NET_TOP, NET_NLT>
		where STO_SIZ:IntUp
		where STO_LAT:IntDown
		where STO_BAND:IntUp
		where STO_NETBAND:IntUp
		where STO:IStorage<STO_SIZ, STO_LAT, STO_BAND, STO_NETBAND>
		where PER_P1:IntUp
		where PER_P2:IntUp
		where PER_P0:IntUp
		where PER:IPerformance<PER_P0, PER_P1, PER_P2>
		where POW_P0:IntDown
		where POW_P1:IntUp
		where POW_P2:IntDown
		where POW_P3:IntUp
		where POW:IPower<POW_P0, POW_P1, POW_P2, POW_P3>
	{
	}
}