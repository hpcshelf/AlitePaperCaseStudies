/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.common.BrowserBinding;
using br.ufc.mdcc.hpcshelf.common.BrowserConnector;
using br.ufc.mdcc.hpcshelf.common.browser.SendDataPortType;

namespace br.ufc.mdcc.hpcshelf.mpi.BrowserConnectorForC
{
    public interface BaseIBrowserPeerForC : BaseIBrowserPeer<br.ufc.mdcc.hpcshelf.mpi.language.C.ILanguage_C, br.ufc.mdcc.hpcshelf.common.browser.SendDataPortType.ISendDataPortType>, ISynchronizerKind 
	{
		new IBrowserBinding<ISendDataPortType> Send_data_port {get;}
	}
}