using br.ufc.mdcc.hpcshelf.common.BrowserConnector;

namespace br.ufc.mdcc.hpcshelf.mpi.BrowserConnectorForC
{
	public interface IBrowserRootForC : BaseIBrowserRootForC, IBrowserRoot<br.ufc.mdcc.hpcshelf.common.browser.RecvDataPortType.IRecvDataPortType>
	{
	}
}