/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.common.BrowserBinding;
using br.ufc.mdcc.hpcshelf.common.BrowserConnector;
using br.ufc.mdcc.hpcshelf.common.browser.RecvDataPortType;

namespace br.ufc.mdcc.hpcshelf.mpi.BrowserConnectorForC
{
    public interface BaseIBrowserRootForC : BaseIBrowserRoot<br.ufc.mdcc.hpcshelf.common.browser.RecvDataPortType.IRecvDataPortType>, ISynchronizerKind 
	{
		new IBrowserBinding<IRecvDataPortType> Browse_port {get;}
	}
}