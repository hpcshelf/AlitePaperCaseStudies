using br.ufc.mdcc.hpcshelf.common.BrowserConnector;

namespace br.ufc.mdcc.hpcshelf.mpi.BrowserConnectorForC
{
	public interface IBrowserPeerForC : BaseIBrowserPeerForC, IBrowserPeer<br.ufc.mdcc.hpcshelf.mpi.language.C.ILanguage_C, br.ufc.mdcc.hpcshelf.common.browser.SendDataPortType.ISendDataPortType>
	{
	}
}