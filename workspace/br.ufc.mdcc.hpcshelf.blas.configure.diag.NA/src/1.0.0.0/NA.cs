using br.ufc.mdcc.hpcshelf.blas.configure.DIAG;

namespace br.ufc.mdcc.hpcshelf.blas.configure.diag.NA
{
	public interface NA : BaseNA, IDIAG
	{
	}
}