/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.blas.configure.DIAG;

namespace br.ufc.mdcc.hpcshelf.blas.configure.diag.NA
{
	public interface BaseNA : BaseIDIAG, IQualifierKind 
	{
	}
}