/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.backend.ec2.type.Type;
using br.ufc.mdcc.backend.ec2.family.Acceleration;

namespace br.ufc.mdcc.backend.ec2.type.accelerated.G3
{
	public interface BaseIEC2TypeG3 : BaseIEC2Type<IEC2FamilyAcceleration>, IQualifierKind 
	{
	}
}