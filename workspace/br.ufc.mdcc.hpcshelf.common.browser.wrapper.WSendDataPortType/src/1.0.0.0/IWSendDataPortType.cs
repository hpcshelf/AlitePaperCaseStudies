using br.ufc.mdcc.hpcshelf.common.BrowserPortType;

namespace br.ufc.mdcc.hpcshelf.common.browser.wrapper.WSendDataPortType
{
	public interface IWSendDataPortType : BaseIWSendDataPortType, IBrowserPortType
	{	   
	   void Browse(string message);
    }
}