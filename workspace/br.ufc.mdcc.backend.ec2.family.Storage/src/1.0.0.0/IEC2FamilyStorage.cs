using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.backend.ec2.family.Family;

namespace br.ufc.mdcc.backend.ec2.family.Storage
{
	public interface IEC2FamilyStorage : BaseIEC2FamilyStorage, IEC2Family
	{
	}
}