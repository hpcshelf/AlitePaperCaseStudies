/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.platform.node.processor.Manufacturer;

namespace br.ufc.mdcc.hpcshelf.platform.node.processor.Microarchitecture
{
	public interface BaseIProcessorMicroarchitecture<MAN> : IQualifierKind 
		where MAN:IProcessorManufacturer
	{
	}
}