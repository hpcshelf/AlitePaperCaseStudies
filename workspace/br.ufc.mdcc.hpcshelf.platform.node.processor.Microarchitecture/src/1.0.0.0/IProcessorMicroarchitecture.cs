using br.ufc.mdcc.hpcshelf.platform.node.processor.Manufacturer;

namespace br.ufc.mdcc.hpcshelf.platform.node.processor.Microarchitecture
{
	public interface IProcessorMicroarchitecture<MAN> : BaseIProcessorMicroarchitecture<MAN>
		where MAN:IProcessorManufacturer
	{
	}
}