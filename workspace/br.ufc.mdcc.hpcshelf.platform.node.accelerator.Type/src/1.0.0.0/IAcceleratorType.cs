using br.ufc.mdcc.hpcshelf.platform.node.accelerator.Manufacturer;

namespace br.ufc.mdcc.hpcshelf.platform.node.accelerator.Type
{
	public interface IAcceleratorType<MAN> : BaseIAcceleratorType<MAN>
		where MAN:IAcceleratorManufacturer
	{
	}
}