using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.common.Data;
using System;

namespace br.ufc.mdcc.common.DComplex { 

	public interface IDComplex : BaseIDComplex, IData
	{
		IDComplexInstance newInstance(double d);
	} // end main interface 

	public interface IDComplexInstance : IDataInstance, ICloneable
	{
		double Value { set; get; }
	}

} // end namespace 
