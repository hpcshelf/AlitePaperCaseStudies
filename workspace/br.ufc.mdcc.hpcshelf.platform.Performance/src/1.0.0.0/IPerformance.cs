using br.ufc.mdcc.hpcshelf.quantifier.IntUp;

namespace br.ufc.mdcc.hpcshelf.platform.Performance
{
	public interface IPerformance<P0, P1, P2> : BaseIPerformance<P0, P1, P2>
		where P0:IntUp
		where P1:IntUp
		where P2:IntUp
	{
	}
}