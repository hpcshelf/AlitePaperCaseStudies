/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.quantifier.IntUp;

namespace br.ufc.mdcc.hpcshelf.platform.Performance
{
	public interface BaseIPerformance<P0, P1, P2> : IQualifierKind 
		where P0:IntUp
		where P1:IntUp
		where P2:IntUp
	{
	}
}