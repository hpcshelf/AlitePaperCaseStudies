/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.blas.matrixtype.General;

namespace br.ufc.mdcc.hpcshelf.blas.matrixtype.Symmetric
{
	public interface BaseISymmetricMatrix : BaseIGeneralMatrix, IQualifierKind 
	{
	}
}