using br.ufc.mdcc.hpcshelf.blas.matrixtype.General;

namespace br.ufc.mdcc.hpcshelf.blas.matrixtype.Symmetric
{
	public interface ISymmetricMatrix : BaseISymmetricMatrix, IGeneralMatrix
	{
	}
}