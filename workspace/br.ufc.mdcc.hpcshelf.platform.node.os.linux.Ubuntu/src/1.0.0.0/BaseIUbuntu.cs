/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.platform.node.os.linux.ubuntu.CodeName;
using br.ufc.mdcc.hpcshelf.quantifier.IntUp;
using br.ufc.mdcc.hpcshelf.platform.node.os.Linux;

namespace br.ufc.mdcc.hpcshelf.platform.node.os.linux.Ubuntu
{
	public interface BaseIUbuntu<N, RELEASE_MONTH, RELEASE_YEAR> : BaseILinux, IQualifierKind 
		where N:ICodeName
		where RELEASE_MONTH:IntUp
		where RELEASE_YEAR:IntUp
	{
	}
}