/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.platform.locale.Oceania;

namespace br.ufc.mdcc.hpcshelf.platform.locale.oceania.Australia
{
	public interface BaseIAustralia : BaseIOceania, IQualifierKind 
	{
	}
}