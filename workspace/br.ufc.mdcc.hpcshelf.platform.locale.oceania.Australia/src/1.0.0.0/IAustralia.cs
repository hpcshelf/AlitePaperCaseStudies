using br.ufc.mdcc.hpcshelf.platform.locale.Oceania;

namespace br.ufc.mdcc.hpcshelf.platform.locale.oceania.Australia
{
	public interface IAustralia : BaseIAustralia, IOceania
	{
	}
}