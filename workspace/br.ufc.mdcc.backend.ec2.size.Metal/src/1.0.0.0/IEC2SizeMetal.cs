using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.backend.ec2.size.Size;

namespace br.ufc.mdcc.backend.ec2.size.Metal
{
	public interface IEC2SizeMetal : BaseIEC2SizeMetal, IEC2Size
	{
	}
}