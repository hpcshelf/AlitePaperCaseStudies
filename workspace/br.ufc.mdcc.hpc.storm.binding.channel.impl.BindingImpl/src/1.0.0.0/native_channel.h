#include "mpi.h"

int SEND = 0;
int RECEIVE = 1;
int TAG_SEND_OPERATION = 999;
int TAG_REPLY = 998;

int registerChannel(MPI_Comm root_comm);

int HPCShelfSend(
		int key,
		const void *buf,
		int count,
		MPI_Datatype datatype,
		int facet,
		int dest,
		int tag);

int HPCShelfRecv(
		int key,
		void *buf,
		int* count,
		MPI_Datatype datatype,
		int facet,
		int source,
		int tag,
		MPI_Status *status);
