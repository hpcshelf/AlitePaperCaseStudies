#include <stdio.h>
#include <stdlib.h>
#include "native_channel.h"

// MPI_Comm root_comm;

int next_conversation_tag = 100;

int takeNextConversationTag()
{
	next_conversation_tag++;
	switch (next_conversation_tag)
	{
		TAG_SEND_OPERATION:	next_conversation_tag++; break;
		INT_MAX: next_conversation_tag = 100; break;
	}
	return next_conversation_tag;
}

int key=0;
MPI_Comm channels[255];

int registerChannel(MPI_Comm root_comm_arg)
{
	printf("registerChannel -- native_channel.c -- key=%d, count=%d comm=%d\n", key, key, root_comm_arg);
    channels[key] = root_comm_arg;
	// root_comm = root_comm_arg;
	return key++;
}

void send1(MPI_Comm comm, int operation, int tag1, int tag2)
{
    int buf[2] = {operation, tag1};
	MPI_Send(buf, 2, MPI_INT, 0, tag2, comm);
}

void send3(MPI_Comm comm, int facet, int source, int tag1, int tag2)
{
	int buf[3] = {facet, source, tag1};
	MPI_Send(buf, 3, MPI_INT, 0, tag2, comm);
}

int send2(MPI_Comm comm, int facet, int target, int tag1, const void* buf, int bufsize, int tag2)
{
	int outbuf_size = 4 * sizeof(int) + bufsize;
	void* outbuf = (void*) malloc(outbuf_size);

	int position = 0;
	printf("HPCShelfSend 31 %d %d %d %d bufsize=%d outbuf_size=%d\n", facet, target, tag1, tag2, bufsize, outbuf_size);
	MPI_Pack(&facet,   1,  MPI_INT,  outbuf, outbuf_size, &position, comm);
	printf("HPCShelfSend 32 %d %d %d %d \n", facet, target, tag1, tag2);
	MPI_Pack(&target,  1,  MPI_INT,  outbuf, outbuf_size, &position, comm);
	printf("HPCShelfSend 33 %d %d %d %d \n", facet, target, tag1, tag2);
	MPI_Pack(&tag1,    1,  MPI_INT,  outbuf, outbuf_size, &position, comm);
	printf("HPCShelfSend 34 %d %d %d %d \n", facet, target, tag1, tag2);
	MPI_Pack(&bufsize, 1,  MPI_INT,  outbuf, outbuf_size, &position, comm);
	printf("HPCShelfSend 35 %d %d %d %d \n", facet, target, tag1, tag2);
	MPI_Pack((void*)buf, bufsize, MPI_BYTE, outbuf, outbuf_size, &position, comm);
	printf("HPCShelfSend 36 %d %d %d %d \n", facet, target, tag1, tag2);

	int errorCode = MPI_Send(outbuf, position, MPI_PACKED, 0, tag2, comm);
	printf("HPCShelfSend 37 %d %d %d %d \n", facet, target, tag1, tag2);

	return errorCode;
}

int buf_recv = 10000000;
void* bufffer_receive = NULL;

void receive(MPI_Comm comm, int source, int tag, void* result, int* size, MPI_Datatype datatype, MPI_Status* status)
{
	if (bufffer_receive == NULL)
		bufffer_receive = (void*) malloc(buf_recv);

	int error = MPI_Recv(bufffer_receive, buf_recv, MPI_PACKED, source, tag, comm, status);

	int position = 0;
	error = MPI_Unpack(bufffer_receive, buf_recv, &position, size, 1, MPI_INT, comm);
	error = MPI_Unpack(bufffer_receive, buf_recv, &position, result, *size, MPI_BYTE, comm);

    int typesize;
    MPI_Type_size(datatype, &typesize);

    *size = *size/typesize;

	 // free(bufffer_receive);
}

int HPCShelfSend(
		int channel_key,
		const void *buf,
		int count,
		MPI_Datatype datatype,
		int facet,
		int target,
		int tag)
{
	MPI_Comm root_comm = channels[channel_key];
	printf("HPCShelfSend 1 %d %d %d count=%d\n", facet, target, tag, count);
    send1(root_comm, SEND, tag, TAG_SEND_OPERATION);
    int typesize;
	printf("HPCShelfSend 2 %d %d %d \n", facet, target, tag);
    MPI_Type_size(datatype, &typesize);
	printf("HPCShelfSend 3 %d %d %d \n", facet, target, tag);
	send2(root_comm, facet, target, tag, buf, count*typesize, tag);
	printf("HPCShelfSend 4 %d %d %d \n", facet, target, tag);
	return MPI_SUCCESS;
}

int HPCShelfRecv(
		int channel_key,
		void *buf,
		int* count,
		MPI_Datatype datatype,
		int facet,
		int source,
		int tag,
		MPI_Status *status)
{
		MPI_Comm root_comm = channels[channel_key];
		int conversation_tag = takeNextConversationTag();
		send1(root_comm, RECEIVE, conversation_tag, TAG_SEND_OPERATION);
		send3(root_comm, facet, source, tag, conversation_tag);
		receive(root_comm, 0, tag < 0 ? tag : conversation_tag, buf, count, datatype, status);
		return MPI_SUCCESS;
}



