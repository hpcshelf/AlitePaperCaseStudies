/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.quantifier.IntUp;
using br.ufc.mdcc.hpcshelf.platform.locale.AnyWhere;
using br.ufc.mdcc.hpcshelf.quantifier.DecimalDown;
using br.ufc.mdcc.hpcshelf.platform.node.processor.Manufacturer;
using br.ufc.mdcc.hpcshelf.platform.node.processor.Family;
using br.ufc.mdcc.hpcshelf.platform.node.processor.Series;
using br.ufc.mdcc.hpcshelf.platform.node.processor.Microarchitecture;
using br.ufc.mdcc.hpcshelf.platform.node.processor.Model;
using br.ufc.mdcc.hpcshelf.platform.node.processor.cache.Mapping;
using br.ufc.mdcc.hpcshelf.quantifier.IntDown;
using br.ufc.mdcc.hpcshelf.platform.node.processor.Cache;
using br.ufc.mdcc.hpcshelf.platform.node.processor.Core;
using br.ufc.mdcc.hpcshelf.platform.node.Processor;
using br.ufc.mdcc.hpcshelf.platform.node.accelerator.Manufacturer;
using br.ufc.mdcc.hpcshelf.platform.node.accelerator.Model;
using br.ufc.mdcc.hpcshelf.platform.node.accelerator.Type;
using br.ufc.mdcc.hpcshelf.platform.node.accelerator.Architecture;
using br.ufc.mdcc.hpcshelf.platform.node.Accelerator;
using br.ufc.mdcc.hpcshelf.platform.node.Memory;
using br.ufc.mdcc.hpcshelf.platform.node.Storage;
using br.ufc.mdcc.hpcshelf.platform.node.OS;

namespace br.ufc.mdcc.hpcshelf.platform.Node
{
	public interface BaseINode<NCT, LOC, CPH, POWER, PRO_PCT, PRO_MAN, PRO_FAM, PRO_SER, PRO_MIC, PRO_MOD, PRO_NCT, PRO_CLK, PRO_TPC, PRO_MAP1i, PRO_SIZ1i, PRO_LAT1i, PRO_LINSIZ1i, PRO_CL1i, PRO_MAP1d, PRO_SIZ1d, PRO_LAT1d, PRO_LINSIZ1d, PRO_CL1d, PRO_MAP2, PRO_SIZ2, PRO_LAT2, PRO_LINSIZ2, PRO_CL2, PRO_COR, PRO_MAP3, PRO_SIZ3, PRO_LAT3, PRO_LINSIZ3, PRO_CL3, TsFMA, TdFMA, TiADD, TiSUB, TiMUL, TiDIV, TsADD, TsSUB, TsMUL, TsDIV, TdADD, TdSUB, TdMUL, TdDIV, PRO, ACC_NCT, ACC_MAN, ACC_MOD, ACC_TYP, ACC_ARC, ACC_MEM, ACC, MEM_SIZ, MEM_LAT, MEM_BAND, MEM, STO_SIZ, STO_LAT, STO_BAND, STO_NETBAND, STO, OS> : IQualifierKind 
		where NCT:IntUp
		where LOC:IAnyWhere
		where CPH:DecimalDown
		where POWER:DecimalDown
		where PRO_PCT:IntUp
		where PRO_MAN:IProcessorManufacturer
		where PRO_FAM:IProcessorFamily<PRO_MAN>
		where PRO_SER:IProcessorSeries<PRO_MAN, PRO_FAM>
		where PRO_MIC:IProcessorMicroarchitecture<PRO_MAN>
		where PRO_MOD:IProcessorModel<PRO_MAN, PRO_FAM, PRO_SER, PRO_MIC>
		where PRO_NCT:IntUp
		where PRO_CLK:IntUp
		where PRO_TPC:IntUp
		where PRO_MAP1i:ICacheMapping
		where PRO_SIZ1i:IntUp
		where PRO_LAT1i:IntDown
		where PRO_LINSIZ1i:IntUp
		where PRO_CL1i:ICache<PRO_MAP1i, PRO_SIZ1i, PRO_LAT1i, PRO_LINSIZ1i>
		where PRO_MAP1d:ICacheMapping
		where PRO_SIZ1d:IntUp
		where PRO_LAT1d:IntDown
		where PRO_LINSIZ1d:IntUp
		where PRO_CL1d:ICache<PRO_MAP1d, PRO_SIZ1d, PRO_LAT1d, PRO_LINSIZ1d>
		where PRO_MAP2:ICacheMapping
		where PRO_SIZ2:IntUp
		where PRO_LAT2:IntDown
		where PRO_LINSIZ2:IntUp
		where PRO_CL2:ICache<PRO_MAP2, PRO_SIZ2, PRO_LAT2, PRO_LINSIZ2>
		where PRO_COR:ICore<PRO_NCT, PRO_CLK, PRO_TPC, PRO_MAP1i, PRO_SIZ1i, PRO_LAT1i, PRO_LINSIZ1i, PRO_CL1i, PRO_MAP1d, PRO_SIZ1d, PRO_LAT1d, PRO_LINSIZ1d, PRO_CL1d, PRO_MAP2, PRO_SIZ2, PRO_LAT2, PRO_LINSIZ2, PRO_CL2>
		where PRO_MAP3:ICacheMapping
		where PRO_SIZ3:IntUp
		where PRO_LAT3:IntDown
		where PRO_LINSIZ3:IntUp
		where PRO_CL3:ICache<PRO_MAP3, PRO_SIZ3, PRO_LAT3, PRO_LINSIZ3>
		where TsFMA:DecimalDown
		where TdFMA:DecimalDown
		where TiADD:DecimalDown
		where TiSUB:DecimalDown
		where TiMUL:DecimalDown
		where TiDIV:DecimalDown
		where TsADD:DecimalDown
		where TsSUB:DecimalDown
		where TsMUL:DecimalDown
		where TsDIV:DecimalDown
		where TdADD:DecimalDown
		where TdSUB:DecimalDown
		where TdMUL:DecimalDown
		where TdDIV:DecimalDown
		where PRO:IProcessor<PRO_PCT, PRO_MAN, PRO_FAM, PRO_SER, PRO_MOD, PRO_MIC, PRO_NCT, PRO_CLK, PRO_TPC, PRO_MAP1i, PRO_SIZ1i, PRO_LAT1i, PRO_LINSIZ1i, PRO_CL1i, PRO_MAP1d, PRO_SIZ1d, PRO_LAT1d, PRO_LINSIZ1d, PRO_CL1d, PRO_MAP2, PRO_SIZ2, PRO_LAT2, PRO_LINSIZ2, PRO_CL2, PRO_COR, PRO_MAP3, PRO_SIZ3, PRO_LAT3, PRO_LINSIZ3, PRO_CL3, TsFMA, TdFMA, TiADD, TiSUB, TiMUL, TiDIV, TsADD, TsSUB, TsMUL, TsDIV, TdADD, TdSUB, TdMUL, TdDIV>
		where ACC_NCT:IntUp
		where ACC_MAN:IAcceleratorManufacturer
		where ACC_MOD:IAcceleratorModel<ACC_MAN, ACC_TYP, ACC_ARC>
		where ACC_TYP:IAcceleratorType<ACC_MAN>
		where ACC_ARC:IAcceleratorArchitecture
		where ACC_MEM:IntUp
		where ACC:IAccelerator<ACC_NCT, ACC_MAN, ACC_TYP, ACC_ARC, ACC_MOD, ACC_MEM>
		where MEM_SIZ:IntUp
		where MEM_LAT:IntDown
		where MEM_BAND:IntUp
		where MEM:IMemory<MEM_SIZ, MEM_LAT, MEM_BAND>
		where STO_SIZ:IntUp
		where STO_LAT:IntDown
		where STO_BAND:IntUp
		where STO_NETBAND:IntUp
		where STO:IStorage<STO_SIZ, STO_LAT, STO_BAND, STO_NETBAND>
		where OS:IOperatingSystem
	{
	}
}