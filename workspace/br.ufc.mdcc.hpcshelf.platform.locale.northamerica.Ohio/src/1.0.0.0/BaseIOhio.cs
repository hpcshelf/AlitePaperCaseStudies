/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.platform.locale.northamerica.US_East;

namespace br.ufc.mdcc.hpcshelf.platform.locale.northamerica.Ohio
{
	public interface BaseIOhio : BaseIUS_East, IQualifierKind 
	{
	}
}