using br.ufc.mdcc.hpcshelf.platform.locale.northamerica.US_East;

namespace br.ufc.mdcc.hpcshelf.platform.locale.northamerica.Ohio
{
	public interface IOhio : BaseIOhio, IUS_East
	{
	}
}