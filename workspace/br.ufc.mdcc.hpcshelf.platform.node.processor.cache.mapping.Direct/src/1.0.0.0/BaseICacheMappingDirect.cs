/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.platform.node.processor.cache.Mapping;

namespace br.ufc.mdcc.hpcshelf.platform.node.processor.cache.mapping.Direct
{
	public interface BaseICacheMappingDirect : BaseICacheMapping, IQualifierKind 
	{
	}
}