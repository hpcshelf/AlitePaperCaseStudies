using br.ufc.mdcc.hpcshelf.platform.node.processor.cache.Mapping;

namespace br.ufc.mdcc.hpcshelf.platform.node.processor.cache.mapping.Direct
{
	public interface ICacheMappingDirect : BaseICacheMappingDirect, ICacheMapping
	{
	}
}