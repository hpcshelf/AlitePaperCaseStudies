/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpc.storm.binding.environment.EnvironmentBindingBaseDirect;
using br.ufc.mdcc.hpc.storm.binding.environment.EnvironmentPortTypeSinglePartner;
using br.ufc.mdcc.hpcshelf.blas.GetPortType;
using br.ufc.mdcc.common.Data;

namespace br.ufc.mdcc.hpcshelf.blas.SetParameterConnector
{
	public interface BaseIGet<T_GET> : ISynchronizerKind 
		where T_GET:IData
	{
		IBindingDirect<IEnvironmentPortTypeSinglePartner, IGetPortType<T_GET>> Get {get;}
	}
}