using br.ufc.mdcc.common.Data;

namespace br.ufc.mdcc.hpcshelf.blas.SetParameterConnector
{
	public interface IGet<T_GET> : BaseIGet<T_GET>
		where T_GET:IData
	{
	}
}