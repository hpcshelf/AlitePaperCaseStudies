/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.platform.node.accelerator.Model;

namespace br.ufc.mdcc.hpcshelf.platform.node.accelerator.model.Tesla_T4
{
	public interface BaseITesla_T4 : BaseIAcceleratorModel<br.ufc.mdcc.hpcshelf.platform.node.accelerator.manufacturer.NVIDIA.INVIDIA, br.ufc.mdcc.hpcshelf.platform.node.accelerator.type.Tesla.ITesla, br.ufc.mdcc.hpcshelf.platform.node.accelerator.architecture.Turing.ITuring>, IQualifierKind 
	{
	}
}