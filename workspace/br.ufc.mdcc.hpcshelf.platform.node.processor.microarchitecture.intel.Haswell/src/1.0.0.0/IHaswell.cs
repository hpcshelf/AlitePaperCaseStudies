using br.ufc.mdcc.hpcshelf.platform.node.processor.Microarchitecture;

namespace br.ufc.mdcc.hpcshelf.platform.node.processor.microarchitecture.intel.Haswell
{
	public interface IHaswell : BaseIHaswell, IProcessorMicroarchitecture<br.ufc.mdcc.hpcshelf.platform.node.processor.manufacturer.Intel.IIntel>
	{
	}
}