/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.quantifier.IntUp;
using br.ufc.mdcc.hpcshelf.platform.node.processor.Manufacturer;
using br.ufc.mdcc.hpcshelf.platform.node.processor.Family;
using br.ufc.mdcc.hpcshelf.platform.node.processor.Series;
using br.ufc.mdcc.hpcshelf.platform.node.processor.Model;
using br.ufc.mdcc.hpcshelf.platform.node.processor.Microarchitecture;
using br.ufc.mdcc.hpcshelf.platform.node.processor.cache.Mapping;
using br.ufc.mdcc.hpcshelf.quantifier.IntDown;
using br.ufc.mdcc.hpcshelf.platform.node.processor.Cache;
using br.ufc.mdcc.hpcshelf.platform.node.processor.Core;
using br.ufc.mdcc.hpcshelf.quantifier.DecimalDown;

namespace br.ufc.mdcc.hpcshelf.platform.node.Processor
{
	public interface BaseIProcessor<PCT, MAN, FAM, SER, MOD, MIC, NCT, CLK, TPC, MAP1i, SIZ1i, LAT1i, LINSIZ1i, CL1i, MAP1d, SIZ1d, LAT1d, LINSIZ1d, CL1d, MAP2, SIZ2, LAT2, LINSIZ2, CL2, COR, MAP3, SIZ3, LAT3, LINSIZ3, CL3, TsFMA, TdFMA, TiADD, TiSUB, TiMUL, TiDIV, TsADD, TsSUB, TsMUL, TsDIV, TdADD, TdSUB, TdMUL, TdDIV> : IQualifierKind 
		where PCT:IntUp
		where MAN:IProcessorManufacturer
		where FAM:IProcessorFamily<MAN>
		where SER:IProcessorSeries<MAN, FAM>
		where MOD:IProcessorModel<MAN, FAM, SER, MIC>
		where MIC:IProcessorMicroarchitecture<MAN>
		where NCT:IntUp
		where CLK:IntUp
		where TPC:IntUp
		where MAP1i:ICacheMapping
		where SIZ1i:IntUp
		where LAT1i:IntDown
		where LINSIZ1i:IntUp
		where CL1i:ICache<MAP1i, SIZ1i, LAT1i, LINSIZ1i>
		where MAP1d:ICacheMapping
		where SIZ1d:IntUp
		where LAT1d:IntDown
		where LINSIZ1d:IntUp
		where CL1d:ICache<MAP1d, SIZ1d, LAT1d, LINSIZ1d>
		where MAP2:ICacheMapping
		where SIZ2:IntUp
		where LAT2:IntDown
		where LINSIZ2:IntUp
		where CL2:ICache<MAP2, SIZ2, LAT2, LINSIZ2>
		where COR:ICore<NCT, CLK, TPC, MAP1i, SIZ1i, LAT1i, LINSIZ1i, CL1i, MAP1d, SIZ1d, LAT1d, LINSIZ1d, CL1d, MAP2, SIZ2, LAT2, LINSIZ2, CL2>
		where MAP3:ICacheMapping
		where SIZ3:IntUp
		where LAT3:IntDown
		where LINSIZ3:IntUp
		where CL3:ICache<MAP3, SIZ3, LAT3, LINSIZ3>
		where TsFMA:DecimalDown
		where TdFMA:DecimalDown
		where TiADD:DecimalDown
		where TiSUB:DecimalDown
		where TiMUL:DecimalDown
		where TiDIV:DecimalDown
		where TsADD:DecimalDown
		where TsSUB:DecimalDown
		where TsMUL:DecimalDown
		where TsDIV:DecimalDown
		where TdADD:DecimalDown
		where TdSUB:DecimalDown
		where TdMUL:DecimalDown
		where TdDIV:DecimalDown
	{
	}
}