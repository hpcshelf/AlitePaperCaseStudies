	using System;
	using System.IO;
	using System.Text;
    using System.Collections.Generic;
	using br.ufc.pargo.hpe.backend.DGAC;
	using br.ufc.pargo.hpe.basic;
	using br.ufc.pargo.hpe.kinds;
	using br.ufc.mdcc.hpcshelf.mpi.Language;
	using br.ufc.mdcc.hpcshelf.platform.Maintainer;
	using br.ufc.mdcc.common.YesOrNo;
	using br.ufc.mdcc.hpcshelf.quantifier.IntUp;
	using br.ufc.mdcc.hpcshelf.platform.locale.AnyWhere;
	using br.ufc.mdcc.hpcshelf.quantifier.DecimalDown;
	using br.ufc.mdcc.hpcshelf.platform.node.processor.Manufacturer;
	using br.ufc.mdcc.hpcshelf.platform.node.processor.Family;
	using br.ufc.mdcc.hpcshelf.platform.node.processor.Series;
	using br.ufc.mdcc.hpcshelf.platform.node.processor.Microarchitecture;
	using br.ufc.mdcc.hpcshelf.platform.node.processor.Model;
	using br.ufc.mdcc.hpcshelf.platform.node.processor.cache.Mapping;
	using br.ufc.mdcc.hpcshelf.quantifier.IntDown;
	using br.ufc.mdcc.hpcshelf.platform.node.processor.Cache;
	using br.ufc.mdcc.hpcshelf.platform.node.processor.Core;
	using br.ufc.mdcc.hpcshelf.platform.node.Processor;
	using br.ufc.mdcc.hpcshelf.platform.node.accelerator.Manufacturer;
	using br.ufc.mdcc.hpcshelf.platform.node.accelerator.Type;
	using br.ufc.mdcc.hpcshelf.platform.node.accelerator.Architecture;
	using br.ufc.mdcc.hpcshelf.platform.node.accelerator.Model;
	using br.ufc.mdcc.hpcshelf.platform.node.Accelerator;
	using br.ufc.mdcc.hpcshelf.platform.node.Memory;
	using br.ufc.mdcc.hpcshelf.platform.node.Storage;
	using br.ufc.mdcc.hpcshelf.platform.node.OS;
	using br.ufc.mdcc.hpcshelf.platform.Node;
	using br.ufc.mdcc.hpcshelf.platform.interconnection.Topology;
	using br.ufc.mdcc.hpcshelf.platform.Interconnection;
	using br.ufc.mdcc.hpcshelf.platform.Performance;
	using br.ufc.mdcc.hpcshelf.platform.Power;
	using br.ufc.mdcc.hpcshelf.platform.Platform;
	using br.ufc.mdcc.hpcshelf.mpi.MPILauncher;
	using br.ufc.mdcc.hpcshelf.mpi.intercommunicator.BasicSendReceive;
	using br.ufc.mdcc.hpc.storm.binding.environment.EnvironmentBindingBaseDirect;
	using br.ufc.mdcc.hpcshelf.mpi.binding.IntercommunicatorBinding;
	using System.Runtime.InteropServices;
	using br.ufc.mdcc.hpcshelf.common.browser.wrapper.WSendDataPortType;
	using br.ufc.mdcc.hpcshelf.common.BrowserBinding;
	using MPI;
	using br.ufc.pargo.hpe.backend.DGAC.utils;
	using ENV = System.Environment;
	using br.ufc.mdcc.hpcshelf.common.browser.wrapper.WSendDataPortType;
	
	namespace br.ufc.mdcc.hpcshelf.mpi.impl.MPILauncherForPythonImpl
	{	   
	   
	    using MPI_Comm = Int32;
	   
		public class IProcessLauncherForPythonImpl<L, BS, PLATFORM_M, PLATFORM_VIR, PLATFORM_DED, PLATFORM_N, PLATFORM_LOC, PLATFORM_CPH, PLATFORM_POWER, PLATFORM_NOD_PRO_PCT, PLATFORM_NOD_PRO_MAN, PLATFORM_NOD_PRO_FAM, PLATFORM_NOD_PRO_SER, PLATFORM_NOD_PRO_MIC, PLATFORM_NOD_PRO_MOD, PLATFORM_NOD_PRO_NCT, PLATFORM_NOD_PRO_CLK, PLATFORM_NOD_PRO_TPC, PLATFORM_NOD_PRO_MAP1i, PLATFORM_NOD_PRO_SIZ1i, PLATFORM_NOD_PRO_LAT1i, PLATFORM_NOD_PRO_LINSIZ1i, PLATFORM_NOD_PRO_CL1i, PLATFORM_NOD_PRO_MAP1d, PLATFORM_NOD_PRO_SIZ1d, PLATFORM_NOD_PRO_LAT1d, PLATFORM_NOD_PRO_LINSIZ1d, PLATFORM_NOD_PRO_CL1d, PLATFORM_NOD_PRO_MAP2, PLATFORM_NOD_PRO_SIZ2, PLATFORM_NOD_PRO_LAT2, PLATFORM_NOD_PRO_LINSIZ2, PLATFORM_NOD_PRO_CL2, PLATFORM_NOD_PRO_COR, PLATFORM_NOD_PRO_MAP3, PLATFORM_NOD_PRO_SIZ3, PLATFORM_NOD_PRO_LAT3, PLATFORM_NOD_PRO_LINSIZ3, PLATFORM_NOD_PRO_CL3, PLATFORM_TsFMA, PLATFORM_TdFMA, PLATFORM_TiADD, PLATFORM_TiSUB, PLATFORM_TiMUL, PLATFORM_TiDIV, PLATFORM_TsADD, PLATFORM_TsSUB, PLATFORM_TsMUL, PLATFORM_TsDIV, PLATFORM_TdADD, PLATFORM_TdSUB, PLATFORM_TdMUL, PLATFORM_TdDIV, PLATFORM_NOD_PRO, PLATFORM_NOD_ACC_NCT, PLATFORM_NOD_ACC_MAN, PLATFORM_NOD_ACC_TYP, PLATFORM_NOD_ACC_ARC, PLATFORM_NOD_ACC_MOD, PLATFORM_NOD_ACC_MEM, PLATFORM_NOD_ACC, PLATFORM_NOD_MEM_SIZ, PLATFORM_NOD_MEM_LAT, PLATFORM_NOD_MEM_BAND, PLATFORM_NOD_MEM, PLATFORM_NOD_STO_LAT, PLATFORM_NOD_STO_SIZ, PLATFORM_NOD_STO_BAND, PLATFORM_NOD_STO_NETBAND, PLATFORM_NOD_STO, PLATFORM_NOD_OS, PLATFORM_NOD, PLATFORM_NET_STT, PLATFORM_NET_NLT, PLATFORM_NET_BAN, PLATFORM_NET_TOP, PLATFORM_NET, PLATFORM_STO_SIZ, PLATFORM_STO_LAT, PLATFORM_STO_BAND, PLATFORM_STO_NETBAND, PLATFORM_STO, PLATFORM_PER_P0, PLATFORM_PER_P1, PLATFORM_PER_P2, PLATFORM_PER, PLATFORM_POW_P0, PLATFORM_POW_P1, PLATFORM_POW_P2, PLATFORM_POW_P3, PLATFORM_POW, PLATFORM> : BaseIProcessLauncherForPythonImpl<L, BS, PLATFORM_M, PLATFORM_VIR, PLATFORM_DED, PLATFORM_N, PLATFORM_LOC, PLATFORM_CPH, PLATFORM_POWER, PLATFORM_NOD_PRO_PCT, PLATFORM_NOD_PRO_MAN, PLATFORM_NOD_PRO_FAM, PLATFORM_NOD_PRO_SER, PLATFORM_NOD_PRO_MIC, PLATFORM_NOD_PRO_MOD, PLATFORM_NOD_PRO_NCT, PLATFORM_NOD_PRO_CLK, PLATFORM_NOD_PRO_TPC, PLATFORM_NOD_PRO_MAP1i, PLATFORM_NOD_PRO_SIZ1i, PLATFORM_NOD_PRO_LAT1i, PLATFORM_NOD_PRO_LINSIZ1i, PLATFORM_NOD_PRO_CL1i, PLATFORM_NOD_PRO_MAP1d, PLATFORM_NOD_PRO_SIZ1d, PLATFORM_NOD_PRO_LAT1d, PLATFORM_NOD_PRO_LINSIZ1d, PLATFORM_NOD_PRO_CL1d, PLATFORM_NOD_PRO_MAP2, PLATFORM_NOD_PRO_SIZ2, PLATFORM_NOD_PRO_LAT2, PLATFORM_NOD_PRO_LINSIZ2, PLATFORM_NOD_PRO_CL2, PLATFORM_NOD_PRO_COR, PLATFORM_NOD_PRO_MAP3, PLATFORM_NOD_PRO_SIZ3, PLATFORM_NOD_PRO_LAT3, PLATFORM_NOD_PRO_LINSIZ3, PLATFORM_NOD_PRO_CL3, PLATFORM_TsFMA, PLATFORM_TdFMA, PLATFORM_TiADD, PLATFORM_TiSUB, PLATFORM_TiMUL, PLATFORM_TiDIV, PLATFORM_TsADD, PLATFORM_TsSUB, PLATFORM_TsMUL, PLATFORM_TsDIV, PLATFORM_TdADD, PLATFORM_TdSUB, PLATFORM_TdMUL, PLATFORM_TdDIV, PLATFORM_NOD_PRO, PLATFORM_NOD_ACC_NCT, PLATFORM_NOD_ACC_MAN, PLATFORM_NOD_ACC_TYP, PLATFORM_NOD_ACC_ARC, PLATFORM_NOD_ACC_MOD, PLATFORM_NOD_ACC_MEM, PLATFORM_NOD_ACC, PLATFORM_NOD_MEM_SIZ, PLATFORM_NOD_MEM_LAT, PLATFORM_NOD_MEM_BAND, PLATFORM_NOD_MEM, PLATFORM_NOD_STO_LAT, PLATFORM_NOD_STO_SIZ, PLATFORM_NOD_STO_BAND, PLATFORM_NOD_STO_NETBAND, PLATFORM_NOD_STO, PLATFORM_NOD_OS, PLATFORM_NOD, PLATFORM_NET_STT, PLATFORM_NET_NLT, PLATFORM_NET_BAN, PLATFORM_NET_TOP, PLATFORM_NET, PLATFORM_STO_SIZ, PLATFORM_STO_LAT, PLATFORM_STO_BAND, PLATFORM_STO_NETBAND, PLATFORM_STO, PLATFORM_PER_P0, PLATFORM_PER_P1, PLATFORM_PER_P2, PLATFORM_PER, PLATFORM_POW_P0, PLATFORM_POW_P1, PLATFORM_POW_P2, PLATFORM_POW_P3, PLATFORM_POW, PLATFORM>, IProcessLauncher<L, BS, PLATFORM_M, PLATFORM_VIR, PLATFORM_DED, PLATFORM_N, PLATFORM_LOC, PLATFORM_CPH, PLATFORM_POWER, PLATFORM_NOD_PRO_PCT, PLATFORM_NOD_PRO_MAN, PLATFORM_NOD_PRO_FAM, PLATFORM_NOD_PRO_SER, PLATFORM_NOD_PRO_MIC, PLATFORM_NOD_PRO_MOD, PLATFORM_NOD_PRO_NCT, PLATFORM_NOD_PRO_CLK, PLATFORM_NOD_PRO_TPC, PLATFORM_NOD_PRO_MAP1i, PLATFORM_NOD_PRO_SIZ1i, PLATFORM_NOD_PRO_LAT1i, PLATFORM_NOD_PRO_LINSIZ1i, PLATFORM_NOD_PRO_CL1i, PLATFORM_NOD_PRO_MAP1d, PLATFORM_NOD_PRO_SIZ1d, PLATFORM_NOD_PRO_LAT1d, PLATFORM_NOD_PRO_LINSIZ1d, PLATFORM_NOD_PRO_CL1d, PLATFORM_NOD_PRO_MAP2, PLATFORM_NOD_PRO_SIZ2, PLATFORM_NOD_PRO_LAT2, PLATFORM_NOD_PRO_LINSIZ2, PLATFORM_NOD_PRO_CL2, PLATFORM_NOD_PRO_COR, PLATFORM_NOD_PRO_MAP3, PLATFORM_NOD_PRO_SIZ3, PLATFORM_NOD_PRO_LAT3, PLATFORM_NOD_PRO_LINSIZ3, PLATFORM_NOD_PRO_CL3, PLATFORM_TsFMA, PLATFORM_TdFMA, PLATFORM_TiADD, PLATFORM_TiSUB, PLATFORM_TiMUL, PLATFORM_TiDIV, PLATFORM_TsADD, PLATFORM_TsSUB, PLATFORM_TsMUL, PLATFORM_TsDIV, PLATFORM_TdADD, PLATFORM_TdSUB, PLATFORM_TdMUL, PLATFORM_TdDIV, PLATFORM_NOD_PRO, PLATFORM_NOD_ACC_NCT, PLATFORM_NOD_ACC_MAN, PLATFORM_NOD_ACC_TYP, PLATFORM_NOD_ACC_ARC, PLATFORM_NOD_ACC_MOD, PLATFORM_NOD_ACC_MEM, PLATFORM_NOD_ACC, PLATFORM_NOD_MEM_SIZ, PLATFORM_NOD_MEM_LAT, PLATFORM_NOD_MEM_BAND, PLATFORM_NOD_MEM, PLATFORM_NOD_STO_LAT, PLATFORM_NOD_STO_SIZ, PLATFORM_NOD_STO_BAND, PLATFORM_NOD_STO_NETBAND, PLATFORM_NOD_STO, PLATFORM_NOD_OS, PLATFORM_NOD, PLATFORM_NET_STT, PLATFORM_NET_NLT, PLATFORM_NET_BAN, PLATFORM_NET_TOP, PLATFORM_NET, PLATFORM_STO_SIZ, PLATFORM_STO_LAT, PLATFORM_STO_BAND, PLATFORM_STO_NETBAND, PLATFORM_STO, PLATFORM_PER_P0, PLATFORM_PER_P1, PLATFORM_PER_P2, PLATFORM_PER, PLATFORM_POW_P0, PLATFORM_POW_P1, PLATFORM_POW_P2, PLATFORM_POW_P3, PLATFORM_POW, PLATFORM>
			where L:ILanguage
			where BS:IWSendDataPortType
			where PLATFORM_M:IMaintainer
			where PLATFORM_VIR:IYesOrNo
			where PLATFORM_DED:IYesOrNo
			where PLATFORM_N:IntUp
			where PLATFORM_LOC:IAnyWhere
			where PLATFORM_CPH:DecimalDown
			where PLATFORM_POWER:DecimalDown
			where PLATFORM_NOD_PRO_PCT:IntUp
			where PLATFORM_NOD_PRO_MAN:IProcessorManufacturer
			where PLATFORM_NOD_PRO_FAM:IProcessorFamily<PLATFORM_NOD_PRO_MAN>
			where PLATFORM_NOD_PRO_SER:IProcessorSeries<PLATFORM_NOD_PRO_MAN, PLATFORM_NOD_PRO_FAM>
			where PLATFORM_NOD_PRO_MIC:IProcessorMicroarchitecture<PLATFORM_NOD_PRO_MAN>
			where PLATFORM_NOD_PRO_MOD:IProcessorModel<PLATFORM_NOD_PRO_MAN, PLATFORM_NOD_PRO_FAM, PLATFORM_NOD_PRO_SER, PLATFORM_NOD_PRO_MIC>
			where PLATFORM_NOD_PRO_NCT:IntUp
			where PLATFORM_NOD_PRO_CLK:IntUp
			where PLATFORM_NOD_PRO_TPC:IntUp
			where PLATFORM_NOD_PRO_MAP1i:ICacheMapping
			where PLATFORM_NOD_PRO_SIZ1i:IntUp
			where PLATFORM_NOD_PRO_LAT1i:IntDown
			where PLATFORM_NOD_PRO_LINSIZ1i:IntUp
			where PLATFORM_NOD_PRO_CL1i:ICache<PLATFORM_NOD_PRO_MAP1i, PLATFORM_NOD_PRO_SIZ1i, PLATFORM_NOD_PRO_LAT1i, PLATFORM_NOD_PRO_LINSIZ1i>
			where PLATFORM_NOD_PRO_MAP1d:ICacheMapping
			where PLATFORM_NOD_PRO_SIZ1d:IntUp
			where PLATFORM_NOD_PRO_LAT1d:IntDown
			where PLATFORM_NOD_PRO_LINSIZ1d:IntUp
			where PLATFORM_NOD_PRO_CL1d:ICache<PLATFORM_NOD_PRO_MAP1d, PLATFORM_NOD_PRO_SIZ1d, PLATFORM_NOD_PRO_LAT1d, PLATFORM_NOD_PRO_LINSIZ1d>
			where PLATFORM_NOD_PRO_MAP2:ICacheMapping
			where PLATFORM_NOD_PRO_SIZ2:IntUp
			where PLATFORM_NOD_PRO_LAT2:IntDown
			where PLATFORM_NOD_PRO_LINSIZ2:IntUp
			where PLATFORM_NOD_PRO_CL2:ICache<PLATFORM_NOD_PRO_MAP2, PLATFORM_NOD_PRO_SIZ2, PLATFORM_NOD_PRO_LAT2, PLATFORM_NOD_PRO_LINSIZ2>
			where PLATFORM_NOD_PRO_COR:ICore<PLATFORM_NOD_PRO_NCT, PLATFORM_NOD_PRO_CLK, PLATFORM_NOD_PRO_TPC, PLATFORM_NOD_PRO_MAP1i, PLATFORM_NOD_PRO_SIZ1i, PLATFORM_NOD_PRO_LAT1i, PLATFORM_NOD_PRO_LINSIZ1i, PLATFORM_NOD_PRO_CL1i, PLATFORM_NOD_PRO_MAP1d, PLATFORM_NOD_PRO_SIZ1d, PLATFORM_NOD_PRO_LAT1d, PLATFORM_NOD_PRO_LINSIZ1d, PLATFORM_NOD_PRO_CL1d, PLATFORM_NOD_PRO_MAP2, PLATFORM_NOD_PRO_SIZ2, PLATFORM_NOD_PRO_LAT2, PLATFORM_NOD_PRO_LINSIZ2, PLATFORM_NOD_PRO_CL2>
			where PLATFORM_NOD_PRO_MAP3:ICacheMapping
			where PLATFORM_NOD_PRO_SIZ3:IntUp
			where PLATFORM_NOD_PRO_LAT3:IntDown
			where PLATFORM_NOD_PRO_LINSIZ3:IntUp
			where PLATFORM_NOD_PRO_CL3:ICache<PLATFORM_NOD_PRO_MAP3, PLATFORM_NOD_PRO_SIZ3, PLATFORM_NOD_PRO_LAT3, PLATFORM_NOD_PRO_LINSIZ3>
			where PLATFORM_TsFMA:DecimalDown
			where PLATFORM_TdFMA:DecimalDown
			where PLATFORM_TiADD:DecimalDown
			where PLATFORM_TiSUB:DecimalDown
			where PLATFORM_TiMUL:DecimalDown
			where PLATFORM_TiDIV:DecimalDown
			where PLATFORM_TsADD:DecimalDown
			where PLATFORM_TsSUB:DecimalDown
			where PLATFORM_TsMUL:DecimalDown
			where PLATFORM_TsDIV:DecimalDown
			where PLATFORM_TdADD:DecimalDown
			where PLATFORM_TdSUB:DecimalDown
			where PLATFORM_TdMUL:DecimalDown
			where PLATFORM_TdDIV:DecimalDown
			where PLATFORM_NOD_PRO:IProcessor<PLATFORM_NOD_PRO_PCT, PLATFORM_NOD_PRO_MAN, PLATFORM_NOD_PRO_FAM, PLATFORM_NOD_PRO_SER, PLATFORM_NOD_PRO_MOD, PLATFORM_NOD_PRO_MIC, PLATFORM_NOD_PRO_NCT, PLATFORM_NOD_PRO_CLK, PLATFORM_NOD_PRO_TPC, PLATFORM_NOD_PRO_MAP1i, PLATFORM_NOD_PRO_SIZ1i, PLATFORM_NOD_PRO_LAT1i, PLATFORM_NOD_PRO_LINSIZ1i, PLATFORM_NOD_PRO_CL1i, PLATFORM_NOD_PRO_MAP1d, PLATFORM_NOD_PRO_SIZ1d, PLATFORM_NOD_PRO_LAT1d, PLATFORM_NOD_PRO_LINSIZ1d, PLATFORM_NOD_PRO_CL1d, PLATFORM_NOD_PRO_MAP2, PLATFORM_NOD_PRO_SIZ2, PLATFORM_NOD_PRO_LAT2, PLATFORM_NOD_PRO_LINSIZ2, PLATFORM_NOD_PRO_CL2, PLATFORM_NOD_PRO_COR, PLATFORM_NOD_PRO_MAP3, PLATFORM_NOD_PRO_SIZ3, PLATFORM_NOD_PRO_LAT3, PLATFORM_NOD_PRO_LINSIZ3, PLATFORM_NOD_PRO_CL3, PLATFORM_TsFMA, PLATFORM_TdFMA, PLATFORM_TiADD, PLATFORM_TiSUB, PLATFORM_TiMUL, PLATFORM_TiDIV, PLATFORM_TsADD, PLATFORM_TsSUB, PLATFORM_TsMUL, PLATFORM_TsDIV, PLATFORM_TdADD, PLATFORM_TdSUB, PLATFORM_TdMUL, PLATFORM_TdDIV>
			where PLATFORM_NOD_ACC_NCT:IntUp
			where PLATFORM_NOD_ACC_MAN:IAcceleratorManufacturer
			where PLATFORM_NOD_ACC_TYP:IAcceleratorType<PLATFORM_NOD_ACC_MAN>
			where PLATFORM_NOD_ACC_ARC:IAcceleratorArchitecture
			where PLATFORM_NOD_ACC_MOD:IAcceleratorModel<PLATFORM_NOD_ACC_MAN, PLATFORM_NOD_ACC_TYP, PLATFORM_NOD_ACC_ARC>
			where PLATFORM_NOD_ACC_MEM:IntUp
			where PLATFORM_NOD_ACC:IAccelerator<PLATFORM_NOD_ACC_NCT, PLATFORM_NOD_ACC_MAN, PLATFORM_NOD_ACC_TYP, PLATFORM_NOD_ACC_ARC, PLATFORM_NOD_ACC_MOD, PLATFORM_NOD_ACC_MEM>
			where PLATFORM_NOD_MEM_SIZ:IntUp
			where PLATFORM_NOD_MEM_LAT:IntDown
			where PLATFORM_NOD_MEM_BAND:IntUp
			where PLATFORM_NOD_MEM:IMemory<PLATFORM_NOD_MEM_SIZ, PLATFORM_NOD_MEM_LAT, PLATFORM_NOD_MEM_BAND>
			where PLATFORM_NOD_STO_LAT:IntDown
			where PLATFORM_NOD_STO_SIZ:IntUp
			where PLATFORM_NOD_STO_BAND:IntUp
			where PLATFORM_NOD_STO_NETBAND:IntUp
			where PLATFORM_NOD_STO:IStorage<PLATFORM_NOD_STO_SIZ, PLATFORM_NOD_STO_LAT, PLATFORM_NOD_STO_BAND, PLATFORM_NOD_STO_NETBAND>
			where PLATFORM_NOD_OS:IOperatingSystem
			where PLATFORM_NOD:INode<PLATFORM_N, PLATFORM_LOC, PLATFORM_CPH, PLATFORM_POWER, PLATFORM_NOD_PRO_PCT, PLATFORM_NOD_PRO_MAN, PLATFORM_NOD_PRO_FAM, PLATFORM_NOD_PRO_SER, PLATFORM_NOD_PRO_MIC, PLATFORM_NOD_PRO_MOD, PLATFORM_NOD_PRO_NCT, PLATFORM_NOD_PRO_CLK, PLATFORM_NOD_PRO_TPC, PLATFORM_NOD_PRO_MAP1i, PLATFORM_NOD_PRO_SIZ1i, PLATFORM_NOD_PRO_LAT1i, PLATFORM_NOD_PRO_LINSIZ1i, PLATFORM_NOD_PRO_CL1i, PLATFORM_NOD_PRO_MAP1d, PLATFORM_NOD_PRO_SIZ1d, PLATFORM_NOD_PRO_LAT1d, PLATFORM_NOD_PRO_LINSIZ1d, PLATFORM_NOD_PRO_CL1d, PLATFORM_NOD_PRO_MAP2, PLATFORM_NOD_PRO_SIZ2, PLATFORM_NOD_PRO_LAT2, PLATFORM_NOD_PRO_LINSIZ2, PLATFORM_NOD_PRO_CL2, PLATFORM_NOD_PRO_COR, PLATFORM_NOD_PRO_MAP3, PLATFORM_NOD_PRO_SIZ3, PLATFORM_NOD_PRO_LAT3, PLATFORM_NOD_PRO_LINSIZ3, PLATFORM_NOD_PRO_CL3, PLATFORM_TsFMA, PLATFORM_TdFMA, PLATFORM_TiADD, PLATFORM_TiSUB, PLATFORM_TiMUL, PLATFORM_TiDIV, PLATFORM_TsADD, PLATFORM_TsSUB, PLATFORM_TsMUL, PLATFORM_TsDIV, PLATFORM_TdADD, PLATFORM_TdSUB, PLATFORM_TdMUL, PLATFORM_TdDIV, PLATFORM_NOD_PRO, PLATFORM_NOD_ACC_NCT, PLATFORM_NOD_ACC_MAN, PLATFORM_NOD_ACC_MOD, PLATFORM_NOD_ACC_TYP, PLATFORM_NOD_ACC_ARC, PLATFORM_NOD_ACC_MEM, PLATFORM_NOD_ACC, PLATFORM_NOD_MEM_SIZ, PLATFORM_NOD_MEM_LAT, PLATFORM_NOD_MEM_BAND, PLATFORM_NOD_MEM, PLATFORM_NOD_STO_SIZ, PLATFORM_NOD_STO_LAT, PLATFORM_NOD_STO_BAND, PLATFORM_NOD_STO_NETBAND, PLATFORM_NOD_STO, PLATFORM_NOD_OS>
			where PLATFORM_NET_STT:IntDown
			where PLATFORM_NET_NLT:IntDown
			where PLATFORM_NET_BAN:IntUp
			where PLATFORM_NET_TOP:ITopology
			where PLATFORM_NET:IInterconnection<PLATFORM_NET_STT, PLATFORM_NET_NLT, PLATFORM_NET_BAN, PLATFORM_NET_TOP>
			where PLATFORM_STO_SIZ:IntUp
			where PLATFORM_STO_LAT:IntDown
			where PLATFORM_STO_BAND:IntUp
			where PLATFORM_STO_NETBAND:IntUp
			where PLATFORM_STO:IStorage<PLATFORM_STO_SIZ, PLATFORM_STO_LAT, PLATFORM_STO_BAND, PLATFORM_STO_NETBAND>
			where PLATFORM_PER_P0:IntUp
			where PLATFORM_PER_P1:IntUp
			where PLATFORM_PER_P2:IntUp
			where PLATFORM_PER:IPerformance<PLATFORM_PER_P0, PLATFORM_PER_P1, PLATFORM_PER_P2>
			where PLATFORM_POW_P0:IntDown
			where PLATFORM_POW_P1:IntUp
			where PLATFORM_POW_P2:IntDown
			where PLATFORM_POW_P3:IntUp
			where PLATFORM_POW:IPower<PLATFORM_POW_P0, PLATFORM_POW_P1, PLATFORM_POW_P2, PLATFORM_POW_P3>
			where PLATFORM:IProcessingNode<PLATFORM_M, PLATFORM_VIR, PLATFORM_DED, PLATFORM_N, PLATFORM_LOC, PLATFORM_CPH, PLATFORM_POWER, PLATFORM_NOD_PRO_PCT, PLATFORM_NOD_PRO_MAN, PLATFORM_NOD_PRO_FAM, PLATFORM_NOD_PRO_SER, PLATFORM_NOD_PRO_MIC, PLATFORM_NOD_PRO_MOD, PLATFORM_NOD_PRO_NCT, PLATFORM_NOD_PRO_CLK, PLATFORM_NOD_PRO_TPC, PLATFORM_NOD_PRO_MAP1i, PLATFORM_NOD_PRO_SIZ1i, PLATFORM_NOD_PRO_LAT1i, PLATFORM_NOD_PRO_LINSIZ1i, PLATFORM_NOD_PRO_CL1i, PLATFORM_NOD_PRO_MAP1d, PLATFORM_NOD_PRO_SIZ1d, PLATFORM_NOD_PRO_LAT1d, PLATFORM_NOD_PRO_LINSIZ1d, PLATFORM_NOD_PRO_CL1d, PLATFORM_NOD_PRO_MAP2, PLATFORM_NOD_PRO_SIZ2, PLATFORM_NOD_PRO_LAT2, PLATFORM_NOD_PRO_LINSIZ2, PLATFORM_NOD_PRO_CL2, PLATFORM_NOD_PRO_COR, PLATFORM_NOD_PRO_MAP3, PLATFORM_NOD_PRO_SIZ3, PLATFORM_NOD_PRO_LAT3, PLATFORM_NOD_PRO_LINSIZ3, PLATFORM_NOD_PRO_CL3, PLATFORM_TsFMA, PLATFORM_TdFMA, PLATFORM_TiADD, PLATFORM_TiSUB, PLATFORM_TiMUL, PLATFORM_TiDIV, PLATFORM_TsADD, PLATFORM_TsSUB, PLATFORM_TsMUL, PLATFORM_TsDIV, PLATFORM_TdADD, PLATFORM_TdSUB, PLATFORM_TdMUL, PLATFORM_TdDIV, PLATFORM_NOD_PRO, PLATFORM_NOD_ACC_NCT, PLATFORM_NOD_ACC_MAN, PLATFORM_NOD_ACC_TYP, PLATFORM_NOD_ACC_ARC, PLATFORM_NOD_ACC_MOD, PLATFORM_NOD_ACC_MEM, PLATFORM_NOD_ACC, PLATFORM_NOD_MEM_SIZ, PLATFORM_NOD_MEM_LAT, PLATFORM_NOD_MEM_BAND, PLATFORM_NOD_MEM, PLATFORM_NOD_STO_LAT, PLATFORM_NOD_STO_SIZ, PLATFORM_NOD_STO_BAND, PLATFORM_NOD_STO_NETBAND, PLATFORM_NOD_STO, PLATFORM_NOD_OS, PLATFORM_NOD, PLATFORM_NET_STT, PLATFORM_NET_NLT, PLATFORM_NET_BAN, PLATFORM_NET_TOP, PLATFORM_NET, PLATFORM_STO_SIZ, PLATFORM_STO_LAT, PLATFORM_STO_BAND, PLATFORM_STO_NETBAND, PLATFORM_STO, PLATFORM_PER_P0, PLATFORM_PER_P1, PLATFORM_PER_P2, PLATFORM_PER, PLATFORM_POW_P0, PLATFORM_POW_P1, PLATFORM_POW_P2, PLATFORM_POW_P3, PLATFORM_POW>
		{		   
	   
		  //  private bool is_registered = false;
		   
			public override void main()
	        {  
                string path;
                path = ENV.GetEnvironmentVariable("PYTHON");
                if (path == null || path.Equals(""))
                   path = "python3";

                Intracommunicator comm = this.Communicator;
                //Intercommunicator inter_comm;

                string[] argv = new string[1];
                argv[0] = Path.Combine(Constants.PATH_GAC, "br.ufc.mdcc.hpcshelf.mpi.impl.MPILauncherForPythonImpl", "<module_name>", "<file_name>").ToString();
                int maxprocs = comm.Size;
                int[] error_codes = new int[maxprocs];

	            IBrowserBinding<IWSendDataPortType> browse_port = (IBrowserBinding<IWSendDataPortType>)Services.getPort("browser_port");
	            IWSendDataPortType browser = (IWSendDataPortType)browse_port.Client;

                Console.WriteLine("{0}: BEFORE SPAWN --- path={1} / argv[0]={2}", comm.Rank, path, argv[0]);
               // inter_comm = comm.Spawn(path, argv, maxprocs, 0);
                MPI_Comm inter_comm = Spawn(comm.comm, path, argv, maxprocs, 0, browser);
                Console.WriteLine("{0}: AFTER SPAWN 1", comm.Rank);
               // browser.Browse("BEFORE SPAWN " + comm.Rank);
	        }
	        
	        
	        static int Spawn(MPI_Comm comm, string command, string[] argv, int maxprocs, int root, IWSendDataPortType browser)
	        {
	            MPI_Comm intercomm;	            
	            
	            unsafe
	            {
	                byte[] byte_command = System.Text.Encoding.ASCII.GetBytes(command);
	
	                // Copy args into C-style argc/argv
	                ASCIIEncoding ascii = new ASCIIEncoding();
	                byte** my_argv = stackalloc byte*[argv.Length];
	                for (int argidx = 0; argidx < argv.Length; ++argidx)
	                {
	                    // Copy argument into a byte array (C-style characters)
	                    char[] arg = argv[argidx].ToCharArray();
	                    fixed (char* argp = arg)
	                    {
	                        int length = ascii.GetByteCount(arg);
	                        byte* c_arg = stackalloc byte[length];
	                        if (length > 0)
	                        {
	                            ascii.GetBytes(argp, arg.Length, c_arg, length);
	                        }
	                        my_argv[argidx] = c_arg;
	                    }
	                }
	                if (argv == null || argv.Length == 0)
	                {
	                    //my_argv = Unsafe.MPI_ARGV_NULL;
	                    my_argv = (byte**)0;
	                }
	
	                fixed (byte* byte_command_p = byte_command)
	                {
	                    //Unsafe.MPI_Comm_spawn(byte_command_p, my_argv, maxprocs, Unsafe.MPI_INFO_NULL, root, comm, out intercomm, out array_of_errorcodes);
	                    Console.WriteLine("spawn 0");
	                    Unsafe.MPI_Comm_spawn(byte_command_p, my_argv, maxprocs, Unsafe.MPI_INFO_NULL, root, comm, out intercomm, (int*)0);
	                }
	                
	                int rank;
	                Unsafe.MPI_Comm_rank(intercomm, out rank);
	                    
	                  
	                string message;
	                do
	                {
	                    byte[] size_ = new byte[4]; 
	                    fixed (byte* s = size_)
	                    {
	                       IntPtr s_buf = (IntPtr) s;
	                       Unsafe.MPI_Status status;	                    
	                       Unsafe.MPI_Recv(s_buf, 4, Unsafe.MPI_BYTE, rank, 111, intercomm, out status);
	                    }
	                   
	                    int size = (int) size_[3];
	                    	                   
	                    byte[] m = new byte[13 + size + 2];
		                fixed (byte* p = m)
		                {   
		                    IntPtr buf = (IntPtr) p;
		                    Unsafe.MPI_Status status;	                    
		                    Unsafe.MPI_Recv(buf, m.Length, Unsafe.MPI_BYTE, rank, 222, intercomm, out status);
		                }
		                
		                char[] mm = new char[m.Length - 13];
		                for (int i=13; i<m.Length-2; i++)	
		                {
		                    mm[i-13] = (char)m[i];
		                }
	                
	                   //ASCIIEncoding ascii = new System.Text.ASCIIEncoding.ASCIIEncoding();
                       message = new String(mm);
                       browser.Browse(message); 
                       Console.WriteLine(message);	                   
	                } 
	                while (!message.EndsWith("done"));
	                    
	                //Unsafe.MPI_Barrier(comm);
	                
                    browser.Browse("!EOS");
	            }
	
	            return intercomm;
	        }
	        	        
		}
	        		
	}
