/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.blas.configure.UPLO;

namespace br.ufc.mdcc.hpcshelf.blas.configure.uplo.NA
{
	public interface BaseNA : BaseIUPLO, IQualifierKind 
	{
	}
}