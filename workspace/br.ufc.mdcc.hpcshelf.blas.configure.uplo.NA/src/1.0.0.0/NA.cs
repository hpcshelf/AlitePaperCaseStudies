using br.ufc.mdcc.hpcshelf.blas.configure.UPLO;

namespace br.ufc.mdcc.hpcshelf.blas.configure.uplo.NA
{
	public interface NA : BaseNA, IUPLO
	{
	}
}