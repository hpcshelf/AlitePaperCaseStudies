/* Automatically Generated Code */

using br.ufc.mdcc.backend.ec2.platform.general.EC2_A1_Metal;
using br.ufc.mdcc.backend.ec2.family.GeneralPurpose;
using br.ufc.mdcc.backend.ec2.type.general.A1;
using br.ufc.mdcc.backend.ec2.size.Metal;
using br.ufc.mdcc.hpcshelf.platform.maintainer.EC2;
using br.ufc.mdcc.hpcshelf.quantifier.IntUp;

namespace br.ufc.mdcc.backend.ec2.impl.platform.general.EC2_A1_Metal_impl 
{
	public abstract class BaseIEC2_A1_Metal_impl<N>: br.ufc.pargo.hpe.kinds.Environment, BaseIEC2_A1_Metal<N>
		where N:IntUp
	{
		private IEC2TypeA1 type = null;

		protected IEC2TypeA1 Type
		{
			get
			{
				if (this.type == null)
					this.type = (IEC2TypeA1) Services.getPort("type");
				return this.type;
			}
		}
		private IEC2SizeMetal size = null;

		protected IEC2SizeMetal Size
		{
			get
			{
				if (this.size == null)
					this.size = (IEC2SizeMetal) Services.getPort("size");
				return this.size;
			}
		}
		private IEC2FamilyGeneralPurpose family = null;

		protected IEC2FamilyGeneralPurpose Family
		{
			get
			{
				if (this.family == null)
					this.family = (IEC2FamilyGeneralPurpose) Services.getPort("family");
				return this.family;
			}
		}
		private IEC2 maintainer = null;

		protected IEC2 Maintainer
		{
			get
			{
				if (this.maintainer == null)
					this.maintainer = (IEC2) Services.getPort("maintainer");
				return this.maintainer;
			}
		}
	}
}