using br.ufc.mdcc.hpcshelf.platform.locale.northamerica.UnitedStates;

namespace br.ufc.mdcc.hpcshelf.platform.locale.northamerica.US_West
{
	public interface IUS_West : BaseIUS_West, IUnitedStates
	{
	}
}