using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.backend.ec2.type.Type;

namespace br.ufc.mdcc.backend.ec2.type.general.M5d
{
	public interface IEC2TypeM5d : BaseIEC2TypeM5d, IEC2Type<family.GeneralPurpose.IEC2FamilyGeneralPurpose>
	{
	}
}