using br.ufc.mdcc.hpcshelf.platform.locale.AsiaPacific;

namespace br.ufc.mdcc.hpcshelf.platform.locale.asiapacific.India
{
	public interface IIndia : BaseIIndia, IAsiaPacific
	{
	}
}