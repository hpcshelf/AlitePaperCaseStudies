/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.quantifier.IntDown;
using br.ufc.mdcc.hpcshelf.quantifier.IntUp;

namespace br.ufc.mdcc.hpcshelf.platform.Power
{
	public interface BaseIPower<P0, P1, P2, P3> : IQualifierKind 
		where P0:IntDown
		where P1:IntUp
		where P2:IntDown
		where P3:IntUp
	{
	}
}