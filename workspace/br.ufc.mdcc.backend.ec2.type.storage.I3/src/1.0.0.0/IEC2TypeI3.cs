using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.backend.ec2.type.Type;
using br.ufc.mdcc.backend.ec2.family.Storage;

namespace br.ufc.mdcc.backend.ec2.type.storage.I3
{
	public interface IEC2TypeI3 : BaseIEC2TypeI3, IEC2Type<IEC2FamilyStorage>
	{
	}
}