/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.mpi.IntercommunicatorPortType;

namespace br.ufc.mdcc.hpcshelf.mpi.intercommunicator.BasicSendReceive
{
	public interface BaseIBasicSendReceive : BaseIIntercommunicatorPortType, IQualifierKind 
	{
	}
}