using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.common.Data;
using System;

namespace br.ufc.mdcc.common.SComplex { 

	public interface ISComplex : BaseISComplex, IData
	{
		ISComplexInstance newInstance(double d);
	} // end main interface 

	public interface ISComplexInstance : IDataInstance, ICloneable
	{
		double Value { set; get; }
	}

} // end namespace 
