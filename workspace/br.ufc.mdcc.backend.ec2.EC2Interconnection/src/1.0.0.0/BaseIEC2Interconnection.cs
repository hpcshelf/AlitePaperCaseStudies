/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.quantifier.IntDown;
using br.ufc.mdcc.hpcshelf.quantifier.IntUp;
using br.ufc.mdcc.hpcshelf.platform.interconnection.Topology;
using br.ufc.mdcc.hpcshelf.platform.Interconnection;

namespace br.ufc.mdcc.backend.ec2.EC2Interconnection
{
	public interface BaseIEC2Interconnection<STT, BAN, TOP, NLT> : BaseIInterconnection<STT, NLT, BAN, TOP>, IQualifierKind 
		where STT:IntDown
		where BAN:IntUp
		where TOP:ITopology
		where NLT:IntDown
	{
	}
}