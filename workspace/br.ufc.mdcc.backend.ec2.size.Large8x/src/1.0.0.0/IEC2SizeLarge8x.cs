using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.backend.ec2.size.Size;

namespace br.ufc.mdcc.backend.ec2.size.Large8x
{
	public interface IEC2SizeLarge8x : BaseIEC2SizeLarge8x, IEC2Size
	{
	}
}