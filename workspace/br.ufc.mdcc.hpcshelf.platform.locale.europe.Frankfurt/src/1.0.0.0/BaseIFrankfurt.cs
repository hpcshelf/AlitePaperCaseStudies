/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.platform.locale.europe.Germany;

namespace br.ufc.mdcc.hpcshelf.platform.locale.europe.Frankfurt
{
	public interface BaseIFrankfurt : BaseIGermany, IQualifierKind 
	{
	}
}