using br.ufc.mdcc.hpcshelf.platform.locale.europe.Germany;

namespace br.ufc.mdcc.hpcshelf.platform.locale.europe.Frankfurt
{
	public interface IFrankfurt : BaseIFrankfurt, IGermany
	{
	}
}