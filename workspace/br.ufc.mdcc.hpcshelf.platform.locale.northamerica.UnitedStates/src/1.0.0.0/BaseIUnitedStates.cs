/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.platform.locale.NorthAmerica;

namespace br.ufc.mdcc.hpcshelf.platform.locale.northamerica.UnitedStates
{
	public interface BaseIUnitedStates : BaseINorthAmerica, IQualifierKind 
	{
	}
}