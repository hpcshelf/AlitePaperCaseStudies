using br.ufc.mdcc.hpcshelf.platform.locale.NorthAmerica;

namespace br.ufc.mdcc.hpcshelf.platform.locale.northamerica.UnitedStates
{
	public interface IUnitedStates : BaseIUnitedStates, INorthAmerica
	{
	}
}