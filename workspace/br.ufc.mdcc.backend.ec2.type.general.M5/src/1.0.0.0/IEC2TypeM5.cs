using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.backend.ec2.type.Type;

namespace br.ufc.mdcc.backend.ec2.type.general.M5
{
	public interface IEC2TypeM5 : BaseIEC2TypeM5, IEC2Type<family.GeneralPurpose.IEC2FamilyGeneralPurpose>
	{
	}
}