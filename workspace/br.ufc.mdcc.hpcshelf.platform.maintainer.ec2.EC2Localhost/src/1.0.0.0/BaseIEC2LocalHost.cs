/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.platform.maintainer.EC2;

namespace br.ufc.mdcc.hpcshelf.platform.maintainer.ec2.EC2Localhost
{
	public interface BaseIEC2LocalHost : BaseIEC2, IEnvironmentKind 
	{
	}
}