/* Automatically Generated Code */

using br.ufc.mdcc.backend.ec2.platform.memory.EC2_X1e_Large8x;
using br.ufc.mdcc.backend.ec2.family.Memory;
using br.ufc.mdcc.backend.ec2.type.memory.X1e;
using br.ufc.mdcc.backend.ec2.size.Large8x;
using br.ufc.mdcc.hpcshelf.platform.maintainer.EC2;
using br.ufc.mdcc.hpcshelf.quantifier.IntUp;

namespace br.ufc.mdcc.backend.ec2.impl.platform.memory.EC2_X1e_Large8x_impl 
{
	public abstract class BaseIEC2_X1e_Large8x_impl<N>: br.ufc.pargo.hpe.kinds.Environment, BaseIEC2_X1e_Large8x<N>
		where N:IntUp
	{
		private IEC2TypeX1e type = null;

		protected IEC2TypeX1e Type
		{
			get
			{
				if (this.type == null)
					this.type = (IEC2TypeX1e) Services.getPort("type");
				return this.type;
			}
		}
		private IEC2SizeLarge8x size = null;

		protected IEC2SizeLarge8x Size
		{
			get
			{
				if (this.size == null)
					this.size = (IEC2SizeLarge8x) Services.getPort("size");
				return this.size;
			}
		}
		private IEC2FamilyMemory family = null;

		protected IEC2FamilyMemory Family
		{
			get
			{
				if (this.family == null)
					this.family = (IEC2FamilyMemory) Services.getPort("family");
				return this.family;
			}
		}
		private IEC2 maintainer = null;

		protected IEC2 Maintainer
		{
			get
			{
				if (this.maintainer == null)
					this.maintainer = (IEC2) Services.getPort("maintainer");
				return this.maintainer;
			}
		}
	}
}