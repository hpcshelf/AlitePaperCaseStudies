/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.backend.ec2.type.Type;
using br.ufc.mdcc.backend.ec2.family.GeneralPurpose;

namespace br.ufc.mdcc.backend.ec2.type.general.T3a
{
	public interface BaseIEC2TypeT3a : BaseIEC2Type<IEC2FamilyGeneralPurpose>, IQualifierKind 
	{
	}
}