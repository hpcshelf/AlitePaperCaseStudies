using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.backend.ec2.type.Type;
using br.ufc.mdcc.backend.ec2.family.GeneralPurpose;

namespace br.ufc.mdcc.backend.ec2.type.general.T3a
{
	public interface IEC2TypeT3a : BaseIEC2TypeT3a, IEC2Type<IEC2FamilyGeneralPurpose>
	{
	}
}