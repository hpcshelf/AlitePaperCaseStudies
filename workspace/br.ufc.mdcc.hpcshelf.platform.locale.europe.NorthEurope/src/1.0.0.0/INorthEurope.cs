using br.ufc.mdcc.hpcshelf.platform.locale.Europe;

namespace br.ufc.mdcc.hpcshelf.platform.locale.europe.NorthEurope
{
	public interface INorthEurope : BaseINorthEurope, IEurope
	{
	}
}