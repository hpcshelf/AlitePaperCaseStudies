/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.platform.node.accelerator.Architecture;

namespace br.ufc.mdcc.hpcshelf.platform.node.accelerator.architecture.Ultrascale_plus
{
	public interface BaseIUltrascale_plus : BaseIAcceleratorArchitecture, IQualifierKind 
	{
	}
}