using br.ufc.mdcc.hpcshelf.platform.node.accelerator.Architecture;

namespace br.ufc.mdcc.hpcshelf.platform.node.accelerator.architecture.Ultrascale_plus
{
	public interface IUltrascale_plus : BaseIUltrascale_plus, IAcceleratorArchitecture
	{
	}
}