using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.backend.ec2.type.Type;

namespace br.ufc.mdcc.backend.ec2.type.memory.X1
{
	public interface IEC2TypeX1 : BaseIEC2TypeX1, IEC2Type<family.Memory.IEC2FamilyMemory>
	{
	}
}