/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpc.storm.binding.environment.EnvironmentBindingBaseDirect;
using br.ufc.mdcc.hpc.storm.binding.environment.EnvironmentPortTypeSinglePartner;
using br.ufc.mdcc.hpcshelf.blas.MatrixPortType;
using br.ufc.mdcc.common.Data;
using br.ufc.mdcc.hpcshelf.blas.data.DataDistribution;

namespace br.ufc.mdcc.hpcshelf.blas.MatrixReader
{
	public interface BaseIMatrixReader<DIST, T> : IComputationKind 
		where DIST:IDataDistribution
		where T:IData
	{
		IBindingDirect<IEnvironmentPortTypeSinglePartner, IMatrixPortType<T, DIST>> Matrix {get;}
	}
}