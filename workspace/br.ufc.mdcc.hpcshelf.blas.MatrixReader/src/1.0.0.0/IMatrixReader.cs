using br.ufc.mdcc.hpcshelf.blas.data.DataDistribution;
using br.ufc.mdcc.common.Data;

namespace br.ufc.mdcc.hpcshelf.blas.MatrixReader
{
	public interface IMatrixReader<DIST, T> : BaseIMatrixReader<DIST, T>
		where DIST:IDataDistribution
		where T:IData
	{
	}
}