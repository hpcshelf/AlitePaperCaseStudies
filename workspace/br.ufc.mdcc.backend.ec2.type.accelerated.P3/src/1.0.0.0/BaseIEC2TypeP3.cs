/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.backend.ec2.type.Type;

namespace br.ufc.mdcc.backend.ec2.type.accelerated.P3
{
	public interface BaseIEC2TypeP3 : BaseIEC2Type<family.Acceleration.IEC2FamilyAcceleration>, IQualifierKind 
	{
	}
}