using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.backend.ec2.type.Type;

namespace br.ufc.mdcc.backend.ec2.type.accelerated.P3
{
	public interface IEC2TypeP3 : BaseIEC2TypeP3, IEC2Type<family.Acceleration.IEC2FamilyAcceleration>
	{
	}
}