/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.platform.locale.AsiaPacific;

namespace br.ufc.mdcc.hpcshelf.platform.locale.asiapacific.SouthKorea
{
	public interface BaseISouthKorea : BaseIAsiaPacific, IQualifierKind 
	{
	}
}