using br.ufc.mdcc.hpcshelf.platform.locale.AsiaPacific;

namespace br.ufc.mdcc.hpcshelf.platform.locale.asiapacific.SouthKorea
{
	public interface ISouthKorea : BaseISouthKorea, IAsiaPacific
	{
	}
}