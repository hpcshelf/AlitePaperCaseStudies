using br.ufc.mdcc.hpcshelf.platform.node.accelerator.Manufacturer;

namespace br.ufc.mdcc.hpcshelf.platform.node.accelerator.manufacturer.Xilinx
{
	public interface IXilinx : BaseIXilinx, IAcceleratorManufacturer
	{
	}
}