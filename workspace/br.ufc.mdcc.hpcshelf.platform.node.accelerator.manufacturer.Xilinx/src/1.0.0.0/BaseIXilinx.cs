/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.platform.node.accelerator.Manufacturer;

namespace br.ufc.mdcc.hpcshelf.platform.node.accelerator.manufacturer.Xilinx
{
	public interface BaseIXilinx : BaseIAcceleratorManufacturer, IQualifierKind 
	{
	}
}