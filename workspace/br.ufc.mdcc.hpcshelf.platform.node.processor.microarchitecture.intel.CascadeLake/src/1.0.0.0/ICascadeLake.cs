using br.ufc.mdcc.hpcshelf.platform.node.processor.Microarchitecture;

namespace br.ufc.mdcc.hpcshelf.platform.node.processor.microarchitecture.intel.CascadeLake
{
	public interface ICascadeLake : BaseICascadeLake, IProcessorMicroarchitecture<br.ufc.mdcc.hpcshelf.platform.node.processor.manufacturer.Intel.IIntel>
	{
	}
}