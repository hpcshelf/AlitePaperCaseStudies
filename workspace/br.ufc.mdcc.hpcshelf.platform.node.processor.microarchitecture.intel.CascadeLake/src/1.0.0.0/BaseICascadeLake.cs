/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.platform.node.processor.Microarchitecture;

namespace br.ufc.mdcc.hpcshelf.platform.node.processor.microarchitecture.intel.CascadeLake
{
	public interface BaseICascadeLake : BaseIProcessorMicroarchitecture<br.ufc.mdcc.hpcshelf.platform.node.processor.manufacturer.Intel.IIntel>, IQualifierKind 
	{
	}
}