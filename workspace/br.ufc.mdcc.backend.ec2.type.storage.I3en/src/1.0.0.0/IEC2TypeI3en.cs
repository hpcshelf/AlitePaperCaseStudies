using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.backend.ec2.type.Type;

namespace br.ufc.mdcc.backend.ec2.type.storage.I3en
{
	public interface IEC2TypeI3en : BaseIEC2TypeI3en, IEC2Type<family.Storage.IEC2FamilyStorage>
	{
	}
}