/* Automatically Generated Code */

using br.ufc.mdcc.backend.ec2.platform.general.EC2_M5d_Large16x;
using br.ufc.mdcc.backend.ec2.family.GeneralPurpose;
using br.ufc.mdcc.backend.ec2.type.general.M5d;
using br.ufc.mdcc.backend.ec2.size.Large16x;
using br.ufc.mdcc.hpcshelf.platform.maintainer.EC2;
using br.ufc.mdcc.hpcshelf.quantifier.IntUp;

namespace br.ufc.mdcc.backend.ec2.impl.platform.general.EC2_M5d_Large16x_impl 
{
	public abstract class BaseIEC2_M5d_Large16x_impl<N>: br.ufc.pargo.hpe.kinds.Environment, BaseIEC2_M5d_Large16x<N>
		where N:IntUp
	{
		private IEC2TypeM5d type = null;

		protected IEC2TypeM5d Type
		{
			get
			{
				if (this.type == null)
					this.type = (IEC2TypeM5d) Services.getPort("type");
				return this.type;
			}
		}
		private IEC2SizeLarge16x size = null;

		protected IEC2SizeLarge16x Size
		{
			get
			{
				if (this.size == null)
					this.size = (IEC2SizeLarge16x) Services.getPort("size");
				return this.size;
			}
		}
		private IEC2FamilyGeneralPurpose family = null;

		protected IEC2FamilyGeneralPurpose Family
		{
			get
			{
				if (this.family == null)
					this.family = (IEC2FamilyGeneralPurpose) Services.getPort("family");
				return this.family;
			}
		}
		private IEC2 maintainer = null;

		protected IEC2 Maintainer
		{
			get
			{
				if (this.maintainer == null)
					this.maintainer = (IEC2) Services.getPort("maintainer");
				return this.maintainer;
			}
		}
	}
}