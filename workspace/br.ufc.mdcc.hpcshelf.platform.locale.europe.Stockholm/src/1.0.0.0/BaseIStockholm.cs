/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.platform.locale.europe.Sweden;

namespace br.ufc.mdcc.hpcshelf.platform.locale.europe.Stockholm
{
	public interface BaseIStockholm : BaseISweden, IQualifierKind 
	{
	}
}