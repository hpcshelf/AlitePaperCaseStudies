using br.ufc.mdcc.hpcshelf.platform.locale.europe.Sweden;

namespace br.ufc.mdcc.hpcshelf.platform.locale.europe.Stockholm
{
	public interface IStockholm : BaseIStockholm, ISweden
	{
	}
}