using br.ufc.mdcc.hpcshelf.platform.locale.Europe;

namespace br.ufc.mdcc.hpcshelf.platform.locale.europe.CentralEurope
{
	public interface ICentralEurope : BaseICentralEurope, IEurope
	{
	}
}