/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.platform.node.processor.Family;

namespace br.ufc.mdcc.hpcshelf.platform.node.processor.family.amd.EPYC
{
	public interface BaseIEPYC : BaseIProcessorFamily<br.ufc.mdcc.hpcshelf.platform.node.processor.manufacturer.AMD.IAMD>, IQualifierKind 
	{
	}
}