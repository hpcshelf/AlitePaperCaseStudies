/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.platform.node.processor.cache.mapping.SetAssociative;

namespace br.ufc.mdcc.hpcshelf.platform.node.processor.cache.mapping.SetAssociative_4Way
{
	public interface BaseISetAssociative_4Way : BaseICacheMappingSetAssociative<br.ufc.mdcc.hpcshelf.quantifier.IntUp.IntUp>, IQualifierKind 
	{
	}
}