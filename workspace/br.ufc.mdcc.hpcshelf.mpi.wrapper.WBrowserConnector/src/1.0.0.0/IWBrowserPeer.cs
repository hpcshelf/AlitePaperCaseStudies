using br.ufc.mdcc.hpcshelf.common.BrowserConnector;

namespace br.ufc.mdcc.hpcshelf.mpi.wrapper.WBrowserConnector
{
	public interface IWBrowserPeer : BaseIWBrowserPeer, IBrowserPeer<br.ufc.mdcc.hpcshelf.mpi.Language.ILanguage, br.ufc.mdcc.hpcshelf.common.browser.wrapper.WSendDataPortType.IWSendDataPortType>
	{
	}
}