/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.common.BrowserBinding;
using br.ufc.mdcc.hpcshelf.common.BrowserConnector;
using br.ufc.mdcc.hpcshelf.common.browser.RecvDataPortType;

namespace br.ufc.mdcc.hpcshelf.mpi.wrapper.WBrowserConnector
{
    public interface BaseIWBrowserRoot : BaseIBrowserRoot<br.ufc.mdcc.hpcshelf.common.browser.RecvDataPortType.IRecvDataPortType>, ISynchronizerKind 
	{
		new IBrowserBinding<IRecvDataPortType> Browse_port {get;}
	}
}