using br.ufc.mdcc.hpcshelf.common.BrowserConnector;

namespace br.ufc.mdcc.hpcshelf.mpi.wrapper.WBrowserConnector
{
	public interface IWBrowserRoot : BaseIWBrowserRoot, IBrowserRoot<br.ufc.mdcc.hpcshelf.common.browser.RecvDataPortType.IRecvDataPortType>
	{
	}
}