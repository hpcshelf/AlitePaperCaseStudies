/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.common.BrowserBinding;
using br.ufc.mdcc.hpcshelf.common.BrowserConnector;
using br.ufc.mdcc.hpcshelf.common.browser.wrapper.WSendDataPortType;

namespace br.ufc.mdcc.hpcshelf.mpi.wrapper.WBrowserConnector
{
    public interface BaseIWBrowserPeer : BaseIBrowserPeer<br.ufc.mdcc.hpcshelf.mpi.Language.ILanguage, br.ufc.mdcc.hpcshelf.common.browser.wrapper.WSendDataPortType.IWSendDataPortType>, ISynchronizerKind 
	{
		new IBrowserBinding<IWSendDataPortType> Send_data_port {get;}
	}
}