using br.ufc.mdcc.hpcshelf.platform.locale.europe.WestEurope;

namespace br.ufc.mdcc.hpcshelf.platform.locale.europe.UnitedKingdon
{
	public interface IUnitedKingdon : BaseIUnitedKingdon, IWestEurope
	{
	}
}