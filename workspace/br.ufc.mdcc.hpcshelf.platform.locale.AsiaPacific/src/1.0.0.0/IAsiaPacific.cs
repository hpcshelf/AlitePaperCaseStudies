using br.ufc.mdcc.hpcshelf.platform.locale.Asia;

namespace br.ufc.mdcc.hpcshelf.platform.locale.AsiaPacific
{
	public interface IAsiaPacific : BaseIAsiaPacific, IAsia
	{
	}
}