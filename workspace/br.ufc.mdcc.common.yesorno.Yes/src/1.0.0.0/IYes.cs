using br.ufc.mdcc.common.YesOrNo;

namespace br.ufc.mdcc.common.yesorno.Yes
{
	public interface IYes : BaseIYes, IYesOrNo
	{
	}
}