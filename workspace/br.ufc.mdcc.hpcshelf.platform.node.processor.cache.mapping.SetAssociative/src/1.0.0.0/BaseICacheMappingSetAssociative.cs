/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.quantifier.IntUp;
using br.ufc.mdcc.hpcshelf.platform.node.processor.cache.mapping.Direct;

namespace br.ufc.mdcc.hpcshelf.platform.node.processor.cache.mapping.SetAssociative
{
	public interface BaseICacheMappingSetAssociative<K> : BaseICacheMappingDirect, IQualifierKind 
		where K:IntUp
	{
	}
}