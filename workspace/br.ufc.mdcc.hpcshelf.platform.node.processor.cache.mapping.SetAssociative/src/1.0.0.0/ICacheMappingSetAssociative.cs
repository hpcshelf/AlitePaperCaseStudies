using br.ufc.mdcc.hpcshelf.quantifier.IntUp;
using br.ufc.mdcc.hpcshelf.platform.node.processor.cache.mapping.Direct;

namespace br.ufc.mdcc.hpcshelf.platform.node.processor.cache.mapping.SetAssociative
{
	public interface ICacheMappingSetAssociative<K> : BaseICacheMappingSetAssociative<K>, ICacheMappingDirect
		where K:IntUp
	{
	}
}