/* Automatically Generated Code */

using br.ufc.mdcc.backend.ec2.platform.storage.EC2_I3en_Large6x;
using br.ufc.mdcc.backend.ec2.family.Storage;
using br.ufc.mdcc.backend.ec2.type.storage.I3en;
using br.ufc.mdcc.backend.ec2.size.Large6x;
using br.ufc.mdcc.hpcshelf.platform.maintainer.EC2;
using br.ufc.mdcc.hpcshelf.quantifier.IntUp;

namespace br.ufc.mdcc.backend.ec2.impl.platform.storage.EC2_I3en_Large6x_impl 
{
	public abstract class BaseIEC2_I3en_Large6x_impl<N>: br.ufc.pargo.hpe.kinds.Environment, BaseIEC2_I3en_Large6x<N>
		where N:IntUp
	{
		private IEC2TypeI3en type = null;

		protected IEC2TypeI3en Type
		{
			get
			{
				if (this.type == null)
					this.type = (IEC2TypeI3en) Services.getPort("type");
				return this.type;
			}
		}
		private IEC2SizeLarge6x size = null;

		protected IEC2SizeLarge6x Size
		{
			get
			{
				if (this.size == null)
					this.size = (IEC2SizeLarge6x) Services.getPort("size");
				return this.size;
			}
		}
		private IEC2FamilyStorage family = null;

		protected IEC2FamilyStorage Family
		{
			get
			{
				if (this.family == null)
					this.family = (IEC2FamilyStorage) Services.getPort("family");
				return this.family;
			}
		}
		private IEC2 maintainer = null;

		protected IEC2 Maintainer
		{
			get
			{
				if (this.maintainer == null)
					this.maintainer = (IEC2) Services.getPort("maintainer");
				return this.maintainer;
			}
		}
	}
}