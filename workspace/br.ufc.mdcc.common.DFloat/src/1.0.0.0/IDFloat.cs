using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.common.Data;
using System;

namespace br.ufc.mdcc.common.DFloat { 

	public interface IDFloat : BaseIDFloat, IData
	{
		IDFloatInstance newInstance(double d);
	} // end main interface 

	public interface IDFloatInstance : IDataInstance, ICloneable
	{
		double Value { set; get; }
	}

} // end namespace 
