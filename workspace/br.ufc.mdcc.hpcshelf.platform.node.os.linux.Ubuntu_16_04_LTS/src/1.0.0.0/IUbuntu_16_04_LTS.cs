using br.ufc.mdcc.hpcshelf.platform.node.os.linux.Ubuntu;

namespace br.ufc.mdcc.hpcshelf.platform.node.os.linux.Ubuntu_16_04_LTS
{
	public interface IUbuntu_16_04_LTS : BaseIUbuntu_16_04_LTS, IUbuntu<br.ufc.mdcc.hpcshelf.platform.node.os.linux.ubuntu.codename.XenialXerus.IXenialXerius, br.ufc.mdcc.hpcshelf.quantifier.IntUp.IntUp, br.ufc.mdcc.hpcshelf.quantifier.IntUp.IntUp>
	{
	}
}