/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.quantifier.IntUp;
using br.ufc.mdcc.hpcshelf.quantifier.IntDown;
using br.ufc.mdcc.backend.ec2.storage.Type;
using br.ufc.mdcc.hpcshelf.platform.node.Storage;

namespace br.ufc.mdcc.backend.ec2.EC2InstanceStorage
{
	public interface BaseIEC2InstanceStorage<SIZ, LAT, BAND, NETBAND, TYPE> : BaseIStorage<SIZ, LAT, BAND, NETBAND>, IQualifierKind 
		where SIZ:IntUp
		where LAT:IntDown
		where BAND:IntUp
		where NETBAND:IntUp
		where TYPE:IEC2StorageType
	{
	}
}