/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.blas.configure.TRANS;

namespace br.ufc.mdcc.hpcshelf.blas.configure.trans.T
{
	public interface BaseT : BaseITRANS, IQualifierKind 
	{
	}
}