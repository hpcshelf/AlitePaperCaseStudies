using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.mpi.Language;

namespace br.ufc.mdcc.hpcshelf.language.Python
{
	public interface ILanguage_Python : BaseILanguage_Python, ILanguage
	{
	}
}