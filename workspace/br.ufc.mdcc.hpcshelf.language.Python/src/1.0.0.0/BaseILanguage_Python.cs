/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.mpi.Language;

namespace br.ufc.mdcc.hpcshelf.language.Python
{
	public interface BaseILanguage_Python : BaseILanguage, IQualifierKind 
	{
	}
}