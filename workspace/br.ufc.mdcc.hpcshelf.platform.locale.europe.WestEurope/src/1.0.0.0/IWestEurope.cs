using br.ufc.mdcc.hpcshelf.platform.locale.Europe;

namespace br.ufc.mdcc.hpcshelf.platform.locale.europe.WestEurope
{
	public interface IWestEurope : BaseIWestEurope, IEurope
	{
	}
}