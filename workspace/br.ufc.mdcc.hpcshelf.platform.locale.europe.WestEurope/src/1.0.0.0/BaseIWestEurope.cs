/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.platform.locale.Europe;

namespace br.ufc.mdcc.hpcshelf.platform.locale.europe.WestEurope
{
	public interface BaseIWestEurope : BaseIEurope, IQualifierKind 
	{
	}
}