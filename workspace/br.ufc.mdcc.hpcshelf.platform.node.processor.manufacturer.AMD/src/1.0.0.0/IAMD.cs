using br.ufc.mdcc.hpcshelf.platform.node.processor.Manufacturer;

namespace br.ufc.mdcc.hpcshelf.platform.node.processor.manufacturer.AMD
{
	public interface IAMD : BaseIAMD, IProcessorManufacturer
	{
	}
}