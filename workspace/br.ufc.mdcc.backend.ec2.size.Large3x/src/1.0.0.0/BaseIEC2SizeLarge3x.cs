/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.backend.ec2.size.Size;

namespace br.ufc.mdcc.backend.ec2.size.Large3x
{
	public interface BaseIEC2SizeLarge3x : BaseIEC2Size, IQualifierKind 
	{
	}
}