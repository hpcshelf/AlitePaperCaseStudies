using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.backend.ec2.size.Size;

namespace br.ufc.mdcc.backend.ec2.size.Large4x
{
	public interface IEC2SizeLarge4x : BaseIEC2SizeLarge4x, IEC2Size
	{
	}
}