/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.backend.ec2.size.Size;

namespace br.ufc.mdcc.backend.ec2.size.Large4x
{
	public interface BaseIEC2SizeLarge4x : BaseIEC2Size, IQualifierKind 
	{
	}
}