/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.common.Data;
using br.ufc.mdcc.hpcshelf.blas.data.DataDistribution;
using br.ufc.mdcc.hpcshelf.quantifier.IntUp;
using br.ufc.mdcc.hpc.storm.binding.environment.EnvironmentPortTypeMultiplePartner;

namespace br.ufc.mdcc.hpcshelf.blas.GetPortType
{
	public interface BaseIGetPortType<T, DIST, DIM> : BaseIEnvironmentPortTypeMultiplePartner, IQualifierKind 
		where T:IData
		where DIST:IDataDistribution
		where DIM:IntUp
	{
	}
}