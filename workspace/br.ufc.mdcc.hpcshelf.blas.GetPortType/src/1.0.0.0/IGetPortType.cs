using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.common.Data;
using br.ufc.mdcc.hpcshelf.blas.data.DataDistribution;
using br.ufc.mdcc.hpcshelf.quantifier.IntUp;
using br.ufc.mdcc.hpc.storm.binding.environment.EnvironmentPortTypeMultiplePartner;

namespace br.ufc.mdcc.hpcshelf.blas.GetPortType
{
	public interface IGetPortType<T, DIST, DIM> : BaseIGetPortType<T, DIST, DIM>, IEnvironmentPortTypeMultiplePartner
		where T:IData
		where DIST:IDataDistribution
		where DIM:IntUp
	{
	}
}