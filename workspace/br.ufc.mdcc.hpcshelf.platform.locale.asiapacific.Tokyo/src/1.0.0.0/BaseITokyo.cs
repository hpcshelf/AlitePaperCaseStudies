/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.platform.locale.asiapacific.Japan;

namespace br.ufc.mdcc.hpcshelf.platform.locale.asiapacific.Tokyo
{
	public interface BaseITokyo : BaseIJapan, IQualifierKind 
	{
	}
}