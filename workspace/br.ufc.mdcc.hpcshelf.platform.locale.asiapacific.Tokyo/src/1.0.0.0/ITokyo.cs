using br.ufc.mdcc.hpcshelf.platform.locale.asiapacific.Japan;

namespace br.ufc.mdcc.hpcshelf.platform.locale.asiapacific.Tokyo
{
	public interface ITokyo : BaseITokyo, IJapan
	{
	}
}