using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.backend.ec2.family.Family;
using br.ufc.mdcc.backend.ec2.size.Size;
using br.ufc.mdcc.backend.ec2.type.Type;
using br.ufc.mdcc.hpcshelf.quantifier.IntUp;
using br.ufc.mdcc.hpcshelf.platform.Platform;
using br.ufc.mdcc.hpcshelf.platform.maintainer.EC2;

namespace br.ufc.mdcc.backend.ec2.EC2Platform
{
 public interface IProcessingNode<F, L, T, N> : BaseIProcessingNode<F, L, T, N>, IProcessingNode<IEC2, N>
  where F:IEC2Family
  where L:IEC2Size
  where T:IEC2Type<F>
  where N:IntUp
 {
 }
}