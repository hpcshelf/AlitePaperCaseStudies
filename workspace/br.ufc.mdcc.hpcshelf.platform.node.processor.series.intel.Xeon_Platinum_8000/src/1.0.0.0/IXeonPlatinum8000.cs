using br.ufc.mdcc.hpcshelf.platform.node.processor.Series;

namespace br.ufc.mdcc.hpcshelf.platform.node.processor.series.intel.Xeon_Platinum_8000
{
	public interface IXeonPlatinum8000 : BaseIXeonPlatinum8000, IProcessorSeries<br.ufc.mdcc.hpcshelf.platform.node.processor.manufacturer.Intel.IIntel, br.ufc.mdcc.hpcshelf.platform.node.processor.family.intel.Xeon_Platinum.IXeonPlatinum>
	{
	}
}