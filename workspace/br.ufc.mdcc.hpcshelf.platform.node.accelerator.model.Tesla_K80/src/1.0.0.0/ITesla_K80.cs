using br.ufc.mdcc.hpcshelf.platform.node.accelerator.Model;

namespace br.ufc.mdcc.hpcshelf.platform.node.accelerator.model.Tesla_K80
{
	public interface ITesla_K80 : BaseITesla_K80, IAcceleratorModel<br.ufc.mdcc.hpcshelf.platform.node.accelerator.manufacturer.NVIDIA.INVIDIA, br.ufc.mdcc.hpcshelf.platform.node.accelerator.type.Tesla.ITesla, br.ufc.mdcc.hpcshelf.platform.node.accelerator.architecture.Kepler.IKepler>
	{
	}
}