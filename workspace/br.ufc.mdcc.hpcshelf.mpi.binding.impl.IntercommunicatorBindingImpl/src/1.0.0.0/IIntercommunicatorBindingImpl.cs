using System;
using br.ufc.pargo.hpe.backend.DGAC;
using br.ufc.pargo.hpe.basic;
using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.mpi.IntercommunicatorPortType;
using br.ufc.mdcc.hpcshelf.mpi.binding.IntercommunicatorBinding;
using System.Threading;

namespace br.ufc.mdcc.hpcshelf.mpi.binding.impl.IntercommunicatorBindingImpl
{
	public class IIntercommunicatorBindingImpl<S> : BaseIIntercommunicatorBindingImpl<S>, IIntercommunicatorBinding<S>
		where S:IIntercommunicatorPortType
	{
		public override void main()
		{
		}
		
		private ManualResetEvent sync = new ManualResetEvent(false);
				

        public IIntercommunicatorPortType Client
        {
              get
              {
     			sync.WaitOne ();
	      		return server;
              }
        }

        private S server = default(S);

        public S Server
        {
              set
              {
	   			this.server = value; 
                if (this.server == null)
                {
                    Console.WriteLine("IIntercommunicatorBindingImpl - SERVER RESET");
                    sync.Reset();
                }
                else
                {
                    Console.WriteLine("IIntercommunicatorBindingImpl - SERVER SET");
                    sync.Set ();
                }
              }
        }
	}
}
