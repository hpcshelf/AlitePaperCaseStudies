using br.ufc.mdcc.hpcshelf.platform.node.accelerator.Architecture;

namespace br.ufc.mdcc.hpcshelf.platform.node.accelerator.architecture.Kepler
{
	public interface IKepler : BaseIKepler, IAcceleratorArchitecture
	{
	}
}