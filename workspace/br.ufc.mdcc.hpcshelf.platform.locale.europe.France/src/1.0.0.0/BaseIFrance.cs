/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.platform.locale.europe.CentralEurope;

namespace br.ufc.mdcc.hpcshelf.platform.locale.europe.France
{
	public interface BaseIFrance : BaseICentralEurope, IQualifierKind 
	{
	}
}