using br.ufc.mdcc.hpcshelf.platform.locale.europe.CentralEurope;

namespace br.ufc.mdcc.hpcshelf.platform.locale.europe.France
{
	public interface IFrance : BaseIFrance, ICentralEurope
	{
	}
}