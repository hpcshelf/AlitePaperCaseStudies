using br.ufc.mdcc.hpcshelf.platform.locale.AsiaPacific;

namespace br.ufc.mdcc.hpcshelf.platform.locale.asiapacific.Japan
{
	public interface IJapan : BaseIJapan, IAsiaPacific
	{
	}
}