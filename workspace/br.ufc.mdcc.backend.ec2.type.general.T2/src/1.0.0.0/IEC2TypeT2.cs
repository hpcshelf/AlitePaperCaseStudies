using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.backend.ec2.type.Type;

namespace br.ufc.mdcc.backend.ec2.type.general.T2
{
	public interface IEC2TypeT2 : BaseIEC2TypeT2, IEC2Type<family.GeneralPurpose.IEC2FamilyGeneralPurpose>
	{
	}
}