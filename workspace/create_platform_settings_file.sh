#!/bin/sh

export PLATFORM_SETTINGS_DIR="br.ufc.mdcc.backend.ec2.impl.platform.$2.EC2_$3_$4_impl/src/1.0.0.0"
export DEFAULT_REGION="\$node-locale"
export DEFAULT_N="\$node-count"
export DEFAULT_AMID="*"


if [ ! -d $PLATFORM_SETTINGS_DIR ] ; then
   echo "Directory $PLATFORM_SETTINGS_DIR not found !"
fi

rm "$PLATFORM_SETTINGS_DIR/platform.settings"
echo "http://127.0.0.1:8077/BackendServices.asmx" >>"$PLATFORM_SETTINGS_DIR/platform.settings"
echo "${DEFAULT_AMID};${DEFAULT_N};${DEFAULT_REGION};$2;$3;$4" >>"$PLATFORM_SETTINGS_DIR/platform.settings"
