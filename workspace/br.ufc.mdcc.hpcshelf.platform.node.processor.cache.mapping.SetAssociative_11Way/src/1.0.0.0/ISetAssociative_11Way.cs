using br.ufc.mdcc.hpcshelf.platform.node.processor.cache.mapping.SetAssociative;

namespace br.ufc.mdcc.hpcshelf.platform.node.processor.cache.mapping.SetAssociative_11Way
{
	public interface ISetAssociative_11Way : BaseISetAssociative_11Way, ICacheMappingSetAssociative<br.ufc.mdcc.hpcshelf.quantifier.IntUp.IntUp>
	{
	}
}