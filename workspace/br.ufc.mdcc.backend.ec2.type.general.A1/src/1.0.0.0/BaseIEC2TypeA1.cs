/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.backend.ec2.type.Type;

namespace br.ufc.mdcc.backend.ec2.type.general.A1
{
	public interface BaseIEC2TypeA1 : BaseIEC2Type<family.GeneralPurpose.IEC2FamilyGeneralPurpose>, IQualifierKind 
	{
	}
}