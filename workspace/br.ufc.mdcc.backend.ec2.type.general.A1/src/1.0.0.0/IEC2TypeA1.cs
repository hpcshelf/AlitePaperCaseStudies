using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.backend.ec2.type.Type;

namespace br.ufc.mdcc.backend.ec2.type.general.A1
{
	public interface IEC2TypeA1 : BaseIEC2TypeA1, IEC2Type<family.GeneralPurpose.IEC2FamilyGeneralPurpose>
	{
	}
}