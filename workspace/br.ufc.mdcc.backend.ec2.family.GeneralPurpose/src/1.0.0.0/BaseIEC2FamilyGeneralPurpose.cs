/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.backend.ec2.family.Family;

namespace br.ufc.mdcc.backend.ec2.family.GeneralPurpose
{
	public interface BaseIEC2FamilyGeneralPurpose : BaseIEC2Family, IQualifierKind 
	{
	}
}