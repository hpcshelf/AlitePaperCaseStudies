using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.backend.ec2.size.Size;

namespace br.ufc.mdcc.backend.ec2.size.Large16x
{
	public interface IEC2SizeLarge16x : BaseIEC2SizeLarge16x, IEC2Size
	{
	}
}