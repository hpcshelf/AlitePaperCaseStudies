using System;
using br.ufc.pargo.hpe.backend.DGAC;
using br.ufc.pargo.hpe.basic;
using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.quantifier.IntUp;
using br.ufc.mdcc.backend.ec2.platform.general.EC2_T3a_Small;

namespace br.ufc.mdcc.backend.ec2.impl.platform.general.EC2_T3a_Small_impl
{
	public class IEC2_T3a_Small_impl<N> : BaseIEC2_T3a_Small_impl<N>, IEC2_T3a_Small<N>
		where N:IntUp
	{
	}
}
