using br.ufc.mdcc.hpcshelf.blas.matrixtype.General;

namespace br.ufc.mdcc.hpcshelf.blas.matrixtype.Hermitian
{
	public interface IHermitianMatrix : BaseIHermitianMatrix, IGeneralMatrix
	{
	}
}