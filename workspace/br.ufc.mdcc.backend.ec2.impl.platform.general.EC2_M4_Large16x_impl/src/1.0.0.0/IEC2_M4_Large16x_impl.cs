using System;
using br.ufc.pargo.hpe.backend.DGAC;
using br.ufc.pargo.hpe.basic;
using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.quantifier.IntUp;
using br.ufc.mdcc.backend.ec2.platform.general.EC2_M4_Large16x;

namespace br.ufc.mdcc.backend.ec2.impl.platform.general.EC2_M4_Large16x_impl
{
	public class IEC2_M4_Large16x_impl<N> : BaseIEC2_M4_Large16x_impl<N>, IEC2_M4_Large16x<N>
		where N:IntUp
	{
	}
}
