using System;
using br.ufc.pargo.hpe.backend.DGAC;
using br.ufc.pargo.hpe.basic;
using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.quantifier.IntUp;
using br.ufc.mdcc.backend.ec2.platform.memory.EC2_X1e_Large32x;

namespace br.ufc.mdcc.backend.ec2.impl.platform.memory.EC2_X1e_Large32x_impl
{
	public class IEC2_X1e_Large32x_impl<N> : BaseIEC2_X1e_Large32x_impl<N>, IEC2_X1e_Large32x<N>
		where N:IntUp
	{
	}
}
