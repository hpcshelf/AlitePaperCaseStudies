using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.backend.ec2.type.Type;

namespace br.ufc.mdcc.backend.ec2.type.compute.C5n
{
	public interface IEC2TypeC5n : BaseIEC2TypeC5n, IEC2Type<family.Computation.IEC2FamilyComputation>
	{
	}
}