/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.backend.ec2.type.Type;

namespace br.ufc.mdcc.backend.ec2.type.compute.C5n
{
	public interface BaseIEC2TypeC5n : BaseIEC2Type<family.Computation.IEC2FamilyComputation>, IQualifierKind 
	{
	}
}