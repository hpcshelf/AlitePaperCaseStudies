/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.backend.ec2.family.Family;

namespace br.ufc.mdcc.backend.ec2.type.Type
{
	public interface BaseIEC2Type<F> : IQualifierKind 
		where F:IEC2Family
	{
	}
}