using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.backend.ec2.family.Family;

namespace br.ufc.mdcc.backend.ec2.type.Type
{
	public interface IEC2Type<F> : BaseIEC2Type<F>
		where F:IEC2Family
	{
	}
}