using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.backend.ec2.type.Type;

namespace br.ufc.mdcc.backend.ec2.type.memory.X1e
{
	public interface IEC2TypeX1e : BaseIEC2TypeX1e, IEC2Type<family.Memory.IEC2FamilyMemory>
	{
	}
}