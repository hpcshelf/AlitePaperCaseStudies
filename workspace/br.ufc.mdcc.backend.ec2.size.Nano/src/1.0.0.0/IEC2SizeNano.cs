using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.backend.ec2.size.Size;

namespace br.ufc.mdcc.backend.ec2.size.Nano
{
	public interface IEC2SizeNano : BaseIEC2SizeNano, IEC2Size
	{
	}
}