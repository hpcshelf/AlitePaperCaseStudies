/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.platform.node.accelerator.Manufacturer;
using br.ufc.mdcc.hpcshelf.platform.node.accelerator.Type;
using br.ufc.mdcc.hpcshelf.platform.node.accelerator.Architecture;

namespace br.ufc.mdcc.hpcshelf.platform.node.accelerator.Model
{
	public interface BaseIAcceleratorModel<MAN, TYPE, ARC> : IQualifierKind 
		where MAN:IAcceleratorManufacturer
		where TYPE:IAcceleratorType<MAN>
		where ARC:IAcceleratorArchitecture
	{
	}
}