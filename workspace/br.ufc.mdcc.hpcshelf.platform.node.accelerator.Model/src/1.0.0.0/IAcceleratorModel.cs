using br.ufc.mdcc.hpcshelf.platform.node.accelerator.Manufacturer;
using br.ufc.mdcc.hpcshelf.platform.node.accelerator.Type;
using br.ufc.mdcc.hpcshelf.platform.node.accelerator.Architecture;

namespace br.ufc.mdcc.hpcshelf.platform.node.accelerator.Model
{
	public interface IAcceleratorModel<MAN, TYPE, ARC> : BaseIAcceleratorModel<MAN, TYPE, ARC>
		where MAN:IAcceleratorManufacturer
		where TYPE:IAcceleratorType<MAN>
		where ARC:IAcceleratorArchitecture
	{
	}
}