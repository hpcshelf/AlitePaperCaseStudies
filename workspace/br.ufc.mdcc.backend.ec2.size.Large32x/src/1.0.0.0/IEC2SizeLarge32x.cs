using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.backend.ec2.size.Size;

namespace br.ufc.mdcc.backend.ec2.size.Large32x
{
	public interface IEC2SizeLarge32x : BaseIEC2SizeLarge32x, IEC2Size
	{
	}
}