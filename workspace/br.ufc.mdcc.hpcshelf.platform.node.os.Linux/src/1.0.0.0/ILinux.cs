using br.ufc.mdcc.hpcshelf.platform.node.OS;

namespace br.ufc.mdcc.hpcshelf.platform.node.os.Linux
{
	public interface ILinux : BaseILinux, IOperatingSystem
	{
	}
}