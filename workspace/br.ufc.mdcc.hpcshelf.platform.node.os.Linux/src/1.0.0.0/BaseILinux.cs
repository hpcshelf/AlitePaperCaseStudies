/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.platform.node.OS;

namespace br.ufc.mdcc.hpcshelf.platform.node.os.Linux
{
	public interface BaseILinux : BaseIOperatingSystem, IQualifierKind 
	{
	}
}