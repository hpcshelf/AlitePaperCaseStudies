using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.backend.ec2.type.Type;
using br.ufc.mdcc.backend.ec2.family.Acceleration;

namespace br.ufc.mdcc.backend.ec2.type.accelerated.F1
{
	public interface IEC2TypeF1 : BaseIEC2TypeF1, IEC2Type<IEC2FamilyAcceleration>
	{
	}
}