/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.backend.ec2.type.Type;
using br.ufc.mdcc.backend.ec2.family.Acceleration;

namespace br.ufc.mdcc.backend.ec2.type.accelerated.F1
{
	public interface BaseIEC2TypeF1 : BaseIEC2Type<IEC2FamilyAcceleration>, IQualifierKind 
	{
	}
}