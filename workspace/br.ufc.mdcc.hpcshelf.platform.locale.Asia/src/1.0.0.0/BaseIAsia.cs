/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.platform.locale.AnyWhere;

namespace br.ufc.mdcc.hpcshelf.platform.locale.Asia
{
	public interface BaseIAsia : BaseIAnyWhere, IQualifierKind 
	{
	}
}