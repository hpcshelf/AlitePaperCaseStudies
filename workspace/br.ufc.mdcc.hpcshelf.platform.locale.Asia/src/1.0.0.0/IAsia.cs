using br.ufc.mdcc.hpcshelf.platform.locale.AnyWhere;

namespace br.ufc.mdcc.hpcshelf.platform.locale.Asia
{
	public interface IAsia : BaseIAsia, IAnyWhere
	{
	}
}