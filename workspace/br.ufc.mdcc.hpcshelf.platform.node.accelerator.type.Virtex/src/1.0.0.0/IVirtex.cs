using br.ufc.mdcc.hpcshelf.platform.node.accelerator.Type;

namespace br.ufc.mdcc.hpcshelf.platform.node.accelerator.type.Virtex
{
	public interface IVirtex : BaseIVirtex, IAcceleratorType<br.ufc.mdcc.hpcshelf.platform.node.accelerator.manufacturer.Xilinx.IXilinx>
	{
	}
}