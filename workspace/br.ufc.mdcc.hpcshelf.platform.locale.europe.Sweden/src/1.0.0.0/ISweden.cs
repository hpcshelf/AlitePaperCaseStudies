using br.ufc.mdcc.hpcshelf.platform.locale.europe.NorthEurope;

namespace br.ufc.mdcc.hpcshelf.platform.locale.europe.Sweden
{
	public interface ISweden : BaseISweden, INorthEurope
	{
	}
}