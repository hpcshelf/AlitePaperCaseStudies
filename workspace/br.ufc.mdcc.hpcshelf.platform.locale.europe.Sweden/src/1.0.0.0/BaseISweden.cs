/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.platform.locale.europe.NorthEurope;

namespace br.ufc.mdcc.hpcshelf.platform.locale.europe.Sweden
{
	public interface BaseISweden : BaseINorthEurope, IQualifierKind 
	{
	}
}