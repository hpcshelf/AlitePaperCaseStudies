using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.backend.ec2.type.Type;

namespace br.ufc.mdcc.backend.ec2.type.accelerated.G4dn
{
	public interface IEC2TypeG4dn : BaseIEC2TypeG4dn, IEC2Type<family.Acceleration.IEC2FamilyAcceleration>
	{
	}
}