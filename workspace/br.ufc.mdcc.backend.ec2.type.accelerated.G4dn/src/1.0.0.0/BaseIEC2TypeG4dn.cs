/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.backend.ec2.type.Type;

namespace br.ufc.mdcc.backend.ec2.type.accelerated.G4dn
{
	public interface BaseIEC2TypeG4dn : BaseIEC2Type<family.Acceleration.IEC2FamilyAcceleration>, IQualifierKind 
	{
	}
}