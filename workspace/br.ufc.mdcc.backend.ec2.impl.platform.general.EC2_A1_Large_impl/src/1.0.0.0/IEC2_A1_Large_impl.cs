using System;
using br.ufc.pargo.hpe.backend.DGAC;
using br.ufc.pargo.hpe.basic;
using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.quantifier.IntUp;
using br.ufc.mdcc.backend.ec2.platform.general.EC2_A1_Large;

namespace br.ufc.mdcc.backend.ec2.impl.platform.general.EC2_A1_Large_impl
{
	public class IEC2_A1_Large_impl<N> : BaseIEC2_A1_Large_impl<N>, IEC2_A1_Large<N>
		where N:IntUp
	{
	}
}
