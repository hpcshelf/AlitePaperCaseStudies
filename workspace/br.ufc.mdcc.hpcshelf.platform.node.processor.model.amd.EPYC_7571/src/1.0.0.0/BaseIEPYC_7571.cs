/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.platform.node.processor.Model;

namespace br.ufc.mdcc.hpcshelf.platform.node.processor.model.amd.EPYC_7571
{
	public interface BaseIEPYC_7571 : BaseIProcessorModel<br.ufc.mdcc.hpcshelf.platform.node.processor.manufacturer.AMD.IAMD, br.ufc.mdcc.hpcshelf.platform.node.processor.family.amd.EPYC.IEPYC, br.ufc.mdcc.hpcshelf.platform.node.processor.series.amd.EPYC_7000.IEPYC7000, br.ufc.mdcc.hpcshelf.platform.node.processor.microarchitecture.amd.Zen.IZen>, IQualifierKind 
	{
	}
}