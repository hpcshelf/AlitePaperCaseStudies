using br.ufc.mdcc.hpcshelf.platform.locale.AnyWhere;

namespace br.ufc.mdcc.hpcshelf.platform.locale.Oceania
{
	public interface IOceania : BaseIOceania, IAnyWhere
	{
	}
}