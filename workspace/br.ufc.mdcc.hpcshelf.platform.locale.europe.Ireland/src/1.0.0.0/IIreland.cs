using br.ufc.mdcc.hpcshelf.platform.locale.europe.WestEurope;

namespace br.ufc.mdcc.hpcshelf.platform.locale.europe.Ireland
{
	public interface IIreland : BaseIIreland, IWestEurope
	{
	}
}