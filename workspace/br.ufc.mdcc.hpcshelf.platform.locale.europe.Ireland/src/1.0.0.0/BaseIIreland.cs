/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.platform.locale.europe.WestEurope;

namespace br.ufc.mdcc.hpcshelf.platform.locale.europe.Ireland
{
	public interface BaseIIreland : BaseIWestEurope, IQualifierKind 
	{
	}
}