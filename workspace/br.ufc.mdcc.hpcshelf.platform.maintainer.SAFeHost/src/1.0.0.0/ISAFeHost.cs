using br.ufc.mdcc.hpcshelf.platform.Maintainer;

namespace br.ufc.mdcc.hpcshelf.platform.maintainer.SAFeHost
{
	public interface ISAFeHost : BaseISAFeHost, IMaintainer
	{
	}
}