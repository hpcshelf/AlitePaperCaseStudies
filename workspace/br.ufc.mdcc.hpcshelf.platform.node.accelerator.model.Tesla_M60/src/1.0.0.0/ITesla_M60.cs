using br.ufc.mdcc.hpcshelf.platform.node.accelerator.Model;

namespace br.ufc.mdcc.hpcshelf.platform.node.accelerator.model.Tesla_M60
{
	public interface ITesla_M60 : BaseITesla_M60, IAcceleratorModel<br.ufc.mdcc.hpcshelf.platform.node.accelerator.manufacturer.NVIDIA.INVIDIA, br.ufc.mdcc.hpcshelf.platform.node.accelerator.type.Tesla.ITesla, br.ufc.mdcc.hpcshelf.platform.node.accelerator.architecture.Maxwell.IMaxwell>
	{
	}
}