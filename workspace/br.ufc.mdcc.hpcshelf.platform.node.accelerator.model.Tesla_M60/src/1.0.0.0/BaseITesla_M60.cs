/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.platform.node.accelerator.Model;

namespace br.ufc.mdcc.hpcshelf.platform.node.accelerator.model.Tesla_M60
{
	public interface BaseITesla_M60 : BaseIAcceleratorModel<br.ufc.mdcc.hpcshelf.platform.node.accelerator.manufacturer.NVIDIA.INVIDIA, br.ufc.mdcc.hpcshelf.platform.node.accelerator.type.Tesla.ITesla, br.ufc.mdcc.hpcshelf.platform.node.accelerator.architecture.Maxwell.IMaxwell>, IQualifierKind 
	{
	}
}