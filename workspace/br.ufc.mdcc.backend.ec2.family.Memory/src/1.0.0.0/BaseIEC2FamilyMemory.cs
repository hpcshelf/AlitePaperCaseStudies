/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.backend.ec2.family.Family;

namespace br.ufc.mdcc.backend.ec2.family.Memory
{
	public interface BaseIEC2FamilyMemory : BaseIEC2Family, IQualifierKind 
	{
	}
}