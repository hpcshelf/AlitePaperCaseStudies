using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.backend.ec2.family.Family;

namespace br.ufc.mdcc.backend.ec2.family.Memory
{
	public interface IEC2FamilyMemory : BaseIEC2FamilyMemory, IEC2Family
	{
	}
}