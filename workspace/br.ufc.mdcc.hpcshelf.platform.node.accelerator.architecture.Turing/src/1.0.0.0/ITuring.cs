using br.ufc.mdcc.hpcshelf.platform.node.accelerator.Architecture;

namespace br.ufc.mdcc.hpcshelf.platform.node.accelerator.architecture.Turing
{
	public interface ITuring : BaseITuring, IAcceleratorArchitecture
	{
	}
}