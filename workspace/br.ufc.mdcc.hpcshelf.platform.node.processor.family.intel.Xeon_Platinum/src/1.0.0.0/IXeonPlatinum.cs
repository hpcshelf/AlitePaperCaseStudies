using br.ufc.mdcc.hpcshelf.platform.node.processor.Family;

namespace br.ufc.mdcc.hpcshelf.platform.node.processor.family.intel.Xeon_Platinum
{
	public interface IXeonPlatinum : BaseIXeonPlatinum, IProcessorFamily<br.ufc.mdcc.hpcshelf.platform.node.processor.manufacturer.Intel.IIntel>
	{
	}
}