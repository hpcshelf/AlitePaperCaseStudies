/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.platform.node.processor.Family;

namespace br.ufc.mdcc.hpcshelf.platform.node.processor.family.intel.Xeon_Platinum
{
	public interface BaseIXeonPlatinum : BaseIProcessorFamily<br.ufc.mdcc.hpcshelf.platform.node.processor.manufacturer.Intel.IIntel>, IQualifierKind 
	{
	}
}