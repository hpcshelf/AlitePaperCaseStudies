using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.backend.ec2.size.Size;

namespace br.ufc.mdcc.backend.ec2.size.Small
{
	public interface IEC2SizeSmall : BaseIEC2SizeSmall, IEC2Size
	{
	}
}