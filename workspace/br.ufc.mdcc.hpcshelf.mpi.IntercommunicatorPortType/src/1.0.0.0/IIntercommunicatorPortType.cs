using br.ufc.mdcc.hpc.storm.binding.environment.EnvironmentPortTypeMultiplePartner;

namespace br.ufc.mdcc.hpcshelf.mpi.IntercommunicatorPortType
{
   
	public interface IIntercommunicatorPortType : BaseIIntercommunicatorPortType, IEnvironmentPortTypeMultiplePartner
	{
	}
}