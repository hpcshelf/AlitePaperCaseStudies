using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.backend.ec2.size.Size;

namespace br.ufc.mdcc.backend.ec2.size.Large2x
{
	public interface IEC2SizeLarge2x : BaseIEC2SizeLarge2x, IEC2Size
	{
	}
}