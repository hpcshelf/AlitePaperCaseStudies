/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.backend.ec2.size.Size;

namespace br.ufc.mdcc.backend.ec2.size.Large2x
{
	public interface BaseIEC2SizeLarge2x : BaseIEC2Size, IQualifierKind 
	{
	}
}