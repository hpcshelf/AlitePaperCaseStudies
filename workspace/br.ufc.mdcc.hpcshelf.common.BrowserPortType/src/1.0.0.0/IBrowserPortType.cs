using br.ufc.mdcc.hpc.storm.binding.environment.EnvironmentPortTypeMultiplePartner;

namespace br.ufc.mdcc.hpcshelf.common.BrowserPortType
{
	public interface IBrowserPortType : BaseIBrowserPortType, IEnvironmentPortTypeMultiplePartner
	{
	}
}