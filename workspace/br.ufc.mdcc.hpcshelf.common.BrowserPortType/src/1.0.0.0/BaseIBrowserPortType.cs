/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpc.storm.binding.environment.EnvironmentPortTypeMultiplePartner;

namespace br.ufc.mdcc.hpcshelf.common.BrowserPortType
{
	public interface BaseIBrowserPortType : BaseIEnvironmentPortTypeMultiplePartner, IQualifierKind 
	{
	}
}