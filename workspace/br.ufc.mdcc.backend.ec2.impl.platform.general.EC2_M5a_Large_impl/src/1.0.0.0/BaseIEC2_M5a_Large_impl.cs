/* Automatically Generated Code */

using br.ufc.mdcc.backend.ec2.platform.general.EC2_M5a_Large;
using br.ufc.mdcc.backend.ec2.family.GeneralPurpose;
using br.ufc.mdcc.backend.ec2.type.general.M5a;
using br.ufc.mdcc.backend.ec2.size.Large;
using br.ufc.mdcc.hpcshelf.platform.maintainer.EC2;
using br.ufc.mdcc.hpcshelf.quantifier.IntUp;

namespace br.ufc.mdcc.backend.ec2.impl.platform.general.EC2_M5a_Large_impl 
{
	public abstract class BaseIEC2_M5a_Large_impl<N>: br.ufc.pargo.hpe.kinds.Environment, BaseIEC2_M5a_Large<N>
		where N:IntUp
	{
		private IEC2TypeM5a type = null;

		protected IEC2TypeM5a Type
		{
			get
			{
				if (this.type == null)
					this.type = (IEC2TypeM5a) Services.getPort("type");
				return this.type;
			}
		}
		private IEC2SizeLarge size = null;

		protected IEC2SizeLarge Size
		{
			get
			{
				if (this.size == null)
					this.size = (IEC2SizeLarge) Services.getPort("size");
				return this.size;
			}
		}
		private IEC2FamilyGeneralPurpose family = null;

		protected IEC2FamilyGeneralPurpose Family
		{
			get
			{
				if (this.family == null)
					this.family = (IEC2FamilyGeneralPurpose) Services.getPort("family");
				return this.family;
			}
		}
		private IEC2 maintainer = null;

		protected IEC2 Maintainer
		{
			get
			{
				if (this.maintainer == null)
					this.maintainer = (IEC2) Services.getPort("maintainer");
				return this.maintainer;
			}
		}
	}
}