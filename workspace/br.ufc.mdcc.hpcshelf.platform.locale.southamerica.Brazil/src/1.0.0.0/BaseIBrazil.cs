/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.platform.locale.SouthAmerica;

namespace br.ufc.mdcc.hpcshelf.platform.locale.southamerica.Brazil
{
	public interface BaseIBrazil : BaseISouthAmerica, IQualifierKind 
	{
	}
}