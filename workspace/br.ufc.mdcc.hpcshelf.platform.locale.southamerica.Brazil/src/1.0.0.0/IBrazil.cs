using br.ufc.mdcc.hpcshelf.platform.locale.SouthAmerica;

namespace br.ufc.mdcc.hpcshelf.platform.locale.southamerica.Brazil
{
	public interface IBrazil : BaseIBrazil, ISouthAmerica
	{
	}
}