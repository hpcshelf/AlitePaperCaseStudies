using br.ufc.mdcc.hpcshelf.platform.locale.southamerica.Brazil;

namespace br.ufc.mdcc.hpcshelf.platform.locale.southamerica.SaoPaulo
{
	public interface ISaoPaulo : BaseISaoPaulo, IBrazil
	{
	}
}