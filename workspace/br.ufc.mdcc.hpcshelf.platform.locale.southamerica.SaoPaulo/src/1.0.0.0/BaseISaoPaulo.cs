/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.platform.locale.southamerica.Brazil;

namespace br.ufc.mdcc.hpcshelf.platform.locale.southamerica.SaoPaulo
{
	public interface BaseISaoPaulo : BaseIBrazil, IQualifierKind 
	{
	}
}