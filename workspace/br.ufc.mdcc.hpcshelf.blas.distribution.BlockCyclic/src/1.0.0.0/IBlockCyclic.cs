using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.blas.data.DataDistribution;

namespace br.ufc.mdcc.hpcshelf.blas.distribution.BlockCyclic
{
	public interface IBlockCyclic : BaseIBlockCyclic, IDataDistribution
	{
	}
}