using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.backend.ec2.family.Family;

namespace br.ufc.mdcc.backend.ec2.family.Computation
{
	public interface IEC2FamilyComputation : BaseIEC2FamilyComputation, IEC2Family
	{
	}
}