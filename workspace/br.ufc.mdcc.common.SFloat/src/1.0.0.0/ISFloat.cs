using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.common.Data;
using System;

namespace br.ufc.mdcc.common.SFloat { 

	public interface ISFloat : BaseISFloat, IData
	{
		ISFloatInstance newInstance(double d);
	} // end main interface 

	public interface ISFloatInstance : IDataInstance, ICloneable
	{
		double Value { set; get; }
	}

} // end namespace 
