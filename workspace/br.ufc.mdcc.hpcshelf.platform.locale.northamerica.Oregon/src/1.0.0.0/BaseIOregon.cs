/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.platform.locale.northamerica.US_West;

namespace br.ufc.mdcc.hpcshelf.platform.locale.northamerica.Oregon
{
	public interface BaseIOregon : BaseIUS_West, IQualifierKind 
	{
	}
}