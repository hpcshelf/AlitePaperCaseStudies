using br.ufc.mdcc.hpcshelf.platform.locale.northamerica.US_West;

namespace br.ufc.mdcc.hpcshelf.platform.locale.northamerica.Oregon
{
	public interface IOregon : BaseIOregon, IUS_West
	{
	}
}