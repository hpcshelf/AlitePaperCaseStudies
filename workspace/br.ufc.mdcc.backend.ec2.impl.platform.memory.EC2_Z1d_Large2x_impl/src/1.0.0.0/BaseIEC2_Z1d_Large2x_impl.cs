/* Automatically Generated Code */

using br.ufc.mdcc.backend.ec2.platform.memory.EC2_Z1d_Large2x;
using br.ufc.mdcc.backend.ec2.family.Memory;
using br.ufc.mdcc.backend.ec2.type.memory.Z1d;
using br.ufc.mdcc.backend.ec2.size.Large2x;
using br.ufc.mdcc.hpcshelf.platform.maintainer.EC2;
using br.ufc.mdcc.hpcshelf.quantifier.IntUp;

namespace br.ufc.mdcc.backend.ec2.impl.platform.memory.EC2_Z1d_Large2x_impl 
{
	public abstract class BaseIEC2_Z1d_Large2x_impl<N>: br.ufc.pargo.hpe.kinds.Environment, BaseIEC2_Z1d_Large2x<N>
		where N:IntUp
	{
		private IEC2TypeZ1d type = null;

		protected IEC2TypeZ1d Type
		{
			get
			{
				if (this.type == null)
					this.type = (IEC2TypeZ1d) Services.getPort("type");
				return this.type;
			}
		}
		private IEC2SizeLarge2x size = null;

		protected IEC2SizeLarge2x Size
		{
			get
			{
				if (this.size == null)
					this.size = (IEC2SizeLarge2x) Services.getPort("size");
				return this.size;
			}
		}
		private IEC2FamilyMemory family = null;

		protected IEC2FamilyMemory Family
		{
			get
			{
				if (this.family == null)
					this.family = (IEC2FamilyMemory) Services.getPort("family");
				return this.family;
			}
		}
		private IEC2 maintainer = null;

		protected IEC2 Maintainer
		{
			get
			{
				if (this.maintainer == null)
					this.maintainer = (IEC2) Services.getPort("maintainer");
				return this.maintainer;
			}
		}
	}
}