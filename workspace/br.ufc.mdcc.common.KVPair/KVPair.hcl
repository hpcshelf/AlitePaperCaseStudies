<?xml version="1.0" encoding="ASCII"?>
<component:hashComponent xmlns:component="http://www.example.org/HashComponent">
  <package>br.ufc.mdcc.common</package>
  <using>br.ufc.mdcc.common</using>
  <kind>Data</kind>
  <name>KVPair</name>
  <parameter>
    <identifier>value_type</identifier>
    <variable>V</variable>
    <constraint>
      <componentConstraint>br.ufc.mdcc.common.Data</componentConstraint>
    </constraint>
  </parameter>
  <parameter>
    <identifier>key_type</identifier>
    <variable>K</variable>
    <constraint>
      <componentConstraint>br.ufc.mdcc.common.Data</componentConstraint>
    </constraint>
  </parameter>
  <innerComponent>
    <kind>Data</kind>
    <identifier>key_type</identifier>
    <type>
      <componentName>K</componentName>
    </type>
    <access>private</access>
    <exportActions>true</exportActions>
  </innerComponent>
  <innerComponent>
    <kind>Data</kind>
    <identifier>value_type</identifier>
    <type>
      <componentName>V</componentName>
    </type>
    <access>private</access>
    <exportActions>true</exportActions>
  </innerComponent>
  <superComponent>
    <componentName>br.ufc.mdcc.common.Data</componentName>
  </superComponent>
  <unit index="0" name="kv_pair" parallel="false">
    <slice index="0" inner="key_type" unit="data"/>
    <slice index="0" inner="value_type" unit="data"/>
  </unit>
</component:hashComponent>