/* Automatically Generated Code */

using br.ufc.mdcc.backend.ec2.platform.general.EC2_T2_Nano;
using br.ufc.mdcc.backend.ec2.family.GeneralPurpose;
using br.ufc.mdcc.backend.ec2.type.general.T2;
using br.ufc.mdcc.backend.ec2.size.Nano;
using br.ufc.mdcc.hpcshelf.platform.maintainer.EC2;
using br.ufc.mdcc.hpcshelf.quantifier.IntUp;

namespace br.ufc.mdcc.backend.ec2.impl.platform.general.EC2_T2_Nano_impl 
{
	public abstract class BaseIEC2_T2_Nano_impl<N>: br.ufc.pargo.hpe.kinds.Environment, BaseIEC2_T2_Nano<N>
		where N:IntUp
	{
		private IEC2TypeT2 type = null;

		protected IEC2TypeT2 Type
		{
			get
			{
				if (this.type == null)
					this.type = (IEC2TypeT2) Services.getPort("type");
				return this.type;
			}
		}
		private IEC2SizeNano size = null;

		protected IEC2SizeNano Size
		{
			get
			{
				if (this.size == null)
					this.size = (IEC2SizeNano) Services.getPort("size");
				return this.size;
			}
		}
		private IEC2FamilyGeneralPurpose family = null;

		protected IEC2FamilyGeneralPurpose Family
		{
			get
			{
				if (this.family == null)
					this.family = (IEC2FamilyGeneralPurpose) Services.getPort("family");
				return this.family;
			}
		}
		private IEC2 maintainer = null;

		protected IEC2 Maintainer
		{
			get
			{
				if (this.maintainer == null)
					this.maintainer = (IEC2) Services.getPort("maintainer");
				return this.maintainer;
			}
		}
	}
}