/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpc.storm.binding.environment.EnvironmentBindingBaseDirect;
using br.ufc.mdcc.hpcshelf.blas.MatrixPortType;
using br.ufc.mdcc.common.Data;
using br.ufc.mdcc.hpcshelf.blas.data.DataDistribution;
using br.ufc.mdcc.hpc.storm.binding.environment.EnvironmentPortTypeSinglePartner;

namespace br.ufc.mdcc.hpcshelf.blas.MatrixWriter
{
	public interface BaseIMatrixWriter<DIST, T> : IComputationKind 
		where DIST:IDataDistribution
		where T:IData
	{
		IBindingDirect<IMatrixPortType<T, DIST>, IEnvironmentPortTypeSinglePartner> Matrix {get;}
	}
}