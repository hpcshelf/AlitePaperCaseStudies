using br.ufc.mdcc.hpcshelf.blas.data.DataDistribution;
using br.ufc.mdcc.common.Data;

namespace br.ufc.mdcc.hpcshelf.blas.MatrixWriter
{
	public interface IMatrixWriter<DIST, T> : BaseIMatrixWriter<DIST, T>
		where DIST:IDataDistribution
		where T:IData
	{
	}
}