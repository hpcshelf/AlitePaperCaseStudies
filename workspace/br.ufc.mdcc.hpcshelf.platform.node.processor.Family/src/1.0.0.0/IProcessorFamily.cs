using br.ufc.mdcc.hpcshelf.platform.node.processor.Manufacturer;

namespace br.ufc.mdcc.hpcshelf.platform.node.processor.Family
{
 public interface IProcessorFamily<MAN> : BaseIProcessorFamily<MAN>
  where MAN:IProcessorManufacturer
 {
 }
}