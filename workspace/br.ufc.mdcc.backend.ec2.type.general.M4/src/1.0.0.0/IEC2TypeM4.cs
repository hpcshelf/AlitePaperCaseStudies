using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.backend.ec2.type.Type;
using br.ufc.mdcc.backend.ec2.family.GeneralPurpose;

namespace br.ufc.mdcc.backend.ec2.type.general.M4
{
	public interface IEC2TypeM4 : BaseIEC2TypeM4, IEC2Type<IEC2FamilyGeneralPurpose>
	{
	}
}