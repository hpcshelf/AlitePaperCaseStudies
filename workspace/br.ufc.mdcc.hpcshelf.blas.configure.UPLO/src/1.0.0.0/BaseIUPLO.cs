/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.blas.CONFIG;

namespace br.ufc.mdcc.hpcshelf.blas.configure.UPLO
{
	public interface BaseIUPLO : BaseICONFIG, IQualifierKind 
	{
	}
}