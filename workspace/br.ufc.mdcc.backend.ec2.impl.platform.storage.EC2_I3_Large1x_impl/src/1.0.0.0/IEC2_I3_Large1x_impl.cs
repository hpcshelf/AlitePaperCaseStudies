using System;
using br.ufc.pargo.hpe.backend.DGAC;
using br.ufc.pargo.hpe.basic;
using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.quantifier.IntUp;
using br.ufc.mdcc.backend.ec2.platform.storage.EC2_I3_Large1x;

namespace br.ufc.mdcc.backend.ec2.impl.platform.storage.EC2_I3_Large1x_impl
{
	public class IEC2_I3_Large1x_impl<N> : BaseIEC2_I3_Large1x_impl<N>, IEC2_I3_Large1x<N>
		where N:IntUp
	{
	}
}
