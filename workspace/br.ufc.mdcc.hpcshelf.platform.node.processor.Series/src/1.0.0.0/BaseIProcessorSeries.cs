/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.platform.node.processor.Manufacturer;
using br.ufc.mdcc.hpcshelf.platform.node.processor.Family;

namespace br.ufc.mdcc.hpcshelf.platform.node.processor.Series
{
	public interface BaseIProcessorSeries<MAN, FAM> : IQualifierKind 
		where MAN:IProcessorManufacturer
		where FAM:IProcessorFamily<MAN>
	{
	}
}