using br.ufc.mdcc.hpcshelf.platform.node.processor.Manufacturer;
using br.ufc.mdcc.hpcshelf.platform.node.processor.Family;

namespace br.ufc.mdcc.hpcshelf.platform.node.processor.Series
{
	public interface IProcessorSeries<MAN, FAM> : BaseIProcessorSeries<MAN, FAM>
		where MAN:IProcessorManufacturer
		where FAM:IProcessorFamily<MAN>
	{
	}
}