/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.backend.ec2.type.Type;
using br.ufc.mdcc.backend.ec2.family.Storage;

namespace br.ufc.mdcc.backend.ec2.type.storage.H1
{
	public interface BaseIEC2TypeH1 : BaseIEC2Type<IEC2FamilyStorage>, IQualifierKind 
	{
	}
}