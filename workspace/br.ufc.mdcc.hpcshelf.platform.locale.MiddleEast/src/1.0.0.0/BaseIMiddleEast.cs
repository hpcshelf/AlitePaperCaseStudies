/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.platform.locale.Asia;

namespace br.ufc.mdcc.hpcshelf.platform.locale.MiddleEast
{
	public interface BaseIMiddleEast : BaseIAsia, IQualifierKind 
	{
	}
}