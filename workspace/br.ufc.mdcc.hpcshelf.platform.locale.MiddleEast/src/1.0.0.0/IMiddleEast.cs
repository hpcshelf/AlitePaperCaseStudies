using br.ufc.mdcc.hpcshelf.platform.locale.Asia;

namespace br.ufc.mdcc.hpcshelf.platform.locale.MiddleEast
{
	public interface IMiddleEast : BaseIMiddleEast, IAsia
	{
	}
}