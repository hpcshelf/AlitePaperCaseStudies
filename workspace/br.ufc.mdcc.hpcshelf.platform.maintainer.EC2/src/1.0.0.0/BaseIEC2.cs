/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.platform.Maintainer;

namespace br.ufc.mdcc.hpcshelf.platform.maintainer.EC2
{
	public interface BaseIEC2 : BaseIMaintainer, IQualifierKind 
	{
	}
}