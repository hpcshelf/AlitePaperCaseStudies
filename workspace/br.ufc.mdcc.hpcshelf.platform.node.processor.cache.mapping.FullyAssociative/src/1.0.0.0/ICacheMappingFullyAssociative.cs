using br.ufc.mdcc.hpcshelf.platform.node.processor.cache.mapping.SetAssociative;

namespace br.ufc.mdcc.hpcshelf.platform.node.processor.cache.mapping.FullyAssociative
{
	public interface ICacheMappingFullyAssociative : BaseICacheMappingFullyAssociative, ICacheMappingSetAssociative<br.ufc.mdcc.hpcshelf.quantifier.IntUp.IntUp>
	{
	}
}