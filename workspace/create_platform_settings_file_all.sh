#!/bin/sh

./create_platform_settings_file.sh GeneralPurpose general A1 Medium
./create_platform_settings_file.sh GeneralPurpose general A1 Large
./create_platform_settings_file.sh GeneralPurpose general A1 Large1x
./create_platform_settings_file.sh GeneralPurpose general A1 Large2x
./create_platform_settings_file.sh GeneralPurpose general A1 Large4x
./create_platform_settings_file.sh GeneralPurpose general A1 Metal

./create_platform_settings_file.sh GeneralPurpose general T3 Nano
./create_platform_settings_file.sh GeneralPurpose general T3 Micro
./create_platform_settings_file.sh GeneralPurpose general T3 Small
./create_platform_settings_file.sh GeneralPurpose general T3 Medium
./create_platform_settings_file.sh GeneralPurpose general T3 Large
./create_platform_settings_file.sh GeneralPurpose general T3 Large1x
./create_platform_settings_file.sh GeneralPurpose general T3 Large2x

./create_platform_settings_file.sh GeneralPurpose general T3a Nano
./create_platform_settings_file.sh GeneralPurpose general T3a Micro
./create_platform_settings_file.sh GeneralPurpose general T3a Small
./create_platform_settings_file.sh GeneralPurpose general T3a Medium
./create_platform_settings_file.sh GeneralPurpose general T3a Large
./create_platform_settings_file.sh GeneralPurpose general T3a Large1x
./create_platform_settings_file.sh GeneralPurpose general T3a Large2x

./create_platform_settings_file.sh GeneralPurpose general T2 Nano
./create_platform_settings_file.sh GeneralPurpose general T2 Micro
./create_platform_settings_file.sh GeneralPurpose general T2 Small
./create_platform_settings_file.sh GeneralPurpose general T2 Medium
./create_platform_settings_file.sh GeneralPurpose general T2 Large
./create_platform_settings_file.sh GeneralPurpose general T2 Large1x
./create_platform_settings_file.sh GeneralPurpose general T2 Large2x

./create_platform_settings_file.sh GeneralPurpose general M5 Large
./create_platform_settings_file.sh GeneralPurpose general M5 Large1x
./create_platform_settings_file.sh GeneralPurpose general M5 Large2x
./create_platform_settings_file.sh GeneralPurpose general M5 Large4x
./create_platform_settings_file.sh GeneralPurpose general M5 Large8x
./create_platform_settings_file.sh GeneralPurpose general M5 Large12x
./create_platform_settings_file.sh GeneralPurpose general M5 Large16x
./create_platform_settings_file.sh GeneralPurpose general M5 Large24x
./create_platform_settings_file.sh GeneralPurpose general M5 Metal

./create_platform_settings_file.sh GeneralPurpose general M5d Large
./create_platform_settings_file.sh GeneralPurpose general M5d Large1x
./create_platform_settings_file.sh GeneralPurpose general M5d Large2x
./create_platform_settings_file.sh GeneralPurpose general M5d Large4x
./create_platform_settings_file.sh GeneralPurpose general M5d Large8x
./create_platform_settings_file.sh GeneralPurpose general M5d Large12x
./create_platform_settings_file.sh GeneralPurpose general M5d Large16x
./create_platform_settings_file.sh GeneralPurpose general M5d Large24x
./create_platform_settings_file.sh GeneralPurpose general M5d Metal

./create_platform_settings_file.sh GeneralPurpose general M5a Large
./create_platform_settings_file.sh GeneralPurpose general M5a Large1x
./create_platform_settings_file.sh GeneralPurpose general M5a Large2x
./create_platform_settings_file.sh GeneralPurpose general M5a Large4x
./create_platform_settings_file.sh GeneralPurpose general M5a Large8x
./create_platform_settings_file.sh GeneralPurpose general M5a Large12x
./create_platform_settings_file.sh GeneralPurpose general M5a Large16x
./create_platform_settings_file.sh GeneralPurpose general M5a Large24x

./create_platform_settings_file.sh GeneralPurpose general M5ad Large
./create_platform_settings_file.sh GeneralPurpose general M5ad Large1x
./create_platform_settings_file.sh GeneralPurpose general M5ad Large2x
./create_platform_settings_file.sh GeneralPurpose general M5ad Large4x
./create_platform_settings_file.sh GeneralPurpose general M5ad Large8x
./create_platform_settings_file.sh GeneralPurpose general M5ad Large12x
./create_platform_settings_file.sh GeneralPurpose general M5ad Large16x
./create_platform_settings_file.sh GeneralPurpose general M5ad Large24x

./create_platform_settings_file.sh GeneralPurpose general M5n Large
./create_platform_settings_file.sh GeneralPurpose general M5n Large1x
./create_platform_settings_file.sh GeneralPurpose general M5n Large2x
./create_platform_settings_file.sh GeneralPurpose general M5n Large4x
./create_platform_settings_file.sh GeneralPurpose general M5n Large8x
./create_platform_settings_file.sh GeneralPurpose general M5n Large12x
./create_platform_settings_file.sh GeneralPurpose general M5n Large16x
./create_platform_settings_file.sh GeneralPurpose general M5n Large24x

./create_platform_settings_file.sh GeneralPurpose general M5dn Large
./create_platform_settings_file.sh GeneralPurpose general M5dn Large1x
./create_platform_settings_file.sh GeneralPurpose general M5dn Large2x
./create_platform_settings_file.sh GeneralPurpose general M5dn Large4x
./create_platform_settings_file.sh GeneralPurpose general M5dn Large8x
./create_platform_settings_file.sh GeneralPurpose general M5dn Large12x
./create_platform_settings_file.sh GeneralPurpose general M5dn Large16x
./create_platform_settings_file.sh GeneralPurpose general M5dn Large24x

./create_platform_settings_file.sh GeneralPurpose general M4 Large
./create_platform_settings_file.sh GeneralPurpose general M4 Large1x
./create_platform_settings_file.sh GeneralPurpose general M4 Large2x
./create_platform_settings_file.sh GeneralPurpose general M4 Large4x
./create_platform_settings_file.sh GeneralPurpose general M4 Large10x
./create_platform_settings_file.sh GeneralPurpose general M4 Large16x

./create_platform_settings_file.sh Computation compute C5 Large
./create_platform_settings_file.sh Computation compute C5 Large1x
./create_platform_settings_file.sh Computation compute C5 Large2x
./create_platform_settings_file.sh Computation compute C5 Large4x
./create_platform_settings_file.sh Computation compute C5 Large9x
./create_platform_settings_file.sh Computation compute C5 Large12x
./create_platform_settings_file.sh Computation compute C5 Large18x
./create_platform_settings_file.sh Computation compute C5 Large24x
./create_platform_settings_file.sh Computation compute C5 Metal

./create_platform_settings_file.sh Computation compute C5d Large
./create_platform_settings_file.sh Computation compute C5d Large1x
./create_platform_settings_file.sh Computation compute C5d Large2x
./create_platform_settings_file.sh Computation compute C5d Large4x
./create_platform_settings_file.sh Computation compute C5d Large9x
./create_platform_settings_file.sh Computation compute C5d Large12x
./create_platform_settings_file.sh Computation compute C5d Large18x
./create_platform_settings_file.sh Computation compute C5d Large24x
./create_platform_settings_file.sh Computation compute C5d Metal

./create_platform_settings_file.sh Computation compute C5n Large
./create_platform_settings_file.sh Computation compute C5n Large1x
./create_platform_settings_file.sh Computation compute C5n Large2x
./create_platform_settings_file.sh Computation compute C5n Large4x
./create_platform_settings_file.sh Computation compute C5n Large9x
./create_platform_settings_file.sh Computation compute C5n Large18x
./create_platform_settings_file.sh Computation compute C5n Metal

./create_platform_settings_file.sh Computation compute C4 Large
./create_platform_settings_file.sh Computation compute C4 Large1x
./create_platform_settings_file.sh Computation compute C4 Large2x
./create_platform_settings_file.sh Computation compute C4 Large4x
./create_platform_settings_file.sh Computation compute C4 Large8x

./create_platform_settings_file.sh Memory memory R5 Large
./create_platform_settings_file.sh Memory memory R5 Large1x
./create_platform_settings_file.sh Memory memory R5 Large2x
./create_platform_settings_file.sh Memory memory R5 Large4x
./create_platform_settings_file.sh Memory memory R5 Large8x
./create_platform_settings_file.sh Memory memory R5 Large12x
./create_platform_settings_file.sh Memory memory R5 Large16x
./create_platform_settings_file.sh Memory memory R5 Large24x
./create_platform_settings_file.sh Memory memory R5 Metal

./create_platform_settings_file.sh Memory memory R5d Large
./create_platform_settings_file.sh Memory memory R5d Large1x
./create_platform_settings_file.sh Memory memory R5d Large2x
./create_platform_settings_file.sh Memory memory R5d Large4x
./create_platform_settings_file.sh Memory memory R5d Large8x
./create_platform_settings_file.sh Memory memory R5d Large12x
./create_platform_settings_file.sh Memory memory R5d Large16x
./create_platform_settings_file.sh Memory memory R5d Large24x
./create_platform_settings_file.sh Memory memory R5d Metal

./create_platform_settings_file.sh Memory memory R5a Large
./create_platform_settings_file.sh Memory memory R5a Large1x
./create_platform_settings_file.sh Memory memory R5a Large2x
./create_platform_settings_file.sh Memory memory R5a Large4x
./create_platform_settings_file.sh Memory memory R5a Large8x
./create_platform_settings_file.sh Memory memory R5a Large12x
./create_platform_settings_file.sh Memory memory R5a Large16x
./create_platform_settings_file.sh Memory memory R5a Large24x

./create_platform_settings_file.sh Memory memory R5ad Large
./create_platform_settings_file.sh Memory memory R5ad Large1x
./create_platform_settings_file.sh Memory memory R5ad Large2x
./create_platform_settings_file.sh Memory memory R5ad Large4x
./create_platform_settings_file.sh Memory memory R5ad Large12x
./create_platform_settings_file.sh Memory memory R5ad Large24x

./create_platform_settings_file.sh Memory memory R5n Large
./create_platform_settings_file.sh Memory memory R5n Large1x
./create_platform_settings_file.sh Memory memory R5n Large2x
./create_platform_settings_file.sh Memory memory R5n Large4x
./create_platform_settings_file.sh Memory memory R5n Large8x
./create_platform_settings_file.sh Memory memory R5n Large12x
./create_platform_settings_file.sh Memory memory R5n Large16x
./create_platform_settings_file.sh Memory memory R5n Large24x

./create_platform_settings_file.sh Memory memory R5dn Large
./create_platform_settings_file.sh Memory memory R5dn Large1x
./create_platform_settings_file.sh Memory memory R5dn Large2x
./create_platform_settings_file.sh Memory memory R5dn Large4x
./create_platform_settings_file.sh Memory memory R5dn Large8x
./create_platform_settings_file.sh Memory memory R5dn Large12x
./create_platform_settings_file.sh Memory memory R5dn Large16x
./create_platform_settings_file.sh Memory memory R5dn Large24x

./create_platform_settings_file.sh Memory memory R4 Large
./create_platform_settings_file.sh Memory memory R4 Large1x
./create_platform_settings_file.sh Memory memory R4 Large2x
./create_platform_settings_file.sh Memory memory R4 Large4x
./create_platform_settings_file.sh Memory memory R4 Large8x
./create_platform_settings_file.sh Memory memory R4 Large16x

./create_platform_settings_file.sh Memory memory X1e Large1x
./create_platform_settings_file.sh Memory memory X1e Large2x
./create_platform_settings_file.sh Memory memory X1e Large4x
./create_platform_settings_file.sh Memory memory X1e Large8x
./create_platform_settings_file.sh Memory memory X1e Large16x
./create_platform_settings_file.sh Memory memory X1e Large32x

./create_platform_settings_file.sh Memory memory X1 Large16x
./create_platform_settings_file.sh Memory memory X1 Large32x

./create_platform_settings_file.sh Memory memory HighMemory Metal_u_6tb1
./create_platform_settings_file.sh Memory memory HighMemory Metal_u_9tb1
./create_platform_settings_file.sh Memory memory HighMemory Metal_u_12tb1
./create_platform_settings_file.sh Memory memory HighMemory Metal_u_18tb1
./create_platform_settings_file.sh Memory memory HighMemory Metal_u_24tb1

./create_platform_settings_file.sh Memory memory Z1d Large
./create_platform_settings_file.sh Memory memory Z1d Large1x
./create_platform_settings_file.sh Memory memory Z1d Large2x
./create_platform_settings_file.sh Memory memory Z1d Large3x
./create_platform_settings_file.sh Memory memory Z1d Large6x
./create_platform_settings_file.sh Memory memory Z1d Large12x
./create_platform_settings_file.sh Memory memory Z1d Metal

./create_platform_settings_file.sh Acceleration accelerated P3 Large2x
./create_platform_settings_file.sh Acceleration accelerated P3 Large8x
./create_platform_settings_file.sh Acceleration accelerated P3 Large16x
./create_platform_settings_file.sh Acceleration accelerated P3 Large24x

./create_platform_settings_file.sh Acceleration accelerated P2 Large1x
./create_platform_settings_file.sh Acceleration accelerated P2 Large8x
./create_platform_settings_file.sh Acceleration accelerated P2 Large16x

./create_platform_settings_file.sh Acceleration accelerated G4dn Large1x
./create_platform_settings_file.sh Acceleration accelerated G4dn Large2x
./create_platform_settings_file.sh Acceleration accelerated G4dn Large4x
./create_platform_settings_file.sh Acceleration accelerated G4dn Large8x
./create_platform_settings_file.sh Acceleration accelerated G4dn Large16x
./create_platform_settings_file.sh Acceleration accelerated G4dn Large12x
./create_platform_settings_file.sh Acceleration accelerated G4dn Metal

./create_platform_settings_file.sh Acceleration accelerated G3 Large1x
./create_platform_settings_file.sh Acceleration accelerated G3 Large4x
./create_platform_settings_file.sh Acceleration accelerated G3 Large8x
./create_platform_settings_file.sh Acceleration accelerated G3 Large16x

./create_platform_settings_file.sh Acceleration accelerated F1 Large2x
./create_platform_settings_file.sh Acceleration accelerated F1 Large4x
./create_platform_settings_file.sh Acceleration accelerated F1 Large16x

./create_platform_settings_file.sh Storage storage I3 Large
./create_platform_settings_file.sh Storage storage I3 Large1x
./create_platform_settings_file.sh Storage storage I3 Large2x
./create_platform_settings_file.sh Storage storage I3 Large4x
./create_platform_settings_file.sh Storage storage I3 Large8x
./create_platform_settings_file.sh Storage storage I3 Large16x
./create_platform_settings_file.sh Storage storage I3 Metal

./create_platform_settings_file.sh Storage storage I3en Large
./create_platform_settings_file.sh Storage storage I3en Large1x
./create_platform_settings_file.sh Storage storage I3en Large2x
./create_platform_settings_file.sh Storage storage I3en Large3x
./create_platform_settings_file.sh Storage storage I3en Large6x
./create_platform_settings_file.sh Storage storage I3en Large12x
./create_platform_settings_file.sh Storage storage I3en Large24x
./create_platform_settings_file.sh Storage storage I3en Metal

./create_platform_settings_file.sh Storage storage D2 Large1x
./create_platform_settings_file.sh Storage storage D2 Large2x
./create_platform_settings_file.sh Storage storage D2 Large4x
./create_platform_settings_file.sh Storage storage D2 Large8x

./create_platform_settings_file.sh Storage storage H1 Large2x
./create_platform_settings_file.sh Storage storage H1 Large4x
./create_platform_settings_file.sh Storage storage H1 Large8x
./create_platform_settings_file.sh Storage storage H1 Large16x


