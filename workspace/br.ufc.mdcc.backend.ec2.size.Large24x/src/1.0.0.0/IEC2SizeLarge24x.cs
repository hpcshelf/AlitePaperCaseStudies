using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.backend.ec2.size.Size;

namespace br.ufc.mdcc.backend.ec2.size.Large24x
{
	public interface IEC2SizeLarge24x : BaseIEC2SizeLarge24x, IEC2Size
	{
	}
}