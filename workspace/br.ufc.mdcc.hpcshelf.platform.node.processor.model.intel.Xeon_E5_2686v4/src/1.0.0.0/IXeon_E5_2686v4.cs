using br.ufc.mdcc.hpcshelf.platform.node.processor.Model;

namespace br.ufc.mdcc.hpcshelf.platform.node.processor.model.intel.Xeon_E5_2686v4
{
	public interface IXeon_E5_2686v4 : BaseIXeon_E5_2686v4, IProcessorModel<br.ufc.mdcc.hpcshelf.platform.node.processor.manufacturer.Intel.IIntel, br.ufc.mdcc.hpcshelf.platform.node.processor.family.intel.Xeon_E5.IXeonE5, br.ufc.mdcc.hpcshelf.platform.node.processor.series.intel.Xeon_E5_2000.IXeonE52000, br.ufc.mdcc.hpcshelf.platform.node.processor.microarchitecture.intel.Broadwell.IBroadwell>
	{
	}
}