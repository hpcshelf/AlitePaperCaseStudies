using br.ufc.mdcc.hpcshelf.platform.node.accelerator.Manufacturer;

namespace br.ufc.mdcc.hpcshelf.platform.node.accelerator.manufacturer.NVIDIA
{
	public interface INVIDIA : BaseINVIDIA, IAcceleratorManufacturer
	{
	}
}