using br.ufc.mdcc.hpcshelf.quantifier.IntUp;
using br.ufc.mdcc.hpcshelf.quantifier.IntDown;

namespace br.ufc.mdcc.hpcshelf.platform.node.Memory
{
	public interface IMemory<SIZ, LAT, BAND> : BaseIMemory<SIZ, LAT, BAND>
		where SIZ:IntUp
		where LAT:IntDown
		where BAND:IntUp
	{
	}
}