/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.platform.node.accelerator.Model;

namespace br.ufc.mdcc.hpcshelf.platform.node.accelerator.model.Tesla_V100
{
	public interface BaseITesla_V100 : BaseIAcceleratorModel<br.ufc.mdcc.hpcshelf.platform.node.accelerator.manufacturer.NVIDIA.INVIDIA, br.ufc.mdcc.hpcshelf.platform.node.accelerator.type.Tesla.ITesla, br.ufc.mdcc.hpcshelf.platform.node.accelerator.architecture.Volta.IVolta>, IQualifierKind 
	{
	}
}