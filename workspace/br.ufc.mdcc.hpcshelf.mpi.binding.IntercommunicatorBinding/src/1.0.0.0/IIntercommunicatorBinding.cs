using br.ufc.mdcc.hpcshelf.mpi.IntercommunicatorPortType;
using br.ufc.mdcc.hpc.storm.binding.environment.EnvironmentBindingBaseDirect;

namespace br.ufc.mdcc.hpcshelf.mpi.binding.IntercommunicatorBinding
{
	public interface IIntercommunicatorBinding<S> : BaseIIntercommunicatorBinding<S>, IBindingDirect<br.ufc.mdcc.hpcshelf.mpi.IntercommunicatorPortType.IIntercommunicatorPortType, S>
		where S:IIntercommunicatorPortType
	{
	}
}