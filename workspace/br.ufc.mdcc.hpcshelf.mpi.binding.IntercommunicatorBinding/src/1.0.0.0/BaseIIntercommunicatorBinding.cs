/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.mpi.IntercommunicatorPortType;
using br.ufc.mdcc.hpc.storm.binding.environment.EnvironmentBindingBaseDirect;

namespace br.ufc.mdcc.hpcshelf.mpi.binding.IntercommunicatorBinding
{
	public interface BaseIIntercommunicatorBinding<C> : BaseIBindingDirect<C, br.ufc.mdcc.hpcshelf.mpi.IntercommunicatorPortType.IIntercommunicatorPortType>, ISynchronizerKind 
		where C:IIntercommunicatorPortType
	{
	}
}