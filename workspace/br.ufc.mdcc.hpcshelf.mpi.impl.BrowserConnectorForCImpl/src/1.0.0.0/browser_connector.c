#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "browser_connector.h"

struct ChannelType
{
	int(*send)(int key, const void*, int, MPI_Datatype, int, int, int);
	int(*recv)(int key, void*, int*, MPI_Datatype, int, int, int, MPI_Status*);
};

struct ChannelType* channel = NULL;

int browser_connector_key[255];
int browser_key=0;

int registerChannel(int channel_key,
	                int(*send)(int, const void*, int, MPI_Datatype, int, int, int),
		            int(*recv)(int, void*, int*, MPI_Datatype, int, int, int, MPI_Status*))
{
	if (channel)
		free(channel);

	channel = (struct ChannelType*) malloc(sizeof(struct ChannelType));

    channel->send = send;
    channel->recv = recv;

    browser_connector_key[browser_key] = channel_key;
    return browser_key++;
}

void output_message(int key, char *message)
{
	// Use the channel->send to send the string ...
	int channel_key = browser_connector_key[key];
    printf("OUTPUT MESSAGE 1 %s", message);
	channel->send(channel_key, message, strlen(message), MPI_CHAR, 0, 0, 0);
    printf("OUTPUT MESSAGE 2 %s", message);
}

