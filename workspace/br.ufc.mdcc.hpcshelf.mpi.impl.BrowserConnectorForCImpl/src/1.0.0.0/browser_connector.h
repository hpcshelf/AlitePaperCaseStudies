#include "mpi.h"

int registerChannel(int channel_key,
		            int(*send)(int key, const void*, int, MPI_Datatype, int, int, int),
		            int(*recv)(int key, void*, int*, MPI_Datatype, int, int, int, MPI_Status*));

void output_message(int key, char *message);
