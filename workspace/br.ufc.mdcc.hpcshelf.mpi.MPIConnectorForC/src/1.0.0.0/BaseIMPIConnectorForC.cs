/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.mpi.binding.IntercommunicatorBinding;
using br.ufc.mdcc.hpcshelf.mpi.IntercommunicatorPortType;
using br.ufc.mdcc.hpcshelf.mpi.MPIConnector;

namespace br.ufc.mdcc.hpcshelf.mpi.MPIConnectorForC
{
	public interface BaseIMPIConnectorForC : BaseIMPIConnector<br.ufc.mdcc.hpcshelf.mpi.language.C.ILanguage_C, br.ufc.mdcc.hpcshelf.mpi.intercommunicator.BasicSendReceive.IBasicSendReceive>, ISynchronizerKind 
	{
		IIntercommunicatorBinding<br.ufc.mdcc.hpcshelf.mpi.intercommunicator.BasicSendReceive.IBasicSendReceive> Port {get;}
	}
}