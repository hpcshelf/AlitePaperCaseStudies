using br.ufc.mdcc.hpcshelf.mpi.MPIConnector;

namespace br.ufc.mdcc.hpcshelf.mpi.MPIConnectorForC
{
	public interface IMPIConnectorForC : BaseIMPIConnectorForC, IMPIConnector<br.ufc.mdcc.hpcshelf.mpi.language.C.ILanguage_C, br.ufc.mdcc.hpcshelf.mpi.intercommunicator.BasicSendReceive.IBasicSendReceive>
	{
	}
}