/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.platform.node.processor.cache.Mapping;
using br.ufc.mdcc.hpcshelf.quantifier.IntUp;
using br.ufc.mdcc.hpcshelf.quantifier.IntDown;

namespace br.ufc.mdcc.hpcshelf.platform.node.processor.Cache
{
	public interface BaseICache<MAP, SIZ, LAT, LINSIZ> : IQualifierKind 
		where MAP:ICacheMapping
		where SIZ:IntUp
		where LAT:IntDown
		where LINSIZ:IntUp
	{
	}
}