using br.ufc.mdcc.hpcshelf.platform.node.processor.cache.Mapping;
using br.ufc.mdcc.hpcshelf.quantifier.IntUp;
using br.ufc.mdcc.hpcshelf.quantifier.IntDown;

namespace br.ufc.mdcc.hpcshelf.platform.node.processor.Cache
{
	public interface ICache<MAP, SIZ, LAT, LINSIZ> : BaseICache<MAP, SIZ, LAT, LINSIZ>
		where MAP:ICacheMapping
		where SIZ:IntUp
		where LAT:IntDown
		where LINSIZ:IntUp
	{
	}
}