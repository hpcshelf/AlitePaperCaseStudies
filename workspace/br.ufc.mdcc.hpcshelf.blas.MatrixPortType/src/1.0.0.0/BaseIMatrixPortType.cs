/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.common.Data;
using br.ufc.mdcc.hpcshelf.blas.data.DataDistribution;
using br.ufc.mdcc.hpc.storm.binding.environment.EnvironmentPortTypeMultiplePartner;

namespace br.ufc.mdcc.hpcshelf.blas.MatrixPortType
{
	public interface BaseIMatrixPortType<T, DIST> : BaseIEnvironmentPortTypeMultiplePartner, IQualifierKind 
		where T:IData
		where DIST:IDataDistribution
	{
	}
}