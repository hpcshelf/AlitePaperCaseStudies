using br.ufc.mdcc.common.Data;
using br.ufc.mdcc.hpcshelf.blas.data.DataDistribution;
using br.ufc.mdcc.hpc.storm.binding.environment.EnvironmentPortTypeMultiplePartner;

namespace br.ufc.mdcc.hpcshelf.blas.MatrixPortType
{
	public interface IMatrixPortType<T, DIST> : BaseIMatrixPortType<T, DIST>, IEnvironmentPortTypeMultiplePartner
		where T:IData
		where DIST:IDataDistribution
	{
	}
}