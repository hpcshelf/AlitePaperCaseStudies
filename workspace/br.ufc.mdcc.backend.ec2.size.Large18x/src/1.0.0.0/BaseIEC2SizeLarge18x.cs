/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.backend.ec2.size.Size;

namespace br.ufc.mdcc.backend.ec2.size.Large18x
{
	public interface BaseIEC2SizeLarge18x : BaseIEC2Size, IQualifierKind 
	{
	}
}