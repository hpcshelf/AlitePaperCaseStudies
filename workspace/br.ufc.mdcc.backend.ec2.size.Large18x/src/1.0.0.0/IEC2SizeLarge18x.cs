using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.backend.ec2.size.Size;

namespace br.ufc.mdcc.backend.ec2.size.Large18x
{
	public interface IEC2SizeLarge18x : BaseIEC2SizeLarge18x, IEC2Size
	{
	}
}