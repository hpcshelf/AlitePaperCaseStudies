/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.quantifier.IntDown;
using br.ufc.mdcc.hpcshelf.quantifier.IntUp;
using br.ufc.mdcc.hpcshelf.platform.interconnection.Topology;

namespace br.ufc.mdcc.hpcshelf.platform.Interconnection
{
	public interface BaseIInterconnection<STT, NLT, BAN, TOP> : IQualifierKind 
		where STT:IntDown
		where NLT:IntDown
		where BAN:IntUp
		where TOP:ITopology
	{
	}
}