using br.ufc.mdcc.hpcshelf.quantifier.IntDown;
using br.ufc.mdcc.hpcshelf.quantifier.IntUp;
using br.ufc.mdcc.hpcshelf.platform.interconnection.Topology;

namespace br.ufc.mdcc.hpcshelf.platform.Interconnection
{
	public interface IInterconnection<STT, NLT, BAN, TOP> : BaseIInterconnection<STT, NLT, BAN, TOP>
		where STT:IntDown
		where NLT:IntDown
		where BAN:IntUp
		where TOP:ITopology
	{
	}
}