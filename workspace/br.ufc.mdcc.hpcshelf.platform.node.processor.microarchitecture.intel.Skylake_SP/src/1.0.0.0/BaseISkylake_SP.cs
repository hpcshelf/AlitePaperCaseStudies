/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.platform.node.processor.microarchitecture.intel.Skylake;

namespace br.ufc.mdcc.hpcshelf.platform.node.processor.microarchitecture.intel.Skylake_SP
{
	public interface BaseISkylake_SP : BaseISkylake, IQualifierKind 
	{
	}
}