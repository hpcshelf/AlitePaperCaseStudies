using br.ufc.mdcc.hpcshelf.platform.node.processor.Family;

namespace br.ufc.mdcc.hpcshelf.platform.node.processor.family.intel.Xeon_E7
{
	public interface IXeonE7 : BaseIXeonE7, IProcessorFamily<br.ufc.mdcc.hpcshelf.platform.node.processor.manufacturer.Intel.IIntel>
	{
	}
}