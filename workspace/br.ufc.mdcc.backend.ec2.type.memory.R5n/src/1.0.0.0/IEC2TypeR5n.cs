using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.backend.ec2.type.Type;

namespace br.ufc.mdcc.backend.ec2.type.memory.R5n
{
	public interface IEC2TypeR5n : BaseIEC2TypeR5n, IEC2Type<family.Memory.IEC2FamilyMemory>
	{
	}
}