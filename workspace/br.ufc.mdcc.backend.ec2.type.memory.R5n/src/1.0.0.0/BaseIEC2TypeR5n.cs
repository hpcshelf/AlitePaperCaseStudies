/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.backend.ec2.type.Type;

namespace br.ufc.mdcc.backend.ec2.type.memory.R5n
{
	public interface BaseIEC2TypeR5n : BaseIEC2Type<family.Memory.IEC2FamilyMemory>, IQualifierKind 
	{
	}
}