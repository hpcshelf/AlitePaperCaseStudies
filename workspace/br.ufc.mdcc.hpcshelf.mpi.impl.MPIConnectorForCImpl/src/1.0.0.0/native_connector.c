#include <stdio.h>
#include <stdlib.h>
#include "native_connector.h"

struct ChannelType
{
	int(*send)(int, const void*, int, MPI_Datatype, int, int, int);
	int(*recv)(int, void*, int*, MPI_Datatype, int, int, int, MPI_Status*);
};

struct ChannelType* channel = NULL;

int intercomm_key=0;
int intercomm_connector_key[255];

int registerChannel(int channel_key,
		            int(*send)(int, const void*, int, MPI_Datatype, int, int, int),
		            int(*recv)(int, void*, int*, MPI_Datatype, int, int, int, MPI_Status*))
{
	if (channel)
		free(channel);

    channel = (struct ChannelType*) malloc(sizeof(struct ChannelType));

    channel->send = send;
    channel->recv = recv;

    intercomm_connector_key[intercomm_key] = channel_key;
    return intercomm_key++;
}

int connector_send(int key,
		           const void *buf,
				   int count,
				   MPI_Datatype datatype,
				   int facet,
				   int target,
				   int tag)
{
	int channel_key = intercomm_connector_key[key];
    channel->send(channel_key, buf, count, datatype, facet, target, tag);
}

int connector_receive(int key,
		              void *buf,
					  int *count,
					  MPI_Datatype datatype,
					  int facet,
					  int source,
					  int tag,
					  MPI_Status *status)
{
	int channel_key = intercomm_connector_key[key];
    channel->recv(channel_key, buf, count, datatype, facet, source, tag, status);
}
