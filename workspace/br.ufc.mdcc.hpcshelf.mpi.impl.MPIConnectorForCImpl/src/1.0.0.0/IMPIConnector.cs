using System;
using br.ufc.pargo.hpe.backend.DGAC;
using br.ufc.pargo.hpe.basic;
using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.mpi.Language;
using br.ufc.mdcc.hpcshelf.mpi.MPIConnector;

namespace br.ufc.mdcc.hpcshelf.mpi.impl.MPIConnectorForCImpl
{
	public class IMPIConnector<L> : BaseIMPIConnector<L>, IMPIConnector<L>
		where L:ILanguage
	{
		public override void main()
		{
		}
	}
}
