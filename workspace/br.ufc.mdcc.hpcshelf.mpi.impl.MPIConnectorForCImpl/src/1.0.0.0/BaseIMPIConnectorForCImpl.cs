/* Automatically Generated Code */

using System;
using br.ufc.pargo.hpe.backend.DGAC;
using br.ufc.pargo.hpe.basic;
using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.mpi.Language;
using br.ufc.mdcc.hpc.storm.binding.environment.EnvironmentBindingBaseDirect;
using br.ufc.mdcc.hpc.storm.binding.environment.EnvironmentPortTypeSinglePartner;
using br.ufc.mdcc.hpc.storm.binding.channel.Binding;
using br.ufc.mdcc.hpcshelf.mpi.IntercommunicatorPortType;
using br.ufc.mdcc.hpcshelf.mpi.MPIConnectorForC;
using br.ufc.mdcc.hpcshelf.mpi.intercommunicator.BasicSendReceive;
using br.ufc.mdcc.hpcshelf.mpi.binding.IntercommunicatorBinding;

namespace br.ufc.mdcc.hpcshelf.mpi.impl.MPIConnectorForCImpl 
{
	public abstract class BaseIMPIConnectorForCImpl: Synchronizer, BaseIMPIConnectorForC
	{
		private ILanguage language = null;

		protected ILanguage Language
		{
			get
			{
				if (this.language == null)
					this.language = (ILanguage) Services.getPort("language");
				return this.language;
			}
		}
		private IIntercommunicatorBinding<IBasicSendReceive> port = null;

		public IIntercommunicatorBinding<IBasicSendReceive> Port
		{
			get
			{
				if (this.port == null)
					this.port = (IIntercommunicatorBinding<IBasicSendReceive>) Services.getPort("port");
				return this.port;
			}
		}
		private IChannel channel = null;

		protected IChannel Channel
		{
			get
			{
				if (this.channel == null)
					this.channel = (IChannel) Services.getPort("channel");
				return this.channel;
			}
		}
		private IIntercommunicatorPortType intercommunicator_port_type = null;

		protected IIntercommunicatorPortType Intercommunicator_port_type
		{
			get
			{
				if (this.intercommunicator_port_type == null)
					this.intercommunicator_port_type = (IIntercommunicatorPortType) Services.getPort("intercommunicator_port_type");
				return this.intercommunicator_port_type;
			}
		}
	}
}