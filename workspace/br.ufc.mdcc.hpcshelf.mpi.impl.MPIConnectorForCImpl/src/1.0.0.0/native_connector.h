#include "mpi.h"

int registerChannel(int channel_key,
		            int(*send)(int, const void*, int, MPI_Datatype, int, int, int),
		            int(*recv)(int, void*, int*, MPI_Datatype, int, int, int, MPI_Status*));

int connector_send(int key,
		           const void *buf,
				   int count,
				   MPI_Datatype datatype,
				   int facet,
				   int target,
				   int tag);

int connector_receive(int key,
		              void *buf,
				      int* count,
					  MPI_Datatype datatype,
					  int facet,
					  int source,
					  int tag,
					  MPI_Status *status);
