/* Automatically Generated Code */

using System;
using br.ufc.pargo.hpe.backend.DGAC;
using br.ufc.pargo.hpe.basic;
using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.mpi.Language;
using br.ufc.mdcc.hpcshelf.mpi.MPIConnector;

namespace br.ufc.mdcc.hpcshelf.mpi.impl.MPIConnectorForCImpl 
{
	public abstract class BaseIMPIConnector<L>: Synchronizer, BaseIMPIConnector<L>
		where L:ILanguage
	{
		private L language = default(L);

		protected L Language
		{
			get
			{
				if (this.language == null)
					this.language = (L) Services.getPort("language");
				return this.language;
			}
		}
	}
}