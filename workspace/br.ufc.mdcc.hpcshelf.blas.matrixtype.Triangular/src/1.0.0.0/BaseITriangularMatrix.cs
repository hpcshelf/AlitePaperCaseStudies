/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.blas.matrixtype.General;

namespace br.ufc.mdcc.hpcshelf.blas.matrixtype.Triangular
{
	public interface BaseITriangularMatrix : BaseIGeneralMatrix, IQualifierKind 
	{
	}
}