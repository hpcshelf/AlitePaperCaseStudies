using br.ufc.mdcc.hpcshelf.blas.matrixtype.General;

namespace br.ufc.mdcc.hpcshelf.blas.matrixtype.Triangular
{
	public interface ITriangularMatrix : BaseITriangularMatrix, IGeneralMatrix
	{
	}
}