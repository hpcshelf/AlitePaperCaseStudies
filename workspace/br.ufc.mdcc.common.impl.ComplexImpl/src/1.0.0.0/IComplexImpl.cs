using System;
using System.Numerics;
using br.ufc.pargo.hpe.backend.DGAC;
using br.ufc.pargo.hpe.basic;
using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.common.Complex;
using br.ufc.mdcc.common.Data;
using System.Collections.Generic;

namespace br.ufc.mdcc.common.impl.ComplexImpl 
{ 

	public class IComplexImpl : BaseIComplexImpl, IComplex
	{

		public IComplexImpl() { } 

		override public void after_initialize()
		{
			newInstance(); 
		}


		public IComplexInstance newInstance (Complex i)
		{
			IComplexInstance instance = (IComplexInstance)newInstance ();
			instance.Value = i;
			return instance;
		}

		public object newInstance ()
		{
			this.instance = new IComplexInstanceImpl ();
			return this.Instance;
		}

		private IComplexInstance instance;

		public object Instance {
			get { return instance;	}
			set { this.instance = (IComplexInstance) value; }
		}
	}

	[Serializable]
	public class IComplexInstanceImpl : IComplexInstance
	{
		#region IComplexInstance implementation

		private Complex val;

		public Complex Value {
			get { return val; }
			set { this.val = value;	}
		}

		public object ObjValue {
			get { return val; }
			set { this.val = (Complex) value;}
		}

		public override int GetHashCode ()
		{
			return Value.GetHashCode();	
		}

		public override string ToString ()
		{
			return Value.ToString();
		}

		public override bool Equals (object obj)
		{
			if (obj is IComplexInstanceImpl)
				return Value==(((IComplexInstanceImpl) obj).Value);
			else if (obj is Complex)
				return Value==(Complex)obj;
			else
				return false;
		}

		#endregion

		#region ICloneable implementation

		public object Clone ()
		{
			IComplexInstance clone = new IComplexInstanceImpl();
			clone.Value = this.Value;
			return clone;
		}

		#endregion

	}


}
