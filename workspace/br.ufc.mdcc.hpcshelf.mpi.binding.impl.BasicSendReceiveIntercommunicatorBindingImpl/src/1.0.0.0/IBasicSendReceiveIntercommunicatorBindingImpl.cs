using System;
using br.ufc.pargo.hpe.backend.DGAC;
using br.ufc.pargo.hpe.basic;
using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.mpi.IntercommunicatorPortType;
using br.ufc.mdcc.hpcshelf.mpi.binding.IntercommunicatorBinding;

namespace br.ufc.mdcc.hpcshelf.mpi.binding.impl.BasicSendReceiveIntercommunicatorBindingImpl
{
	public class IBasicSendReceiveIntercommunicatorBindingImpl<C> : BaseIBasicSendReceiveIntercommunicatorBindingImpl<C>, IIntercommunicatorBinding<C>
		where C:IIntercommunicatorPortType
	{
		public override void main()
		{
		}
	}
}
