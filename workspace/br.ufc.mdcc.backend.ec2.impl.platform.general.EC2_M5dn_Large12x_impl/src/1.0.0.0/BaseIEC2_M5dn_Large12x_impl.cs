/* Automatically Generated Code */

using br.ufc.mdcc.backend.ec2.platform.general.EC2_M5dn_Large12x;
using br.ufc.mdcc.backend.ec2.family.GeneralPurpose;
using br.ufc.mdcc.backend.ec2.type.general.M5dn;
using br.ufc.mdcc.backend.ec2.size.Large12x;
using br.ufc.mdcc.hpcshelf.platform.maintainer.EC2;
using br.ufc.mdcc.hpcshelf.quantifier.IntUp;

namespace br.ufc.mdcc.backend.ec2.impl.platform.general.EC2_M5dn_Large12x_impl 
{
	public abstract class BaseIEC2_M5dn_Large12x_impl<N>: br.ufc.pargo.hpe.kinds.Environment, BaseIEC2_M5dn_Large12x<N>
		where N:IntUp
	{
		private IEC2TypeM5dn type = null;

		protected IEC2TypeM5dn Type
		{
			get
			{
				if (this.type == null)
					this.type = (IEC2TypeM5dn) Services.getPort("type");
				return this.type;
			}
		}
		private IEC2SizeLarge12x size = null;

		protected IEC2SizeLarge12x Size
		{
			get
			{
				if (this.size == null)
					this.size = (IEC2SizeLarge12x) Services.getPort("size");
				return this.size;
			}
		}
		private IEC2FamilyGeneralPurpose family = null;

		protected IEC2FamilyGeneralPurpose Family
		{
			get
			{
				if (this.family == null)
					this.family = (IEC2FamilyGeneralPurpose) Services.getPort("family");
				return this.family;
			}
		}
		private IEC2 maintainer = null;

		protected IEC2 Maintainer
		{
			get
			{
				if (this.maintainer == null)
					this.maintainer = (IEC2) Services.getPort("maintainer");
				return this.maintainer;
			}
		}
	}
}