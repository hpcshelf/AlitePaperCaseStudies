/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.platform.node.processor.Manufacturer;

namespace br.ufc.mdcc.hpcshelf.platform.node.processor.manufacturer.Intel
{
 public interface BaseIIntel : BaseIProcessorManufacturer, IQualifierKind 
 {
 }
}