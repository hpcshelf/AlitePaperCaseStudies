using br.ufc.mdcc.hpcshelf.platform.node.processor.Manufacturer;

namespace br.ufc.mdcc.hpcshelf.platform.node.processor.manufacturer.Intel
{
 public interface IIntel : BaseIIntel, IProcessorManufacturer
 {
 }
}