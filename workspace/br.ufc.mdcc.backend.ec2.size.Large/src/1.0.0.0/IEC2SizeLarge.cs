using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.backend.ec2.size.Size;

namespace br.ufc.mdcc.backend.ec2.size.Large
{
	public interface IEC2SizeLarge : BaseIEC2SizeLarge, IEC2Size
	{
	}
}