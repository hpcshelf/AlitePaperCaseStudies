using br.ufc.mdcc.common.Data;
using br.ufc.mdcc.hpc.storm.binding.environment.EnvironmentPortTypeSinglePartner;

namespace br.ufc.mdcc.hpcshelf.blas.SetPortType
{
	public interface ISetPortType<T> : BaseISetPortType<T>, IEnvironmentPortTypeSinglePartner
		where T:IData
	{
	}
}