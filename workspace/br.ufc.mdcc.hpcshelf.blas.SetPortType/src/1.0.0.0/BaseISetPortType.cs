/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.common.Data;
using br.ufc.mdcc.hpc.storm.binding.environment.EnvironmentPortTypeSinglePartner;

namespace br.ufc.mdcc.hpcshelf.blas.SetPortType
{
	public interface BaseISetPortType<T> : BaseIEnvironmentPortTypeSinglePartner, IQualifierKind 
		where T:IData
	{
	}
}