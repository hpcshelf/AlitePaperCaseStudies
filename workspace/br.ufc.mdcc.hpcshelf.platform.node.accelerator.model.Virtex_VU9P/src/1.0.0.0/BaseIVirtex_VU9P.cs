/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.platform.node.accelerator.Model;

namespace br.ufc.mdcc.hpcshelf.platform.node.accelerator.model.Virtex_VU9P
{
	public interface BaseIVirtex_VU9P : BaseIAcceleratorModel<br.ufc.mdcc.hpcshelf.platform.node.accelerator.manufacturer.Xilinx.IXilinx, br.ufc.mdcc.hpcshelf.platform.node.accelerator.type.Virtex.IVirtex, br.ufc.mdcc.hpcshelf.platform.node.accelerator.architecture.Ultrascale_plus.IUltrascale_plus>, IQualifierKind 
	{
	}
}