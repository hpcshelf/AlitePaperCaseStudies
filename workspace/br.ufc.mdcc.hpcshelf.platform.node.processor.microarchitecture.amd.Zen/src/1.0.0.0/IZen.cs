using br.ufc.mdcc.hpcshelf.platform.node.processor.Microarchitecture;

namespace br.ufc.mdcc.hpcshelf.platform.node.processor.microarchitecture.amd.Zen
{
	public interface IZen : BaseIZen, IProcessorMicroarchitecture<br.ufc.mdcc.hpcshelf.platform.node.processor.manufacturer.AMD.IAMD>
	{
	}
}