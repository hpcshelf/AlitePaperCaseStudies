/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.backend.ec2.type.Type;
using br.ufc.mdcc.backend.ec2.family.Computation;

namespace br.ufc.mdcc.backend.ec2.type.compute.C4
{
	public interface BaseIEC2TypeC4 : BaseIEC2Type<IEC2FamilyComputation>, IQualifierKind 
	{
	}
}