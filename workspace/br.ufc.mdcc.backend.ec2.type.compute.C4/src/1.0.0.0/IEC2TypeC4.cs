using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.backend.ec2.type.Type;
using br.ufc.mdcc.backend.ec2.family.Computation;

namespace br.ufc.mdcc.backend.ec2.type.compute.C4
{
	public interface IEC2TypeC4 : BaseIEC2TypeC4, IEC2Type<IEC2FamilyComputation>
	{
	}
}