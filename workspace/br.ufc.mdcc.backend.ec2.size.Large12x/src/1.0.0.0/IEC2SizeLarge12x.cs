using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.backend.ec2.size.Size;

namespace br.ufc.mdcc.backend.ec2.size.Large12x
{
	public interface IEC2SizeLarge12x : BaseIEC2SizeLarge12x, IEC2Size
	{
	}
}