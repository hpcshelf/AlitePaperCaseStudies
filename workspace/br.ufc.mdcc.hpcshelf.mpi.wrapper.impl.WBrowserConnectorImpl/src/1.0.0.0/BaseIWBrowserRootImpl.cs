/* Automatically Generated Code */

using System;
using br.ufc.pargo.hpe.backend.DGAC;
using br.ufc.pargo.hpe.basic;
using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpc.storm.binding.channel.Binding;
using br.ufc.mdcc.hpcshelf.common.BrowserBinding;
using br.ufc.mdcc.hpcshelf.common.BrowserPortType;
using br.ufc.mdcc.hpcshelf.mpi.wrapper.WBrowserConnector;
using br.ufc.mdcc.hpcshelf.common.browser.RecvDataPortType;

namespace br.ufc.mdcc.hpcshelf.mpi.wrapper.impl.WBrowserConnectorImpl 
{
	public abstract class BaseIWBrowserRootImpl: Synchronizer, BaseIWBrowserRoot
	{
		private IChannel channel = null;

		protected IChannel Channel
		{
			get
			{
				if (this.channel == null)
					this.channel = (IChannel) Services.getPort("channel");
				return this.channel;
			}
		}
		private IBrowserBinding<IRecvDataPortType> browse_port = null;

		public IBrowserBinding<IRecvDataPortType> Browse_port
		{
			get
			{
				if (this.browse_port == null)
					this.browse_port = (IBrowserBinding<IRecvDataPortType>) Services.getPort("browse_port");
				return this.browse_port;
			}
		}
		private IRecvDataPortType recv_data_port_type = null;

		protected IRecvDataPortType Recv_data_port_type
		{
			get
			{
				if (this.recv_data_port_type == null)
					this.recv_data_port_type = (IRecvDataPortType) Services.getPort("recv_data_port_type");
				return this.recv_data_port_type;
			}
		}
	}
}