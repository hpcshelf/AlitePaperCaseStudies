/* Automatically Generated Code */

using System;
using br.ufc.pargo.hpe.backend.DGAC;
using br.ufc.pargo.hpe.basic;
using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.mpi.Language;
using br.ufc.mdcc.hpc.storm.binding.channel.Binding;
using br.ufc.mdcc.hpcshelf.common.BrowserBinding;
using br.ufc.mdcc.hpcshelf.common.BrowserPortType;
using br.ufc.mdcc.hpcshelf.mpi.wrapper.WBrowserConnector;
using br.ufc.mdcc.hpcshelf.common.browser.wrapper.WSendDataPortType;

namespace br.ufc.mdcc.hpcshelf.mpi.wrapper.impl.WBrowserConnectorImpl 
{
	public abstract class BaseIWBrowserPeerImpl: Synchronizer, BaseIWBrowserPeer
	{
		private ILanguage language = null;

		protected ILanguage Language
		{
			get
			{
				if (this.language == null)
					this.language = (ILanguage) Services.getPort("language");
				return this.language;
			}
		}
		private IChannel channel = null;

		protected IChannel Channel
		{
			get
			{
				if (this.channel == null)
					this.channel = (IChannel) Services.getPort("channel");
				return this.channel;
			}
		}
		private IBrowserBinding<IWSendDataPortType> send_data_port = null;

		public IBrowserBinding<IWSendDataPortType> Send_data_port
		{
			get
			{
				if (this.send_data_port == null)
					this.send_data_port = (IBrowserBinding<IWSendDataPortType>) Services.getPort("send_data_port");
				return this.send_data_port;
			}
		}
		private IWSendDataPortType send_data_port_type = null;

		protected IWSendDataPortType Send_data_port_type
		{
			get
			{
				if (this.send_data_port_type == null)
					this.send_data_port_type = (IWSendDataPortType) Services.getPort("send_data_port_type");
				return this.send_data_port_type;
			}
		}
	}
}