/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.platform.node.processor.Model;

namespace br.ufc.mdcc.hpcshelf.platform.node.processor.model.intel.Xeon_Platinum_8124M
{
	public interface BaseIXeon_Platinum_8124M : BaseIProcessorModel<br.ufc.mdcc.hpcshelf.platform.node.processor.manufacturer.Intel.IIntel, br.ufc.mdcc.hpcshelf.platform.node.processor.family.intel.Xeon_Platinum.IXeonPlatinum, br.ufc.mdcc.hpcshelf.platform.node.processor.series.intel.Xeon_Platinum_8000.IXeonPlatinum8000, br.ufc.mdcc.hpcshelf.platform.node.processor.microarchitecture.intel.Skylake_SP.ISkylake_SP>, IQualifierKind 
	{
	}
}