using System;
using br.ufc.pargo.hpe.backend.DGAC;
using br.ufc.pargo.hpe.basic;
using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.common.BrowserPortType;
using br.ufc.mdcc.hpcshelf.common.BrowserBinding;
using System.Threading;

namespace br.ufc.mdcc.hpcshelf.common.browser.BrowserBindingImpl
{
	public class IBrowserBindingImpl<S> : BaseIBrowserBindingImpl<S>, IBrowserBinding<S>
		where S:IBrowserPortType
	{
		public override void main()
		{
		}
		
 		private ManualResetEvent sync = new ManualResetEvent(false);				

        public IBrowserPortType Client
        {
            get
            {
		   	   sync.WaitOne ();
			   return server;
            }
        }

        private S server = default(S);

        public S Server
        {
            set
            {
   		    	this.server = value;
                if (this.server == null)
                {
                    Console.WriteLine("IBrowseBindingImpl - SERVER RESET");
                    sync.Reset();
                }
                else
                {
                    Console.WriteLine("IBrowseBindingImpl - SERVER SET");
                    sync.Set ();
                }
            }
        }
	}
}
