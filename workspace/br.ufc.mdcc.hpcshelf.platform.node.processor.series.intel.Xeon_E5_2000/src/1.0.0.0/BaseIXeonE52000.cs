/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.platform.node.processor.Series;

namespace br.ufc.mdcc.hpcshelf.platform.node.processor.series.intel.Xeon_E5_2000
{
	public interface BaseIXeonE52000 : BaseIProcessorSeries<br.ufc.mdcc.hpcshelf.platform.node.processor.manufacturer.Intel.IIntel, br.ufc.mdcc.hpcshelf.platform.node.processor.family.intel.Xeon_E5.IXeonE5>, IQualifierKind 
	{
	}
}