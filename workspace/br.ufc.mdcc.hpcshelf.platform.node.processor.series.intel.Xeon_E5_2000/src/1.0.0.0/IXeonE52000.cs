using br.ufc.mdcc.hpcshelf.platform.node.processor.Series;

namespace br.ufc.mdcc.hpcshelf.platform.node.processor.series.intel.Xeon_E5_2000
{
	public interface IXeonE52000 : BaseIXeonE52000, IProcessorSeries<br.ufc.mdcc.hpcshelf.platform.node.processor.manufacturer.Intel.IIntel, br.ufc.mdcc.hpcshelf.platform.node.processor.family.intel.Xeon_E5.IXeonE5>
	{
	}
}