/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.mpi.Language;
using br.ufc.mdcc.hpcshelf.mpi.IntercommunicatorPortType;

namespace br.ufc.mdcc.hpcshelf.mpi.MPIConnector
{
	public interface BaseIMPIConnector<L,S> : ISynchronizerKind 
		where L:ILanguage
		where S:IIntercommunicatorPortType
	{
	}
}