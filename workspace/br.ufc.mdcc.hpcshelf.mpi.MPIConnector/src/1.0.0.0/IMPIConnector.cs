using br.ufc.mdcc.hpcshelf.mpi.Language;
using br.ufc.mdcc.hpcshelf.mpi.IntercommunicatorPortType;

namespace br.ufc.mdcc.hpcshelf.mpi.MPIConnector
{   
	public interface IMPIConnector<L,S> : BaseIMPIConnector<L,S>
		where L:ILanguage
		where S:IIntercommunicatorPortType
	{
	}
}